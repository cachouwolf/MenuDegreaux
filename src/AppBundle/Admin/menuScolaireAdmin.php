<?php
namespace AppBundle\Admin;

use AppBundle\Entity\menuScolaire;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;


class menuScolaireAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Détails')
                ->add('name', 'text')
                ->add('actif', CheckboxType::class, array(
                    'label'    => 'Menu actif ',
                    'required' => true,
                ))
            ->end()
            ->with('Lundi')
                ->add('lundi_entree', 'text', array( 'required'   => false,))
                ->add('lundi_plat', 'text', array( 'required'   => false,))
                ->add('lundi_accompagnement', 'text', array( 'required'   => false,))
                ->add('lundi_dessert', 'text', array( 'required'   => false,))
            ->end()
            ->with('Mardi')
                ->add('mardi_entree', 'text', array( 'required'   => false,))
                ->add('mardi_plat', 'text', array( 'required'   => false,))
                ->add('mardi_accompagnement', 'text', array( 'required'   => false,))
                ->add('mardi_dessert', 'text', array( 'required'   => false,))
            ->end()
            ->with('Mercredi')
                ->add('mercredi_entree', 'text', array( 'required'   => false,))
                ->add('mercredi_plat', 'text', array( 'required'   => false,))
                ->add('mercredi_accompagnement', 'text', array( 'required'   => false,))
                ->add('mercredi_dessert', 'text', array( 'required'   => false,))
            ->end()
            ->with('Jeudi')
                ->add('jeudi_entree', 'text', array( 'required'   => false,))
                ->add('jeudi_plat', 'text', array( 'required'   => false,))
                ->add('jeudi_accompagnement', 'text', array( 'required'   => false,))
                ->add('jeudi_dessert', 'text', array( 'required'   => false,))
            ->end()
            ->with('Vendredi')
                ->add('vendredi_entree', 'text', array( 'required'   => false,))
                ->add('vendredi_plat', 'text', array( 'required'   => false,))
                ->add('vendredi_accompagnement', 'text', array( 'required'   => false,))
                ->add('vendredi_dessert', 'text', array( 'required'   => false,))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('actif')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('actif')
            ->add('_action', null, [
                'actions' => [
                    'edit' => [],
                    'delete' => ['template' => ':buttons:delete_button.html.twig'],

                ],
            ]) ;
        ;
    }

    public function toString($object)
    {
        return $object instanceof menuScolaire
        ? $object->getName()
            : ' Menu scolaire'; // shown in the breadcrumb on the create view
    }



}
