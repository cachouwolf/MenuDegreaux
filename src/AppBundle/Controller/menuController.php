<?php


namespace AppBundle\Controller;


use AppBundle\Entity\menuMidi;
use AppBundle\Entity\menuSoir;
use AppBundle\Entity\menuScolaire;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller used to manage blog contents in the public part of the site.
 *
 * @Route("/")
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class menuController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     *
     */
    public function homepageAction()
    {

    }


    /**
     * @Route("/menu/externe", name="externe")
     * @Method("GET")
     *
     */
    public function ExterneAction()
    {

        $menuMidi = $this->getDoctrine()->getRepository(menuMidi::class)->findOneBy([], ['id' => 'DESC']);

        return $this->render('menu/externe.html.twig', ['menuMidi' => $menuMidi]);
    }


    /**
     * @Route("/menu/scolaire", name="scolaire")
     * @Method("GET")
     *
     */
    public function ScolaireAction()
    {
        $menuScolaire = $this->getDoctrine()->getRepository(menuScolaire::class)->findOneBy([], ['id' => 'DESC']);

        return $this->render('menu/scolaire.html.twig', ['menuScolaire' => $menuScolaire]);
    }




    /**
     * @Route("/menu/resident", name="resident")
     * @Method("GET")
     *
     */
    public function ResidentAction()
    {
         $menuMidi = $this->getDoctrine()->getRepository(menuMidi::class)->findOneBy([], ['id' => 'DESC']);

         $menuSoir = $this->getDoctrine()->getRepository(menuSoir::class)->findOneBy([], ['id' => 'DESC']);

        return $this->render('menu/resident.html.twig', [
            'menuMidi' => $menuMidi,
            'menuSoir'=> $menuSoir,
        ]);
    }



}
