<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        // security_login
        if (preg_match('#^/(?P<_locale>fr)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_login')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'fr',));
        }

        // security_logout
        if (preg_match('#^/(?P<_locale>fr)/logout$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_logout')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'fr',));
        }

        // externe
        if (preg_match('#^/(?P<_locale>fr)/menu/externe$#s', $pathinfo, $matches)) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_externe;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'externe')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ExterneAction',  '_locale' => 'fr',));
        }
        not_externe:

        // scolaire
        if (preg_match('#^/(?P<_locale>fr)/menu/scolaire$#s', $pathinfo, $matches)) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_scolaire;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'scolaire')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ScolaireAction',  '_locale' => 'fr',));
        }
        not_scolaire:

        // resident
        if (preg_match('#^/(?P<_locale>fr)/menu/resident$#s', $pathinfo, $matches)) {
            if ('GET' !== $canonicalMethod) {
                $allow[] = 'GET';
                goto not_resident;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'resident')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ResidentAction',  '_locale' => 'fr',));
        }
        not_resident:

        // homepage
        if (preg_match('#^/(?P<_locale>fr)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'homepage')), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => 'menu/homepage.html.twig',  '_locale' => 'fr',));
        }

        if (0 === strpos($pathinfo, '/adminSonata')) {
            // sonata_admin_redirect
            if ('/adminSonata' === $trimmedPathinfo) {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
                }

                return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',  '_route' => 'sonata_admin_redirect',);
            }

            // sonata_admin_dashboard
            if ('/adminSonata/dashboard' === $pathinfo) {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',  '_route' => 'sonata_admin_dashboard',);
            }

            if (0 === strpos($pathinfo, '/adminSonata/core')) {
                if (0 === strpos($pathinfo, '/adminSonata/core/get-')) {
                    // sonata_admin_retrieve_form_element
                    if ('/adminSonata/core/get-form-field-element' === $pathinfo) {
                        return array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',  '_route' => 'sonata_admin_retrieve_form_element',);
                    }

                    // sonata_admin_short_object_information
                    if (0 === strpos($pathinfo, '/adminSonata/core/get-short-object-description') && preg_match('#^/adminSonata/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
                    }

                    // sonata_admin_retrieve_autocomplete_items
                    if ('/adminSonata/core/get-autocomplete-items' === $pathinfo) {
                        return array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',  '_route' => 'sonata_admin_retrieve_autocomplete_items',);
                    }

                }

                // sonata_admin_append_form_element
                if ('/adminSonata/core/append-form-field-element' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',  '_route' => 'sonata_admin_append_form_element',);
                }

                // sonata_admin_set_object_field_value
                if ('/adminSonata/core/set-object-field-value' === $pathinfo) {
                    return array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',  '_route' => 'sonata_admin_set_object_field_value',);
                }

            }

            // sonata_admin_search
            if ('/adminSonata/search' === $pathinfo) {
                return array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',  '_route' => 'sonata_admin_search',);
            }

            if (0 === strpos($pathinfo, '/adminSonata/app/menu')) {
                if (0 === strpos($pathinfo, '/adminSonata/app/menumidi')) {
                    // admin_app_menumidi_list
                    if ('/adminSonata/app/menumidi/list' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_list',  '_route' => 'admin_app_menumidi_list',);
                    }

                    // admin_app_menumidi_create
                    if ('/adminSonata/app/menumidi/create' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_create',  '_route' => 'admin_app_menumidi_create',);
                    }

                    // admin_app_menumidi_batch
                    if ('/adminSonata/app/menumidi/batch' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_batch',  '_route' => 'admin_app_menumidi_batch',);
                    }

                    // admin_app_menumidi_edit
                    if (preg_match('#^/adminSonata/app/menumidi/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_edit',));
                    }

                    // admin_app_menumidi_delete
                    if (preg_match('#^/adminSonata/app/menumidi/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_delete',));
                    }

                    // admin_app_menumidi_show
                    if (preg_match('#^/adminSonata/app/menumidi/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_show',));
                    }

                    // admin_app_menumidi_export
                    if ('/adminSonata/app/menumidi/export' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menuMidi',  '_sonata_name' => 'admin_app_menumidi_export',  '_route' => 'admin_app_menumidi_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/adminSonata/app/menusoir')) {
                    // admin_app_menusoir_list
                    if ('/adminSonata/app/menusoir/list' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_list',  '_route' => 'admin_app_menusoir_list',);
                    }

                    // admin_app_menusoir_create
                    if ('/adminSonata/app/menusoir/create' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_create',  '_route' => 'admin_app_menusoir_create',);
                    }

                    // admin_app_menusoir_batch
                    if ('/adminSonata/app/menusoir/batch' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_batch',  '_route' => 'admin_app_menusoir_batch',);
                    }

                    // admin_app_menusoir_edit
                    if (preg_match('#^/adminSonata/app/menusoir/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_edit',));
                    }

                    // admin_app_menusoir_delete
                    if (preg_match('#^/adminSonata/app/menusoir/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_delete',));
                    }

                    // admin_app_menusoir_show
                    if (preg_match('#^/adminSonata/app/menusoir/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_show',));
                    }

                    // admin_app_menusoir_export
                    if ('/adminSonata/app/menusoir/export' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menuSoir',  '_sonata_name' => 'admin_app_menusoir_export',  '_route' => 'admin_app_menusoir_export',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/adminSonata/app/menuscolaire')) {
                    // admin_app_menuscolaire_list
                    if ('/adminSonata/app/menuscolaire/list' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_list',  '_route' => 'admin_app_menuscolaire_list',);
                    }

                    // admin_app_menuscolaire_create
                    if ('/adminSonata/app/menuscolaire/create' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_create',  '_route' => 'admin_app_menuscolaire_create',);
                    }

                    // admin_app_menuscolaire_batch
                    if ('/adminSonata/app/menuscolaire/batch' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_batch',  '_route' => 'admin_app_menuscolaire_batch',);
                    }

                    // admin_app_menuscolaire_edit
                    if (preg_match('#^/adminSonata/app/menuscolaire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_edit',));
                    }

                    // admin_app_menuscolaire_delete
                    if (preg_match('#^/adminSonata/app/menuscolaire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_delete',));
                    }

                    // admin_app_menuscolaire_show
                    if (preg_match('#^/adminSonata/app/menuscolaire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_show',));
                    }

                    // admin_app_menuscolaire_export
                    if ('/adminSonata/app/menuscolaire/export' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menuScolaire',  '_sonata_name' => 'admin_app_menuscolaire_export',  '_route' => 'admin_app_menuscolaire_export',);
                    }

                }

            }

        }

        elseif (0 === strpos($pathinfo, '/login')) {
            // fos_user_security_login
            if ('/login' === $pathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_fos_user_security_login;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
            }
            not_fos_user_security_login:

            // fos_user_security_check
            if ('/login_check' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_fos_user_security_check;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
            }
            not_fos_user_security_check:

        }

        // fos_user_security_logout
        if ('/logout' === $pathinfo) {
            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                $allow = array_merge($allow, array('GET', 'POST'));
                goto not_fos_user_security_logout;
            }

            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }
        not_fos_user_security_logout:

        if (0 === strpos($pathinfo, '/profile')) {
            // fos_user_profile_show
            if ('/profile' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_fos_user_profile_show;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_route' => 'fos_user_profile_show',);
            }
            not_fos_user_profile_show:

            // fos_user_profile_edit
            if ('/profile/edit' === $pathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_fos_user_profile_edit;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_route' => 'fos_user_profile_edit',);
            }
            not_fos_user_profile_edit:

            // fos_user_change_password
            if ('/profile/change-password' === $pathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_fos_user_change_password;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_route' => 'fos_user_change_password',);
            }
            not_fos_user_change_password:

        }

        elseif (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if ('/register' === $trimmedPathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_fos_user_registration_register;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
            }
            not_fos_user_registration_register:

            // fos_user_registration_check_email
            if ('/register/check-email' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_fos_user_registration_check_email;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
            }
            not_fos_user_registration_check_email:

            if (0 === strpos($pathinfo, '/register/confirm')) {
                // fos_user_registration_confirm
                if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_fos_user_registration_confirm;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
                }
                not_fos_user_registration_confirm:

                // fos_user_registration_confirmed
                if ('/register/confirmed' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_fos_user_registration_confirmed;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
                }
                not_fos_user_registration_confirmed:

            }

        }

        elseif (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ('/resetting/request' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_fos_user_resetting_request;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_fos_user_resetting_reset;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
            }
            not_fos_user_resetting_reset:

            // fos_user_resetting_send_email
            if ('/resetting/send-email' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ('/resetting/check-email' === $pathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_fos_user_resetting_check_email;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
