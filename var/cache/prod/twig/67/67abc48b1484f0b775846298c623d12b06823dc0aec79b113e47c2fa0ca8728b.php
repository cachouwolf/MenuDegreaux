<?php

/* menu/homepage.html.twig */
class __TwigTemplate_46edcc5e938533c51d760c9c4c4d6221540a39340903eea8b9f53d1c17f25dd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67ab871cc970dac20d9a4f17bf1aa419a9e3d25612007d50706370f7e59ebfaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67ab871cc970dac20d9a4f17bf1aa419a9e3d25612007d50706370f7e59ebfaa->enter($__internal_67ab871cc970dac20d9a4f17bf1aa419a9e3d25612007d50706370f7e59ebfaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_67ab871cc970dac20d9a4f17bf1aa419a9e3d25612007d50706370f7e59ebfaa->leave($__internal_67ab871cc970dac20d9a4f17bf1aa419a9e3d25612007d50706370f7e59ebfaa_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_c0f34ad531279f281d0884f6a2a657a0615fdeb3d2d28ea2826494200ea35b88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0f34ad531279f281d0884f6a2a657a0615fdeb3d2d28ea2826494200ea35b88->enter($__internal_c0f34ad531279f281d0884f6a2a657a0615fdeb3d2d28ea2826494200ea35b88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_c0f34ad531279f281d0884f6a2a657a0615fdeb3d2d28ea2826494200ea35b88->leave($__internal_c0f34ad531279f281d0884f6a2a657a0615fdeb3d2d28ea2826494200ea35b88_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_7d7783dac315772b05f44caa74544905bf36b193c98d39bfdcc6594c639ff276 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d7783dac315772b05f44caa74544905bf36b193c98d39bfdcc6594c639ff276->enter($__internal_7d7783dac315772b05f44caa74544905bf36b193c98d39bfdcc6594c639ff276_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
";
        
        $__internal_7d7783dac315772b05f44caa74544905bf36b193c98d39bfdcc6594c639ff276->leave($__internal_7d7783dac315772b05f44caa74544905bf36b193c98d39bfdcc6594c639ff276_prof);

    }

    public function getTemplateName()
    {
        return "menu/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 25,  70 => 18,  60 => 11,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{% block main %}
    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"{{ path('externe') }}\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"{{ path('scolaire') }}\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"{{ path('resident') }}\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
{% endblock %}
", "menu/homepage.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/homepage.html.twig");
    }
}
