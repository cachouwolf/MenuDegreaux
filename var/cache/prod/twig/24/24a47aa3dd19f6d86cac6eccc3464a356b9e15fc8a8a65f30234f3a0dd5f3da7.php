<?php

/* menu/homepage.html.twig */
class __TwigTemplate_baa0d80be13a090ee628fb898acfac3b07d6367c8417e6107e19bf7d58a7370e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        echo "homepage";
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "menu/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 25,  55 => 18,  45 => 11,  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "menu/homepage.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/homepage.html.twig");
    }
}
