<?php

/* menu/externe.html.twig */
class __TwigTemplate_fde72f299c3929b5a8cf7abd5b5e3c7bb6106f62604b9b2c0ee953cfb8b819b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/externe.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        echo "menu_externe";
    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        // line 6
        echo "        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "lundiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mardiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "samediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimanchePlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : null), "dimancheDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>




";
    }

    // line 96
    public function block_sidebar($context, array $blocks = array())
    {
        // line 97
        echo "    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"";
        // line 101
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            Résident
        </a>
    </div>



";
    }

    public function getTemplateName()
    {
        return "menu/externe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 111,  231 => 106,  223 => 101,  217 => 97,  214 => 96,  200 => 84,  196 => 83,  192 => 82,  188 => 81,  175 => 71,  171 => 70,  167 => 69,  163 => 68,  153 => 61,  149 => 60,  145 => 59,  141 => 58,  131 => 51,  127 => 50,  123 => 49,  119 => 48,  106 => 38,  102 => 37,  98 => 36,  94 => 35,  84 => 28,  80 => 27,  76 => 26,  72 => 25,  62 => 18,  58 => 17,  54 => 16,  50 => 15,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "menu/externe.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/externe.html.twig");
    }
}
