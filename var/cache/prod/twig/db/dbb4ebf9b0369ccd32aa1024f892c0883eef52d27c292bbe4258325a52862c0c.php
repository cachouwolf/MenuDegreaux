<?php

/* menu/externe.html.twig */
class __TwigTemplate_35f97f8f54fc536d7514f5bd3fd28b03e41c839545abb96e1e470392cf15b580 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/externe.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad29722e98e8028d0d6cc682c0c7486d53e132c6b2af6c4c4b0072211d392a2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad29722e98e8028d0d6cc682c0c7486d53e132c6b2af6c4c4b0072211d392a2b->enter($__internal_ad29722e98e8028d0d6cc682c0c7486d53e132c6b2af6c4c4b0072211d392a2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/externe.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ad29722e98e8028d0d6cc682c0c7486d53e132c6b2af6c4c4b0072211d392a2b->leave($__internal_ad29722e98e8028d0d6cc682c0c7486d53e132c6b2af6c4c4b0072211d392a2b_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_60db460975cc8c37d4a2a3a8f9db7e65e55042d1eab53e947c193b7979d3e251 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60db460975cc8c37d4a2a3a8f9db7e65e55042d1eab53e947c193b7979d3e251->enter($__internal_60db460975cc8c37d4a2a3a8f9db7e65e55042d1eab53e947c193b7979d3e251_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_externe";
        
        $__internal_60db460975cc8c37d4a2a3a8f9db7e65e55042d1eab53e947c193b7979d3e251->leave($__internal_60db460975cc8c37d4a2a3a8f9db7e65e55042d1eab53e947c193b7979d3e251_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_7039bcfd99e1ee4371355deb2cf9fe58fda329338c946806a07240ee7ee44c41 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7039bcfd99e1ee4371355deb2cf9fe58fda329338c946806a07240ee7ee44c41->enter($__internal_7039bcfd99e1ee4371355deb2cf9fe58fda329338c946806a07240ee7ee44c41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimanchePlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>




";
        
        $__internal_7039bcfd99e1ee4371355deb2cf9fe58fda329338c946806a07240ee7ee44c41->leave($__internal_7039bcfd99e1ee4371355deb2cf9fe58fda329338c946806a07240ee7ee44c41_prof);

    }

    // line 96
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_7294cfc7c47fdc49f6876b5a96e10e7bcb613fec73b765a3e1741dab7d2b7e97 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7294cfc7c47fdc49f6876b5a96e10e7bcb613fec73b765a3e1741dab7d2b7e97->enter($__internal_7294cfc7c47fdc49f6876b5a96e10e7bcb613fec73b765a3e1741dab7d2b7e97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 97
        echo "    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"";
        // line 101
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            Résident
        </a>
    </div>



";
        
        $__internal_7294cfc7c47fdc49f6876b5a96e10e7bcb613fec73b765a3e1741dab7d2b7e97->leave($__internal_7294cfc7c47fdc49f6876b5a96e10e7bcb613fec73b765a3e1741dab7d2b7e97_prof);

    }

    public function getTemplateName()
    {
        return "menu/externe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  260 => 111,  252 => 106,  244 => 101,  238 => 97,  232 => 96,  215 => 84,  211 => 83,  207 => 82,  203 => 81,  190 => 71,  186 => 70,  182 => 69,  178 => 68,  168 => 61,  164 => 60,  160 => 59,  156 => 58,  146 => 51,  142 => 50,  138 => 49,  134 => 48,  121 => 38,  117 => 37,  113 => 36,  109 => 35,  99 => 28,  95 => 27,  91 => 26,  87 => 25,  77 => 18,  73 => 17,  69 => 16,  65 => 15,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'menu_externe' %}

{% block main %}
        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.lundiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.lundiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.lundiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.lundiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mardiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mardiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mardiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mardiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mercrediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mercrediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.jeudiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.jeudiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuMidi.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuMidi.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediDessert }}</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.samediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.samediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.samediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.samediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.dimancheEntree }}</div>
                        <div class=\" text\">{{ menuMidi.dimanchePlat }}</div>
                        <div class=\"text\">{{ menuMidi.dimancheAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.dimancheDessert }}</div>
                    </div>
                </div>

            </div>




{% endblock %}


{% block sidebar %}
    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"{{ path('externe') }}\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"{{ path('scolaire') }}\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"{{ path('resident') }}\" >
            Résident
        </a>
    </div>



{% endblock %}
", "menu/externe.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/externe.html.twig");
    }
}
