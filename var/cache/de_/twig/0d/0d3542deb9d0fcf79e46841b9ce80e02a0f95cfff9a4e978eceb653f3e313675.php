<?php

/* SonataAdminBundle:CRUD:base_standard_edit_field.html.twig */
class __TwigTemplate_8178301632ebbc9715d5a06bf44800033e85e2413611a54cdfcea5f517ede1b3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'label' => array($this, 'block_label'),
            'field' => array($this, 'block_field'),
            'help' => array($this, 'block_help'),
            'errors' => array($this, 'block_errors'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_384603446afc6db766506c1e58fa201961572e48b669f3938a32c812342e8bb8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_384603446afc6db766506c1e58fa201961572e48b669f3938a32c812342e8bb8->enter($__internal_384603446afc6db766506c1e58fa201961572e48b669f3938a32c812342e8bb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig"));

        $__internal_ac626ed6076d3559148b6888f51340cb312cc473487c2ef5416730c983cb1d7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac626ed6076d3559148b6888f51340cb312cc473487c2ef5416730c983cb1d7d->enter($__internal_ac626ed6076d3559148b6888f51340cb312cc473487c2ef5416730c983cb1d7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig"));

        // line 11
        echo "
<div class=\"form-group";
        // line 12
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "var", array()), "errors", array())) > 0)) {
            echo " has-error";
        }
        echo "\" id=\"sonata-ba-field-container-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "vars", array()), "id", array()), "html", null, true);
        echo "\">
    ";
        // line 13
        $this->displayBlock('label', $context, $blocks);
        // line 20
        echo "
    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-";
        // line 21
        echo twig_escape_filter($this->env, (isset($context["edit"]) ? $context["edit"] : $this->getContext($context, "edit")), "html", null, true);
        echo "-";
        echo twig_escape_filter($this->env, (isset($context["inline"]) ? $context["inline"] : $this->getContext($context, "inline")), "html", null, true);
        echo " ";
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), "vars", array()), "errors", array())) > 0)) {
            echo "sonata-ba-field-error";
        }
        echo "\">

        ";
        // line 23
        $this->displayBlock('field', $context, $blocks);
        // line 24
        echo "
        ";
        // line 25
        if ($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "help", array())) {
            // line 26
            echo "            <span class=\"help-block\">";
            $this->displayBlock('help', $context, $blocks);
            echo "</span>
        ";
        }
        // line 28
        echo "
        <div class=\"sonata-ba-field-error-messages\">
            ";
        // line 30
        $this->displayBlock('errors', $context, $blocks);
        // line 31
        echo "        </div>

    </div>
</div>
";
        
        $__internal_384603446afc6db766506c1e58fa201961572e48b669f3938a32c812342e8bb8->leave($__internal_384603446afc6db766506c1e58fa201961572e48b669f3938a32c812342e8bb8_prof);

        
        $__internal_ac626ed6076d3559148b6888f51340cb312cc473487c2ef5416730c983cb1d7d->leave($__internal_ac626ed6076d3559148b6888f51340cb312cc473487c2ef5416730c983cb1d7d_prof);

    }

    // line 13
    public function block_label($context, array $blocks = array())
    {
        $__internal_6568f24c2616d065fbcfccc8d889b5655115946e7aebf53deef3f059f9a87490 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6568f24c2616d065fbcfccc8d889b5655115946e7aebf53deef3f059f9a87490->enter($__internal_6568f24c2616d065fbcfccc8d889b5655115946e7aebf53deef3f059f9a87490_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_7916ab8c4cc38498ff9565a702d9c37846d9c35a5c18cb3f68379ce056b23493 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7916ab8c4cc38498ff9565a702d9c37846d9c35a5c18cb3f68379ce056b23493->enter($__internal_7916ab8c4cc38498ff9565a702d9c37846d9c35a5c18cb3f68379ce056b23493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 14
        echo "        ";
        if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "name", array(), "any", true, true)) {
            // line 15
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label', (twig_test_empty($_label_ = $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "name", array())) ? array() : array("label" => $_label_)));
            echo "
        ";
        } else {
            // line 17
            echo "            ";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'label');
            echo "
        ";
        }
        // line 19
        echo "    ";
        
        $__internal_7916ab8c4cc38498ff9565a702d9c37846d9c35a5c18cb3f68379ce056b23493->leave($__internal_7916ab8c4cc38498ff9565a702d9c37846d9c35a5c18cb3f68379ce056b23493_prof);

        
        $__internal_6568f24c2616d065fbcfccc8d889b5655115946e7aebf53deef3f059f9a87490->leave($__internal_6568f24c2616d065fbcfccc8d889b5655115946e7aebf53deef3f059f9a87490_prof);

    }

    // line 23
    public function block_field($context, array $blocks = array())
    {
        $__internal_93c5f5a780bfa74ef4f967fda3aef82b12f53093572266eff4001bfe12ba9e71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93c5f5a780bfa74ef4f967fda3aef82b12f53093572266eff4001bfe12ba9e71->enter($__internal_93c5f5a780bfa74ef4f967fda3aef82b12f53093572266eff4001bfe12ba9e71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_6007326713376c04900a3bba121b67841e58d81a87ecd3f459aa816cbfd515ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6007326713376c04900a3bba121b67841e58d81a87ecd3f459aa816cbfd515ac->enter($__internal_6007326713376c04900a3bba121b67841e58d81a87ecd3f459aa816cbfd515ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget');
        
        $__internal_6007326713376c04900a3bba121b67841e58d81a87ecd3f459aa816cbfd515ac->leave($__internal_6007326713376c04900a3bba121b67841e58d81a87ecd3f459aa816cbfd515ac_prof);

        
        $__internal_93c5f5a780bfa74ef4f967fda3aef82b12f53093572266eff4001bfe12ba9e71->leave($__internal_93c5f5a780bfa74ef4f967fda3aef82b12f53093572266eff4001bfe12ba9e71_prof);

    }

    // line 26
    public function block_help($context, array $blocks = array())
    {
        $__internal_6b91422ec00fd942ed8d184508a19a939217f04a008335304fe1673de30d7658 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b91422ec00fd942ed8d184508a19a939217f04a008335304fe1673de30d7658->enter($__internal_6b91422ec00fd942ed8d184508a19a939217f04a008335304fe1673de30d7658_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "help"));

        $__internal_c8ebf99f9a546f820e77a30019c2434a42fbb33910c96d1bc71d5e4253e05eaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8ebf99f9a546f820e77a30019c2434a42fbb33910c96d1bc71d5e4253e05eaa->enter($__internal_c8ebf99f9a546f820e77a30019c2434a42fbb33910c96d1bc71d5e4253e05eaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "help"));

        echo $this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "help", array());
        
        $__internal_c8ebf99f9a546f820e77a30019c2434a42fbb33910c96d1bc71d5e4253e05eaa->leave($__internal_c8ebf99f9a546f820e77a30019c2434a42fbb33910c96d1bc71d5e4253e05eaa_prof);

        
        $__internal_6b91422ec00fd942ed8d184508a19a939217f04a008335304fe1673de30d7658->leave($__internal_6b91422ec00fd942ed8d184508a19a939217f04a008335304fe1673de30d7658_prof);

    }

    // line 30
    public function block_errors($context, array $blocks = array())
    {
        $__internal_1068900210e169697acc27a358c4daf0a73134e84fee412f2190a3771825ba53 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1068900210e169697acc27a358c4daf0a73134e84fee412f2190a3771825ba53->enter($__internal_1068900210e169697acc27a358c4daf0a73134e84fee412f2190a3771825ba53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        $__internal_261bff6c52236863f7a18c6bed32e7cd1ef0da9b012596a6de03ec698ecc6199 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_261bff6c52236863f7a18c6bed32e7cd1ef0da9b012596a6de03ec698ecc6199->enter($__internal_261bff6c52236863f7a18c6bed32e7cd1ef0da9b012596a6de03ec698ecc6199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "errors"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'errors');
        
        $__internal_261bff6c52236863f7a18c6bed32e7cd1ef0da9b012596a6de03ec698ecc6199->leave($__internal_261bff6c52236863f7a18c6bed32e7cd1ef0da9b012596a6de03ec698ecc6199_prof);

        
        $__internal_1068900210e169697acc27a358c4daf0a73134e84fee412f2190a3771825ba53->leave($__internal_1068900210e169697acc27a358c4daf0a73134e84fee412f2190a3771825ba53_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 30,  141 => 26,  123 => 23,  113 => 19,  107 => 17,  101 => 15,  98 => 14,  89 => 13,  75 => 31,  73 => 30,  69 => 28,  63 => 26,  61 => 25,  58 => 24,  56 => 23,  45 => 21,  42 => 20,  40 => 13,  32 => 12,  29 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

<div class=\"form-group{% if field_element.var.errors|length > 0%} has-error{%endif%}\" id=\"sonata-ba-field-container-{{ field_element.vars.id }}\">
    {% block label %}
        {% if field_description.options.name is defined %}
            {{ form_label(field_element, field_description.options.name) }}
        {% else %}
            {{ form_label(field_element) }}
        {% endif %}
    {% endblock %}

    <div class=\"col-sm-10 col-md-5 sonata-ba-field sonata-ba-field-{{ edit }}-{{ inline }} {% if field_element.vars.errors|length > 0 %}sonata-ba-field-error{% endif %}\">

        {% block field %}{{ form_widget(field_element) }}{% endblock %}

        {% if field_description.help %}
            <span class=\"help-block\">{% block help %}{{ field_description.help|raw }}{% endblock %}</span>
        {% endif %}

        <div class=\"sonata-ba-field-error-messages\">
            {% block errors %}{{ form_errors(field_element) }}{% endblock %}
        </div>

    </div>
</div>
", "SonataAdminBundle:CRUD:base_standard_edit_field.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_standard_edit_field.html.twig");
    }
}
