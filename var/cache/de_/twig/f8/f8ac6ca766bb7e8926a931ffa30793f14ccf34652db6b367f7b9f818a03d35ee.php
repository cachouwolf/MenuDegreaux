<?php

/* SonataAdminBundle:CRUD:list_datetime.html.twig */
class __TwigTemplate_4830f24a283298c22c4365b343a5a871a75a8a5f5e348eb3b66f002153d21c62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_datetime.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3365a8f0313afa80cdbdabe8b379c3023b8b88b0135e0fa2786a1456d5089188 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3365a8f0313afa80cdbdabe8b379c3023b8b88b0135e0fa2786a1456d5089188->enter($__internal_3365a8f0313afa80cdbdabe8b379c3023b8b88b0135e0fa2786a1456d5089188_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_datetime.html.twig"));

        $__internal_f7c04b82fc13e7db8f7cbc201ffc2070a39b9d0a391bd6e0c1598b98c29975b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f7c04b82fc13e7db8f7cbc201ffc2070a39b9d0a391bd6e0c1598b98c29975b7->enter($__internal_f7c04b82fc13e7db8f7cbc201ffc2070a39b9d0a391bd6e0c1598b98c29975b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_datetime.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3365a8f0313afa80cdbdabe8b379c3023b8b88b0135e0fa2786a1456d5089188->leave($__internal_3365a8f0313afa80cdbdabe8b379c3023b8b88b0135e0fa2786a1456d5089188_prof);

        
        $__internal_f7c04b82fc13e7db8f7cbc201ffc2070a39b9d0a391bd6e0c1598b98c29975b7->leave($__internal_f7c04b82fc13e7db8f7cbc201ffc2070a39b9d0a391bd6e0c1598b98c29975b7_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_a84a79160383cb833b2cd9590a8ae3f00f48b5578ae7aa5b563db299942fd52d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a84a79160383cb833b2cd9590a8ae3f00f48b5578ae7aa5b563db299942fd52d->enter($__internal_a84a79160383cb833b2cd9590a8ae3f00f48b5578ae7aa5b563db299942fd52d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_d037d6df9432efc5b15fa304f545c1586835626ef2bb11f4fa78ba5cf3d99430 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d037d6df9432efc5b15fa304f545c1586835626ef2bb11f4fa78ba5cf3d99430->enter($__internal_d037d6df9432efc5b15fa304f545c1586835626ef2bb11f4fa78ba5cf3d99430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } elseif ($this->getAttribute($this->getAttribute(        // line 17
(isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            $context["timezone"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "timezone", array(), "any", true, true)) ? ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "timezone", array())) : (null));
            // line 19
            echo "        ";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "format", array()), (isset($context["timezone"]) ? $context["timezone"] : $this->getContext($context, "timezone"))), "html", null, true);
        } elseif ($this->getAttribute($this->getAttribute(        // line 20
(isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "timezone", array(), "any", true, true)) {
            // line 21
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), null, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "timezone", array())), "html", null, true);
        } else {
            // line 23
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
        }
        
        $__internal_d037d6df9432efc5b15fa304f545c1586835626ef2bb11f4fa78ba5cf3d99430->leave($__internal_d037d6df9432efc5b15fa304f545c1586835626ef2bb11f4fa78ba5cf3d99430_prof);

        
        $__internal_a84a79160383cb833b2cd9590a8ae3f00f48b5578ae7aa5b563db299942fd52d->leave($__internal_a84a79160383cb833b2cd9590a8ae3f00f48b5578ae7aa5b563db299942fd52d_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 23,  61 => 21,  59 => 20,  56 => 19,  54 => 18,  52 => 17,  50 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {% set timezone = field_description.options.timezone is defined ? field_description.options.timezone : null %}
        {{ value|date(field_description.options.format, timezone) }}
    {%- elseif field_description.options.timezone is defined -%}
        {{ value|date(null, field_description.options.timezone) }}
    {%- else -%}
        {{ value|date }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_datetime.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_datetime.html.twig");
    }
}
