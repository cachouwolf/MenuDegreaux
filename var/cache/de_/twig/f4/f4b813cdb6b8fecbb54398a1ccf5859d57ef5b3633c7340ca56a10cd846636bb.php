<?php

/* SonataAdminBundle:Block:block_stats.html.twig */
class __TwigTemplate_f820ae75c76f4c90b7b05a609f0a93deaa51665aea7e4db54a1aaad13837ad02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataAdminBundle:Block:block_stats.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_521f1dc32a46f10c96dc04d12844b708cf4be4766ae6bbf710ad09760c836d97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_521f1dc32a46f10c96dc04d12844b708cf4be4766ae6bbf710ad09760c836d97->enter($__internal_521f1dc32a46f10c96dc04d12844b708cf4be4766ae6bbf710ad09760c836d97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Block:block_stats.html.twig"));

        $__internal_891aedd98f82b32d8ed868ba443a26e6db3568b37e30e7e0f82fc6fc81a2b0f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_891aedd98f82b32d8ed868ba443a26e6db3568b37e30e7e0f82fc6fc81a2b0f6->enter($__internal_891aedd98f82b32d8ed868ba443a26e6db3568b37e30e7e0f82fc6fc81a2b0f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Block:block_stats.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_521f1dc32a46f10c96dc04d12844b708cf4be4766ae6bbf710ad09760c836d97->leave($__internal_521f1dc32a46f10c96dc04d12844b708cf4be4766ae6bbf710ad09760c836d97_prof);

        
        $__internal_891aedd98f82b32d8ed868ba443a26e6db3568b37e30e7e0f82fc6fc81a2b0f6->leave($__internal_891aedd98f82b32d8ed868ba443a26e6db3568b37e30e7e0f82fc6fc81a2b0f6_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_ca80c552b22a6acf1e61dd7e9e5936b79bbaef6d6f1680a29265bbac72b33049 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca80c552b22a6acf1e61dd7e9e5936b79bbaef6d6f1680a29265bbac72b33049->enter($__internal_ca80c552b22a6acf1e61dd7e9e5936b79bbaef6d6f1680a29265bbac72b33049_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_096c41f3788f86f5f5b1f6c995111246668320ddf88ab210ebf82e14df361521 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_096c41f3788f86f5f5b1f6c995111246668320ddf88ab210ebf82e14df361521->enter($__internal_096c41f3788f86f5f5b1f6c995111246668320ddf88ab210ebf82e14df361521_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <!-- small box -->
    <div class=\"small-box ";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "color", array()), "html", null, true);
        echo "\">
        <div class=\"inner\">
            <h3>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["pager"]) ? $context["pager"] : $this->getContext($context, "pager")), "count", array(), "method"), "html", null, true);
        echo "</h3>

            <p>";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "text", array()), array(), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())), "html", null, true);
        echo "</p>
        </div>
        <div class=\"icon\">
            <i class=\"fa ";
        // line 23
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "icon", array()), "html", null, true);
        echo "\"></i>
        </div>
        <a href=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "list", 1 => array("filter" => $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "filters", array()))), "method"), "html", null, true);
        echo "\" class=\"small-box-footer\">
            ";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("stats_view_more", array(), "SonataAdminBundle"), "html", null, true);
        echo " <i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i>
        </a>
    </div>
";
        
        $__internal_096c41f3788f86f5f5b1f6c995111246668320ddf88ab210ebf82e14df361521->leave($__internal_096c41f3788f86f5f5b1f6c995111246668320ddf88ab210ebf82e14df361521_prof);

        
        $__internal_ca80c552b22a6acf1e61dd7e9e5936b79bbaef6d6f1680a29265bbac72b33049->leave($__internal_ca80c552b22a6acf1e61dd7e9e5936b79bbaef6d6f1680a29265bbac72b33049_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Block:block_stats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 26,  72 => 25,  67 => 23,  61 => 20,  56 => 18,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <!-- small box -->
    <div class=\"small-box {{ settings.color }}\">
        <div class=\"inner\">
            <h3>{{ pager.count() }}</h3>

            <p>{{ settings.text|trans({}, admin.translationDomain) }}</p>
        </div>
        <div class=\"icon\">
            <i class=\"fa {{ settings.icon }}\"></i>
        </div>
        <a href=\"{{ admin.generateUrl('list', {filter: settings.filters}) }}\" class=\"small-box-footer\">
            {{ 'stats_view_more'|trans({}, 'SonataAdminBundle') }} <i class=\"fa fa-arrow-circle-right\" aria-hidden=\"true\"></i>
        </a>
    </div>
{% endblock %}
", "SonataAdminBundle:Block:block_stats.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Block/block_stats.html.twig");
    }
}
