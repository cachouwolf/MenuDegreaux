<?php

/* SonataAdminBundle:Pager:base_results.html.twig */
class __TwigTemplate_f80fe491ce52f0ad3a62757afab6a8554f9faccb042eba9068a4fdecc6cee40b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'num_pages' => array($this, 'block_num_pages'),
            'num_results' => array($this, 'block_num_results'),
            'max_per_page' => array($this, 'block_max_per_page'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae5284627eb76b8c7b726255c73739a2e40d1dcc27c8aba85b7f7eba102004aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae5284627eb76b8c7b726255c73739a2e40d1dcc27c8aba85b7f7eba102004aa->enter($__internal_ae5284627eb76b8c7b726255c73739a2e40d1dcc27c8aba85b7f7eba102004aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:base_results.html.twig"));

        $__internal_ce53235077b6f1f16016a8bb0a2d31d35664a9551c3dd91902aeb685926fe77c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce53235077b6f1f16016a8bb0a2d31d35664a9551c3dd91902aeb685926fe77c->enter($__internal_ce53235077b6f1f16016a8bb0a2d31d35664a9551c3dd91902aeb685926fe77c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:base_results.html.twig"));

        // line 11
        echo "
";
        // line 12
        $this->displayBlock('num_pages', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('num_results', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('max_per_page', $context, $blocks);
        
        $__internal_ae5284627eb76b8c7b726255c73739a2e40d1dcc27c8aba85b7f7eba102004aa->leave($__internal_ae5284627eb76b8c7b726255c73739a2e40d1dcc27c8aba85b7f7eba102004aa_prof);

        
        $__internal_ce53235077b6f1f16016a8bb0a2d31d35664a9551c3dd91902aeb685926fe77c->leave($__internal_ce53235077b6f1f16016a8bb0a2d31d35664a9551c3dd91902aeb685926fe77c_prof);

    }

    // line 12
    public function block_num_pages($context, array $blocks = array())
    {
        $__internal_0826c8eca15bfbf28fe15cc0d9d6fe4109f88fd4690db36c598ac4f3932ca5a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0826c8eca15bfbf28fe15cc0d9d6fe4109f88fd4690db36c598ac4f3932ca5a8->enter($__internal_0826c8eca15bfbf28fe15cc0d9d6fe4109f88fd4690db36c598ac4f3932ca5a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        $__internal_ca04ffd1e6c4d5f5291b8165e2918945ec0bdb3fe8408fcf42fed4c3c634fad1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca04ffd1e6c4d5f5291b8165e2918945ec0bdb3fe8408fcf42fed4c3c634fad1->enter($__internal_ca04ffd1e6c4d5f5291b8165e2918945ec0bdb3fe8408fcf42fed4c3c634fad1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        // line 13
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
        echo "
    &nbsp;-&nbsp;
";
        
        $__internal_ca04ffd1e6c4d5f5291b8165e2918945ec0bdb3fe8408fcf42fed4c3c634fad1->leave($__internal_ca04ffd1e6c4d5f5291b8165e2918945ec0bdb3fe8408fcf42fed4c3c634fad1_prof);

        
        $__internal_0826c8eca15bfbf28fe15cc0d9d6fe4109f88fd4690db36c598ac4f3932ca5a8->leave($__internal_0826c8eca15bfbf28fe15cc0d9d6fe4109f88fd4690db36c598ac4f3932ca5a8_prof);

    }

    // line 17
    public function block_num_results($context, array $blocks = array())
    {
        $__internal_08cd8c426ccc94b0f83eff79bc4f17086189876492e5e4d535833bf687cc0c1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08cd8c426ccc94b0f83eff79bc4f17086189876492e5e4d535833bf687cc0c1a->enter($__internal_08cd8c426ccc94b0f83eff79bc4f17086189876492e5e4d535833bf687cc0c1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        $__internal_9580066785ada7e2409eb5a66dcac9af7b9449e53cbd84e46e1aba6f308233b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9580066785ada7e2409eb5a66dcac9af7b9449e53cbd84e46e1aba6f308233b6->enter($__internal_9580066785ada7e2409eb5a66dcac9af7b9449e53cbd84e46e1aba6f308233b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("list_results_count", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_9580066785ada7e2409eb5a66dcac9af7b9449e53cbd84e46e1aba6f308233b6->leave($__internal_9580066785ada7e2409eb5a66dcac9af7b9449e53cbd84e46e1aba6f308233b6_prof);

        
        $__internal_08cd8c426ccc94b0f83eff79bc4f17086189876492e5e4d535833bf687cc0c1a->leave($__internal_08cd8c426ccc94b0f83eff79bc4f17086189876492e5e4d535833bf687cc0c1a_prof);

    }

    // line 22
    public function block_max_per_page($context, array $blocks = array())
    {
        $__internal_9a3d4160e53cb90345d41af687161fc4c3bf71c4f12c2f440fc6c703d5aae109 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a3d4160e53cb90345d41af687161fc4c3bf71c4f12c2f440fc6c703d5aae109->enter($__internal_9a3d4160e53cb90345d41af687161fc4c3bf71c4f12c2f440fc6c703d5aae109_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "max_per_page"));

        $__internal_bc044eb172683c860516b8af14f0c4235386bf7e359bc10f451a51ae9a521f69 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc044eb172683c860516b8af14f0c4235386bf7e359bc10f451a51ae9a521f69->enter($__internal_bc044eb172683c860516b8af14f0c4235386bf7e359bc10f451a51ae9a521f69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "max_per_page"));

        // line 23
        echo "    <label class=\"control-label\" for=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "uniqid", array()), "html", null, true);
        echo "_per_page\">";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_per_page", array(), "SonataAdminBundle");
        echo "</label>
    <select class=\"per-page small form-control\" id=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "uniqid", array()), "html", null, true);
        echo "_per_page\" style=\"width: auto\">
        ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getperpageoptions", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["per_page"]) {
            // line 26
            echo "            <option ";
            if (($context["per_page"] == $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "maxperpage", array()))) {
                echo "selected=\"selected\"";
            }
            echo " value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "list", 1 => array("filter" => twig_array_merge($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "values", array()), array("_page" => 1, "_per_page" => $context["per_page"])))), "method"), "html", null, true);
            echo "\">";
            // line 27
            echo twig_escape_filter($this->env, $context["per_page"], "html", null, true);
            // line 28
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['per_page'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "    </select>
";
        
        $__internal_bc044eb172683c860516b8af14f0c4235386bf7e359bc10f451a51ae9a521f69->leave($__internal_bc044eb172683c860516b8af14f0c4235386bf7e359bc10f451a51ae9a521f69_prof);

        
        $__internal_9a3d4160e53cb90345d41af687161fc4c3bf71c4f12c2f440fc6c703d5aae109->leave($__internal_9a3d4160e53cb90345d41af687161fc4c3bf71c4f12c2f440fc6c703d5aae109_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  140 => 30,  133 => 28,  131 => 27,  123 => 26,  119 => 25,  115 => 24,  108 => 23,  99 => 22,  88 => 19,  85 => 18,  76 => 17,  60 => 13,  51 => 12,  41 => 22,  38 => 21,  36 => 17,  33 => 16,  31 => 12,  28 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% block num_pages %}
    {{ admin.datagrid.pager.page }} / {{ admin.datagrid.pager.lastpage }}
    &nbsp;-&nbsp;
{% endblock %}

{% block num_results %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}list_results_count{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block max_per_page %}
    <label class=\"control-label\" for=\"{{ admin.uniqid }}_per_page\">{% trans from 'SonataAdminBundle' %}label_per_page{% endtrans %}</label>
    <select class=\"per-page small form-control\" id=\"{{ admin.uniqid }}_per_page\" style=\"width: auto\">
        {% for per_page in admin.getperpageoptions %}
            <option {% if per_page == admin.datagrid.pager.maxperpage %}selected=\"selected\"{% endif %} value=\"{{ admin.generateUrl('list', {'filter': admin.datagrid.values|merge({'_page': 1, '_per_page': per_page})}) }}\">
                {{- per_page -}}
            </option>
        {% endfor %}
    </select>
{% endblock %}
", "SonataAdminBundle:Pager:base_results.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Pager/base_results.html.twig");
    }
}
