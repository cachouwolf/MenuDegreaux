<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_7861f70e6d1c3b3c4ab6d4dd676731e4393632e180074939cff3c9615039ab3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e81e25e777f7365f42dfa5a988f0d80f9219decdda759c9e4c46c2d3ef3af71f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e81e25e777f7365f42dfa5a988f0d80f9219decdda759c9e4c46c2d3ef3af71f->enter($__internal_e81e25e777f7365f42dfa5a988f0d80f9219decdda759c9e4c46c2d3ef3af71f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        $__internal_08a048d034b06fd8d27f11f1bb49c776cab2170e8a7cb13f760767b24486c1ce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08a048d034b06fd8d27f11f1bb49c776cab2170e8a7cb13f760767b24486c1ce->enter($__internal_08a048d034b06fd8d27f11f1bb49c776cab2170e8a7cb13f760767b24486c1ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_e81e25e777f7365f42dfa5a988f0d80f9219decdda759c9e4c46c2d3ef3af71f->leave($__internal_e81e25e777f7365f42dfa5a988f0d80f9219decdda759c9e4c46c2d3ef3af71f_prof);

        
        $__internal_08a048d034b06fd8d27f11f1bb49c776cab2170e8a7cb13f760767b24486c1ce->leave($__internal_08a048d034b06fd8d27f11f1bb49c776cab2170e8a7cb13f760767b24486c1ce_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "@Twig/Exception/error.json.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.json.twig");
    }
}
