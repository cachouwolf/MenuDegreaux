<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_d64a7435f7ab946fe3962bf9febf21e3bf256407da7b4eb78f244eb155e60a10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_40989441a6afa5e5e02cf6873057d8fb5e70e9d5a18f72c36f1165940319bd6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40989441a6afa5e5e02cf6873057d8fb5e70e9d5a18f72c36f1165940319bd6b->enter($__internal_40989441a6afa5e5e02cf6873057d8fb5e70e9d5a18f72c36f1165940319bd6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_10bf2f37595fad2652fcb802cdb05a3a27fc011b307a07e85a31d011648de900 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10bf2f37595fad2652fcb802cdb05a3a27fc011b307a07e85a31d011648de900->enter($__internal_10bf2f37595fad2652fcb802cdb05a3a27fc011b307a07e85a31d011648de900_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_40989441a6afa5e5e02cf6873057d8fb5e70e9d5a18f72c36f1165940319bd6b->leave($__internal_40989441a6afa5e5e02cf6873057d8fb5e70e9d5a18f72c36f1165940319bd6b_prof);

        
        $__internal_10bf2f37595fad2652fcb802cdb05a3a27fc011b307a07e85a31d011648de900->leave($__internal_10bf2f37595fad2652fcb802cdb05a3a27fc011b307a07e85a31d011648de900_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_attributes.html.php");
    }
}
