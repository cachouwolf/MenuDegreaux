<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_83de6488f36481c5ea7cf530afce78efd34ca58393232e29bbd58115f4d17cd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0fea2fdf0815eded952cdebd4ee08c634a70c715e13f3a320ebb83210382829 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0fea2fdf0815eded952cdebd4ee08c634a70c715e13f3a320ebb83210382829->enter($__internal_f0fea2fdf0815eded952cdebd4ee08c634a70c715e13f3a320ebb83210382829_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_cf978d35016d1b4246f9961af76b8ded966d0d5c611803fd2f5ffdbe8605c5c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf978d35016d1b4246f9961af76b8ded966d0d5c611803fd2f5ffdbe8605c5c7->enter($__internal_cf978d35016d1b4246f9961af76b8ded966d0d5c611803fd2f5ffdbe8605c5c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_f0fea2fdf0815eded952cdebd4ee08c634a70c715e13f3a320ebb83210382829->leave($__internal_f0fea2fdf0815eded952cdebd4ee08c634a70c715e13f3a320ebb83210382829_prof);

        
        $__internal_cf978d35016d1b4246f9961af76b8ded966d0d5c611803fd2f5ffdbe8605c5c7->leave($__internal_cf978d35016d1b4246f9961af76b8ded966d0d5c611803fd2f5ffdbe8605c5c7_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
