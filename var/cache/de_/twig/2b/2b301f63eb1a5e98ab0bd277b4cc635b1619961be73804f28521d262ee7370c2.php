<?php

/* default/homepage.html.twig */
class __TwigTemplate_2a3ccd440c4db56d89f8e30b06f5ca4a7dab7cfee3b69de7c5b4effdc53954cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3aa80e7c282da5317300fad273a8f3d50b6190a83ab598863d9f3bab3bbcd0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3aa80e7c282da5317300fad273a8f3d50b6190a83ab598863d9f3bab3bbcd0b->enter($__internal_b3aa80e7c282da5317300fad273a8f3d50b6190a83ab598863d9f3bab3bbcd0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $__internal_f8a536bd0d0461c1e644224c07ac14892997caff7061afa7d385706fe960a35b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8a536bd0d0461c1e644224c07ac14892997caff7061afa7d385706fe960a35b->enter($__internal_f8a536bd0d0461c1e644224c07ac14892997caff7061afa7d385706fe960a35b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3aa80e7c282da5317300fad273a8f3d50b6190a83ab598863d9f3bab3bbcd0b->leave($__internal_b3aa80e7c282da5317300fad273a8f3d50b6190a83ab598863d9f3bab3bbcd0b_prof);

        
        $__internal_f8a536bd0d0461c1e644224c07ac14892997caff7061afa7d385706fe960a35b->leave($__internal_f8a536bd0d0461c1e644224c07ac14892997caff7061afa7d385706fe960a35b_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_24057801a1b4b9ce8a2d64cb21524798a8ac6825bc0caba364a408b823d6c82a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24057801a1b4b9ce8a2d64cb21524798a8ac6825bc0caba364a408b823d6c82a->enter($__internal_24057801a1b4b9ce8a2d64cb21524798a8ac6825bc0caba364a408b823d6c82a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_afc18f149ea141dd8a379427f7f9faed4c228ef66fe6dab59feeb5ddb582b961 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_afc18f149ea141dd8a379427f7f9faed4c228ef66fe6dab59feeb5ddb582b961->enter($__internal_afc18f149ea141dd8a379427f7f9faed4c228ef66fe6dab59feeb5ddb582b961_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_afc18f149ea141dd8a379427f7f9faed4c228ef66fe6dab59feeb5ddb582b961->leave($__internal_afc18f149ea141dd8a379427f7f9faed4c228ef66fe6dab59feeb5ddb582b961_prof);

        
        $__internal_24057801a1b4b9ce8a2d64cb21524798a8ac6825bc0caba364a408b823d6c82a->leave($__internal_24057801a1b4b9ce8a2d64cb21524798a8ac6825bc0caba364a408b823d6c82a_prof);

    }

    // line 6
    public function block_header($context, array $blocks = array())
    {
        $__internal_3846d7b38697cd1ee43a059c3f4f3d76ff82468d0721fe37883952973be780a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3846d7b38697cd1ee43a059c3f4f3d76ff82468d0721fe37883952973be780a5->enter($__internal_3846d7b38697cd1ee43a059c3f4f3d76ff82468d0721fe37883952973be780a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_64078fbd20a4dc4874256a7c4c14f91b73051572338dd827c1d96ec987ee4184 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64078fbd20a4dc4874256a7c4c14f91b73051572338dd827c1d96ec987ee4184->enter($__internal_64078fbd20a4dc4874256a7c4c14f91b73051572338dd827c1d96ec987ee4184_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_64078fbd20a4dc4874256a7c4c14f91b73051572338dd827c1d96ec987ee4184->leave($__internal_64078fbd20a4dc4874256a7c4c14f91b73051572338dd827c1d96ec987ee4184_prof);

        
        $__internal_3846d7b38697cd1ee43a059c3f4f3d76ff82468d0721fe37883952973be780a5->leave($__internal_3846d7b38697cd1ee43a059c3f4f3d76ff82468d0721fe37883952973be780a5_prof);

    }

    // line 7
    public function block_footer($context, array $blocks = array())
    {
        $__internal_2a062e62cf8083cdad9f6d0e4db8672bc3ea2ea9e3a5e77794cf8d656a46e809 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a062e62cf8083cdad9f6d0e4db8672bc3ea2ea9e3a5e77794cf8d656a46e809->enter($__internal_2a062e62cf8083cdad9f6d0e4db8672bc3ea2ea9e3a5e77794cf8d656a46e809_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_8c8d260b169f5826c4d7d12b5d72fcaf45a50a089366a9d3ce282088d88d3c39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c8d260b169f5826c4d7d12b5d72fcaf45a50a089366a9d3ce282088d88d3c39->enter($__internal_8c8d260b169f5826c4d7d12b5d72fcaf45a50a089366a9d3ce282088d88d3c39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        
        $__internal_8c8d260b169f5826c4d7d12b5d72fcaf45a50a089366a9d3ce282088d88d3c39->leave($__internal_8c8d260b169f5826c4d7d12b5d72fcaf45a50a089366a9d3ce282088d88d3c39_prof);

        
        $__internal_2a062e62cf8083cdad9f6d0e4db8672bc3ea2ea9e3a5e77794cf8d656a46e809->leave($__internal_2a062e62cf8083cdad9f6d0e4db8672bc3ea2ea9e3a5e77794cf8d656a46e809_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_cd59e7c97552ae8cd6a894d8794fe879a851a44084202449e9e491758518bb93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cd59e7c97552ae8cd6a894d8794fe879a851a44084202449e9e491758518bb93->enter($__internal_cd59e7c97552ae8cd6a894d8794fe879a851a44084202449e9e491758518bb93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_cecb49b962638f4f3d3675730edc7297f8e87d05fd863728c967647a7c117519 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cecb49b962638f4f3d3675730edc7297f8e87d05fd863728c967647a7c117519->enter($__internal_cecb49b962638f4f3d3675730edc7297f8e87d05fd863728c967647a7c117519_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"page-header\">
        <h1>";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.homepage");
        echo "</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_app");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index");
        echo "\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_app"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_admin");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_index");
        echo "\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_admin"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_cecb49b962638f4f3d3675730edc7297f8e87d05fd863728c967647a7c117519->leave($__internal_cecb49b962638f4f3d3675730edc7297f8e87d05fd863728c967647a7c117519_prof);

        
        $__internal_cd59e7c97552ae8cd6a894d8794fe879a851a44084202449e9e491758518bb93->leave($__internal_cd59e7c97552ae8cd6a894d8794fe879a851a44084202449e9e491758518bb93_prof);

    }

    public function getTemplateName()
    {
        return "default/homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 35,  145 => 34,  139 => 31,  127 => 22,  123 => 21,  117 => 18,  107 => 11,  104 => 10,  95 => 9,  78 => 7,  61 => 6,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}


{% block header %}{% endblock %}
{% block footer %}{% endblock %}

{% block body %}
    <div class=\"page-header\">
        <h1>{{ 'title.homepage'|trans|raw }}</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_app'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('blog_index') }}\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> {{ 'action.browse_app'|trans }}
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_admin'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('admin_index') }}\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'action.browse_admin'|trans }}
                    </a>
                </p>
            </div>
        </div>
    </div>
{% endblock %}
", "default/homepage.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\default\\homepage.html.twig");
    }
}
