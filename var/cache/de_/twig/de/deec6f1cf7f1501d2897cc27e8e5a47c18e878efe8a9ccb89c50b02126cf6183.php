<?php

/* @App/CRUD/list_batch.html.twig */
class __TwigTemplate_685bf208ff274600ac24a702a963262353c32188222349f302942753a446082f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "@App/CRUD/list_batch.html.twig", 4);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09de6964ccc70ebbb906e9a6f0a9a327604d513db62c6f799c4b0a2a4da76452 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09de6964ccc70ebbb906e9a6f0a9a327604d513db62c6f799c4b0a2a4da76452->enter($__internal_09de6964ccc70ebbb906e9a6f0a9a327604d513db62c6f799c4b0a2a4da76452_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/CRUD/list_batch.html.twig"));

        $__internal_3d99f4ac43ac91d9cd92a029fb49150ed2d2baaf5feefaa6e7fa3cdba5add46f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3d99f4ac43ac91d9cd92a029fb49150ed2d2baaf5feefaa6e7fa3cdba5add46f->enter($__internal_3d99f4ac43ac91d9cd92a029fb49150ed2d2baaf5feefaa6e7fa3cdba5add46f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@App/CRUD/list_batch.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_09de6964ccc70ebbb906e9a6f0a9a327604d513db62c6f799c4b0a2a4da76452->leave($__internal_09de6964ccc70ebbb906e9a6f0a9a327604d513db62c6f799c4b0a2a4da76452_prof);

        
        $__internal_3d99f4ac43ac91d9cd92a029fb49150ed2d2baaf5feefaa6e7fa3cdba5add46f->leave($__internal_3d99f4ac43ac91d9cd92a029fb49150ed2d2baaf5feefaa6e7fa3cdba5add46f_prof);

    }

    // line 6
    public function block_field($context, array $blocks = array())
    {
        $__internal_d179e5e19439e3a65e62f811de07b6b819bbc74e2d652b40476e88969dc14acb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d179e5e19439e3a65e62f811de07b6b819bbc74e2d652b40476e88969dc14acb->enter($__internal_d179e5e19439e3a65e62f811de07b6b819bbc74e2d652b40476e88969dc14acb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_84b6127da8531f4c2feea2f1962e7c5fb125dca7596f6507ea1e9c6b551462e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84b6127da8531f4c2feea2f1962e7c5fb125dca7596f6507ea1e9c6b551462e5->enter($__internal_84b6127da8531f4c2feea2f1962e7c5fb125dca7596f6507ea1e9c6b551462e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 7
        echo "    <input type=\"checkbox\" name=\"idx[]\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\" />

    ";
        // line 10
        echo "    <input type=\"radio\" name=\"targetId\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\" />
";
        
        $__internal_84b6127da8531f4c2feea2f1962e7c5fb125dca7596f6507ea1e9c6b551462e5->leave($__internal_84b6127da8531f4c2feea2f1962e7c5fb125dca7596f6507ea1e9c6b551462e5_prof);

        
        $__internal_d179e5e19439e3a65e62f811de07b6b819bbc74e2d652b40476e88969dc14acb->leave($__internal_d179e5e19439e3a65e62f811de07b6b819bbc74e2d652b40476e88969dc14acb_prof);

    }

    public function getTemplateName()
    {
        return "@App/CRUD/list_batch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 10,  48 => 7,  39 => 6,  18 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/AppBundle/Resources/views/CRUD/list__batch.html.twig #}
{# see SonataAdminBundle:CRUD:list__batch.html.twig for the current default template #}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\" />

    {# the new radio button #}
    <input type=\"radio\" name=\"targetId\" value=\"{{ admin.id(object) }}\" />
{% endblock %}
", "@App/CRUD/list_batch.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\src\\AppBundle\\Resources\\views\\CRUD\\list_batch.html.twig");
    }
}
