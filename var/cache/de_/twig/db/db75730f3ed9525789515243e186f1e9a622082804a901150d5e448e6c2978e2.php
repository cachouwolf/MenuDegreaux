<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_4b18faae8846c70c2830cdee9734a6b221c3003d6c8eb7e238c432d6c13ab6c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7366a84668cb5e6f31fc086cbe905786a0b6efc0ee6fa36af263e067a6061344 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7366a84668cb5e6f31fc086cbe905786a0b6efc0ee6fa36af263e067a6061344->enter($__internal_7366a84668cb5e6f31fc086cbe905786a0b6efc0ee6fa36af263e067a6061344_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_61a91db85e18ed676e63cf5d4053dd02179735343d5d2d5fc12efd482f7c9ce5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61a91db85e18ed676e63cf5d4053dd02179735343d5d2d5fc12efd482f7c9ce5->enter($__internal_61a91db85e18ed676e63cf5d4053dd02179735343d5d2d5fc12efd482f7c9ce5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7366a84668cb5e6f31fc086cbe905786a0b6efc0ee6fa36af263e067a6061344->leave($__internal_7366a84668cb5e6f31fc086cbe905786a0b6efc0ee6fa36af263e067a6061344_prof);

        
        $__internal_61a91db85e18ed676e63cf5d4053dd02179735343d5d2d5fc12efd482f7c9ce5->leave($__internal_61a91db85e18ed676e63cf5d4053dd02179735343d5d2d5fc12efd482f7c9ce5_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_cc955f299ed16b65c859c75dc496e4353c501e069c522044887a48fb409d8d7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc955f299ed16b65c859c75dc496e4353c501e069c522044887a48fb409d8d7e->enter($__internal_cc955f299ed16b65c859c75dc496e4353c501e069c522044887a48fb409d8d7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_fa0ac2cad1080fd426ceb311fe73f48b887d0f9a1cfe54be2c41895dbfa8395f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa0ac2cad1080fd426ceb311fe73f48b887d0f9a1cfe54be2c41895dbfa8395f->enter($__internal_fa0ac2cad1080fd426ceb311fe73f48b887d0f9a1cfe54be2c41895dbfa8395f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_fa0ac2cad1080fd426ceb311fe73f48b887d0f9a1cfe54be2c41895dbfa8395f->leave($__internal_fa0ac2cad1080fd426ceb311fe73f48b887d0f9a1cfe54be2c41895dbfa8395f_prof);

        
        $__internal_cc955f299ed16b65c859c75dc496e4353c501e069c522044887a48fb409d8d7e->leave($__internal_cc955f299ed16b65c859c75dc496e4353c501e069c522044887a48fb409d8d7e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_ef5338a33149e6c19b32d7de4debe873807bfab1b9a80a76797ee9961ba05b81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef5338a33149e6c19b32d7de4debe873807bfab1b9a80a76797ee9961ba05b81->enter($__internal_ef5338a33149e6c19b32d7de4debe873807bfab1b9a80a76797ee9961ba05b81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_3f9e94f606598a952a0a8804f4235a2e069fdabcd139a5dbb19425bd0da43ae6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f9e94f606598a952a0a8804f4235a2e069fdabcd139a5dbb19425bd0da43ae6->enter($__internal_3f9e94f606598a952a0a8804f4235a2e069fdabcd139a5dbb19425bd0da43ae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_3f9e94f606598a952a0a8804f4235a2e069fdabcd139a5dbb19425bd0da43ae6->leave($__internal_3f9e94f606598a952a0a8804f4235a2e069fdabcd139a5dbb19425bd0da43ae6_prof);

        
        $__internal_ef5338a33149e6c19b32d7de4debe873807bfab1b9a80a76797ee9961ba05b81->leave($__internal_ef5338a33149e6c19b32d7de4debe873807bfab1b9a80a76797ee9961ba05b81_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
