<?php

/* layout.html.twig */
class __TwigTemplate_75bce681e7eda1698b08acfe96576a5970d57ae3f6d0b86166c5f4774922e14c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "layout.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8671bb2ea329c5bd09f9086cfc9b71721554577ee0219fbe03af5e15c3f18cac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8671bb2ea329c5bd09f9086cfc9b71721554577ee0219fbe03af5e15c3f18cac->enter($__internal_8671bb2ea329c5bd09f9086cfc9b71721554577ee0219fbe03af5e15c3f18cac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layout.html.twig"));

        $__internal_34104a95c9544ff4606f5a33ebf57eb2c87b8b9df9138214b39ed635a7016f72 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34104a95c9544ff4606f5a33ebf57eb2c87b8b9df9138214b39ed635a7016f72->enter($__internal_34104a95c9544ff4606f5a33ebf57eb2c87b8b9df9138214b39ed635a7016f72_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8671bb2ea329c5bd09f9086cfc9b71721554577ee0219fbe03af5e15c3f18cac->leave($__internal_8671bb2ea329c5bd09f9086cfc9b71721554577ee0219fbe03af5e15c3f18cac_prof);

        
        $__internal_34104a95c9544ff4606f5a33ebf57eb2c87b8b9df9138214b39ed635a7016f72->leave($__internal_34104a95c9544ff4606f5a33ebf57eb2c87b8b9df9138214b39ed635a7016f72_prof);

    }

    // line 4
    public function block_title($context, array $blocks = array())
    {
        $__internal_c56035023b9e1eec096457755535bf82d9ec3b0a8d84f6177569a2eacbe32d0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c56035023b9e1eec096457755535bf82d9ec3b0a8d84f6177569a2eacbe32d0b->enter($__internal_c56035023b9e1eec096457755535bf82d9ec3b0a8d84f6177569a2eacbe32d0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_e0401e168bb497a2319648c2b78695b6ffaddc9055668a36fccf3bfce2ee363b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0401e168bb497a2319648c2b78695b6ffaddc9055668a36fccf3bfce2ee363b->enter($__internal_e0401e168bb497a2319648c2b78695b6ffaddc9055668a36fccf3bfce2ee363b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_e0401e168bb497a2319648c2b78695b6ffaddc9055668a36fccf3bfce2ee363b->leave($__internal_e0401e168bb497a2319648c2b78695b6ffaddc9055668a36fccf3bfce2ee363b_prof);

        
        $__internal_c56035023b9e1eec096457755535bf82d9ec3b0a8d84f6177569a2eacbe32d0b->leave($__internal_c56035023b9e1eec096457755535bf82d9ec3b0a8d84f6177569a2eacbe32d0b_prof);

    }

    // line 6
    public function block_content($context, array $blocks = array())
    {
        $__internal_66ccea03a85c7a51bb7cea44ef8ff7eab2bc9f729a568a99d6487221cfaff2bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66ccea03a85c7a51bb7cea44ef8ff7eab2bc9f729a568a99d6487221cfaff2bf->enter($__internal_66ccea03a85c7a51bb7cea44ef8ff7eab2bc9f729a568a99d6487221cfaff2bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_fcbfc749233d5228f4f0f70f983b6de82f1b88ba1a04cd0220d9a14f36026355 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcbfc749233d5228f4f0f70f983b6de82f1b88ba1a04cd0220d9a14f36026355->enter($__internal_fcbfc749233d5228f4f0f70f983b6de82f1b88ba1a04cd0220d9a14f36026355_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 7
        echo "
            ";
        // line 8
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
";
        
        $__internal_fcbfc749233d5228f4f0f70f983b6de82f1b88ba1a04cd0220d9a14f36026355->leave($__internal_fcbfc749233d5228f4f0f70f983b6de82f1b88ba1a04cd0220d9a14f36026355_prof);

        
        $__internal_66ccea03a85c7a51bb7cea44ef8ff7eab2bc9f729a568a99d6487221cfaff2bf->leave($__internal_66ccea03a85c7a51bb7cea44ef8ff7eab2bc9f729a568a99d6487221cfaff2bf_prof);

    }

    // line 8
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4ed98637f717d9d86d66909e8a41c280c1cfb8315ace8e9c36f23de6690d88fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ed98637f717d9d86d66909e8a41c280c1cfb8315ace8e9c36f23de6690d88fc->enter($__internal_4ed98637f717d9d86d66909e8a41c280c1cfb8315ace8e9c36f23de6690d88fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_983eaedb8823b76739ef5567711aca7160d92debef0979c816c9d987cfe42a62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_983eaedb8823b76739ef5567711aca7160d92debef0979c816c9d987cfe42a62->enter($__internal_983eaedb8823b76739ef5567711aca7160d92debef0979c816c9d987cfe42a62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 9
        echo "            ";
        
        $__internal_983eaedb8823b76739ef5567711aca7160d92debef0979c816c9d987cfe42a62->leave($__internal_983eaedb8823b76739ef5567711aca7160d92debef0979c816c9d987cfe42a62_prof);

        
        $__internal_4ed98637f717d9d86d66909e8a41c280c1cfb8315ace8e9c36f23de6690d88fc->leave($__internal_4ed98637f717d9d86d66909e8a41c280c1cfb8315ace8e9c36f23de6690d88fc_prof);

    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 9,  84 => 8,  73 => 10,  71 => 8,  68 => 7,  59 => 6,  42 => 4,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}


{% block title %}{% endblock %}

{% block content %}

            {% block fos_user_content %}
            {% endblock fos_user_content %}
            
{% endblock %}
", "layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\layout.html.twig");
    }
}
