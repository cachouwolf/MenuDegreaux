<?php

/* SonataAdminBundle:CRUD:list__batch.html.twig */
class __TwigTemplate_52f20ec418589fead0139b034ca29b91e65b7c249990f16dee8d7b2e4d278ab1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__batch.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_84992778f2279905ed35af47b3588c0bf4a75d157ed6f7216048f35b64b55e2b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_84992778f2279905ed35af47b3588c0bf4a75d157ed6f7216048f35b64b55e2b->enter($__internal_84992778f2279905ed35af47b3588c0bf4a75d157ed6f7216048f35b64b55e2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__batch.html.twig"));

        $__internal_069d72a90a79dc4de5e30f8ba729cdee48d295d4004871f224268dc7c332c96e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_069d72a90a79dc4de5e30f8ba729cdee48d295d4004871f224268dc7c332c96e->enter($__internal_069d72a90a79dc4de5e30f8ba729cdee48d295d4004871f224268dc7c332c96e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__batch.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_84992778f2279905ed35af47b3588c0bf4a75d157ed6f7216048f35b64b55e2b->leave($__internal_84992778f2279905ed35af47b3588c0bf4a75d157ed6f7216048f35b64b55e2b_prof);

        
        $__internal_069d72a90a79dc4de5e30f8ba729cdee48d295d4004871f224268dc7c332c96e->leave($__internal_069d72a90a79dc4de5e30f8ba729cdee48d295d4004871f224268dc7c332c96e_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_4122d4ca288372a0a8d18279a51bca3f03710346f7aefaabf9af890a408503c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4122d4ca288372a0a8d18279a51bca3f03710346f7aefaabf9af890a408503c4->enter($__internal_4122d4ca288372a0a8d18279a51bca3f03710346f7aefaabf9af890a408503c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_93cbebf71113d4a2169927aebf8850f73bfcd3e6dc9575ded7a6b76e5521f5a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93cbebf71113d4a2169927aebf8850f73bfcd3e6dc9575ded7a6b76e5521f5a4->enter($__internal_93cbebf71113d4a2169927aebf8850f73bfcd3e6dc9575ded7a6b76e5521f5a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <input type=\"checkbox\" name=\"idx[]\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\">
";
        
        $__internal_93cbebf71113d4a2169927aebf8850f73bfcd3e6dc9575ded7a6b76e5521f5a4->leave($__internal_93cbebf71113d4a2169927aebf8850f73bfcd3e6dc9575ded7a6b76e5521f5a4_prof);

        
        $__internal_4122d4ca288372a0a8d18279a51bca3f03710346f7aefaabf9af890a408503c4->leave($__internal_4122d4ca288372a0a8d18279a51bca3f03710346f7aefaabf9af890a408503c4_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__batch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\">
{% endblock %}
", "SonataAdminBundle:CRUD:list__batch.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list__batch.html.twig");
    }
}
