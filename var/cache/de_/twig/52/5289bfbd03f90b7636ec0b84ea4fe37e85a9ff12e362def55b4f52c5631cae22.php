<?php

/* @Twig/Exception/error.html.twig */
class __TwigTemplate_013fe97dac1542d317a115ce2f28c09237a69fc4350c910b13a5c3cef72fa50e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("base.html.twig", "@Twig/Exception/error.html.twig", 11);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_471ad6e2d6f07ce77cfa494825469404d35afaa36c0f8edf0353aeb245b0cfe3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_471ad6e2d6f07ce77cfa494825469404d35afaa36c0f8edf0353aeb245b0cfe3->enter($__internal_471ad6e2d6f07ce77cfa494825469404d35afaa36c0f8edf0353aeb245b0cfe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.html.twig"));

        $__internal_04204f72fc938725afd4a52d71ab6197d18ab9ba6d3ea280e12ad96186cf6e9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04204f72fc938725afd4a52d71ab6197d18ab9ba6d3ea280e12ad96186cf6e9a->enter($__internal_04204f72fc938725afd4a52d71ab6197d18ab9ba6d3ea280e12ad96186cf6e9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_471ad6e2d6f07ce77cfa494825469404d35afaa36c0f8edf0353aeb245b0cfe3->leave($__internal_471ad6e2d6f07ce77cfa494825469404d35afaa36c0f8edf0353aeb245b0cfe3_prof);

        
        $__internal_04204f72fc938725afd4a52d71ab6197d18ab9ba6d3ea280e12ad96186cf6e9a->leave($__internal_04204f72fc938725afd4a52d71ab6197d18ab9ba6d3ea280e12ad96186cf6e9a_prof);

    }

    // line 13
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_bc4ec0d6f8a37cd52a413d22e493384074b1b136a235005a043f18cde385e3aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc4ec0d6f8a37cd52a413d22e493384074b1b136a235005a043f18cde385e3aa->enter($__internal_bc4ec0d6f8a37cd52a413d22e493384074b1b136a235005a043f18cde385e3aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_8714992b74600d04d3e1186c10c03ebecc595fdf851a86cc2c6dcc9c63e2a4ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8714992b74600d04d3e1186c10c03ebecc595fdf851a86cc2c6dcc9c63e2a4ac->enter($__internal_8714992b74600d04d3e1186c10c03ebecc595fdf851a86cc2c6dcc9c63e2a4ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "error";
        
        $__internal_8714992b74600d04d3e1186c10c03ebecc595fdf851a86cc2c6dcc9c63e2a4ac->leave($__internal_8714992b74600d04d3e1186c10c03ebecc595fdf851a86cc2c6dcc9c63e2a4ac_prof);

        
        $__internal_bc4ec0d6f8a37cd52a413d22e493384074b1b136a235005a043f18cde385e3aa->leave($__internal_bc4ec0d6f8a37cd52a413d22e493384074b1b136a235005a043f18cde385e3aa_prof);

    }

    // line 15
    public function block_main($context, array $blocks = array())
    {
        $__internal_07283f091cef4a599f3737a64a1d3470ee737126f1c12a171e380763fb6487fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07283f091cef4a599f3737a64a1d3470ee737126f1c12a171e380763fb6487fb->enter($__internal_07283f091cef4a599f3737a64a1d3470ee737126f1c12a171e380763fb6487fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_80f6a40834b4358d356ae8c65a4380637aebb27fd00d45a8c71da03353179b7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80f6a40834b4358d356ae8c65a4380637aebb27fd00d45a8c71da03353179b7c->enter($__internal_80f6a40834b4358d356ae8c65a4380637aebb27fd00d45a8c71da03353179b7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 16
        echo "    <h1 class=\"text-danger\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.name", array("%status_code%" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")))), "html", null, true);
        echo "</h1>

    <p class=\"lead\">
        ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.description", array("%status_code%" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")))), "html", null, true);
        echo "
    </p>
    <p>
        ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.suggestion", array("%url%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index")));
        echo "
    </p>
";
        
        $__internal_80f6a40834b4358d356ae8c65a4380637aebb27fd00d45a8c71da03353179b7c->leave($__internal_80f6a40834b4358d356ae8c65a4380637aebb27fd00d45a8c71da03353179b7c_prof);

        
        $__internal_07283f091cef4a599f3737a64a1d3470ee737126f1c12a171e380763fb6487fb->leave($__internal_07283f091cef4a599f3737a64a1d3470ee737126f1c12a171e380763fb6487fb_prof);

    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_9c9c63f474b7292e375e30305ad46fb90b199c53877d3c37cb35d0d52680de7f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c9c63f474b7292e375e30305ad46fb90b199c53877d3c37cb35d0d52680de7f->enter($__internal_9c9c63f474b7292e375e30305ad46fb90b199c53877d3c37cb35d0d52680de7f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_ba3aa4dc8fa530de366cbd1e3a9a49fb28ed1373512cc1ad6a7a0722f9dccf52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba3aa4dc8fa530de366cbd1e3a9a49fb28ed1373512cc1ad6a7a0722f9dccf52->enter($__internal_ba3aa4dc8fa530de366cbd1e3a9a49fb28ed1373512cc1ad6a7a0722f9dccf52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 27
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 29
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_ba3aa4dc8fa530de366cbd1e3a9a49fb28ed1373512cc1ad6a7a0722f9dccf52->leave($__internal_ba3aa4dc8fa530de366cbd1e3a9a49fb28ed1373512cc1ad6a7a0722f9dccf52_prof);

        
        $__internal_9c9c63f474b7292e375e30305ad46fb90b199c53877d3c37cb35d0d52680de7f->leave($__internal_9c9c63f474b7292e375e30305ad46fb90b199c53877d3c37cb35d0d52680de7f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 29,  104 => 27,  95 => 26,  82 => 22,  76 => 19,  69 => 16,  60 => 15,  42 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    This template is used to render any error different from 403, 404 and 500.

    This is the simplest way to customize error pages in Symfony applications.
    In case you need it, you can also hook into the internal exception handling
    made by Symfony. This allows you to perform advanced tasks and even recover
    your application from some errors.
    See http://symfony.com/doc/current/cookbook/controller/error_pages.html
#}

{% extends 'base.html.twig' %}

{% block body_id 'error' %}

{% block main %}
    <h1 class=\"text-danger\">{{ 'http_error.name'|trans({ '%status_code%': status_code }) }}</h1>

    <p class=\"lead\">
        {{ 'http_error.description'|trans({ '%status_code%': status_code }) }}
    </p>
    <p>
        {{ 'http_error.suggestion'|trans({ '%url%': path('blog_index') })|raw }}
    </p>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "@Twig/Exception/error.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\TwigBundle\\views\\Exception\\error.html.twig");
    }
}
