<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_191fcd9ede60c1006a68c9faf525ecf29583d19d98f2e06b21ab45f2a1dab0f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e58325e13240249be4b425016c66eed9e6d7d9348ba39ef366a6f2d07970591c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e58325e13240249be4b425016c66eed9e6d7d9348ba39ef366a6f2d07970591c->enter($__internal_e58325e13240249be4b425016c66eed9e6d7d9348ba39ef366a6f2d07970591c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_758bc40a4a0c9cf5acfdb6a50ef6154655b7cf0a8dad20b5f93d3421255cf116 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_758bc40a4a0c9cf5acfdb6a50ef6154655b7cf0a8dad20b5f93d3421255cf116->enter($__internal_758bc40a4a0c9cf5acfdb6a50ef6154655b7cf0a8dad20b5f93d3421255cf116_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_e58325e13240249be4b425016c66eed9e6d7d9348ba39ef366a6f2d07970591c->leave($__internal_e58325e13240249be4b425016c66eed9e6d7d9348ba39ef366a6f2d07970591c_prof);

        
        $__internal_758bc40a4a0c9cf5acfdb6a50ef6154655b7cf0a8dad20b5f93d3421255cf116->leave($__internal_758bc40a4a0c9cf5acfdb6a50ef6154655b7cf0a8dad20b5f93d3421255cf116_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\submit_widget.html.php");
    }
}
