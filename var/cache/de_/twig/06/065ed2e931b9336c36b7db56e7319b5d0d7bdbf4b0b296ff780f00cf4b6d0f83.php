<?php

/* menu/externe.html.twig */
class __TwigTemplate_42891627f35f0b83dd03d9df8e88b3999b3aca396ee6578f3426aa6cb4041c84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/externe.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc16bbeea2a1ae8066881cb4b548ec16449607b84c8612e71784ebd7f28863c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc16bbeea2a1ae8066881cb4b548ec16449607b84c8612e71784ebd7f28863c1->enter($__internal_bc16bbeea2a1ae8066881cb4b548ec16449607b84c8612e71784ebd7f28863c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/externe.html.twig"));

        $__internal_02922561ad876caacb1c1d179caab16caf24f2eef972accdc9bbea56a4dddcd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_02922561ad876caacb1c1d179caab16caf24f2eef972accdc9bbea56a4dddcd0->enter($__internal_02922561ad876caacb1c1d179caab16caf24f2eef972accdc9bbea56a4dddcd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/externe.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bc16bbeea2a1ae8066881cb4b548ec16449607b84c8612e71784ebd7f28863c1->leave($__internal_bc16bbeea2a1ae8066881cb4b548ec16449607b84c8612e71784ebd7f28863c1_prof);

        
        $__internal_02922561ad876caacb1c1d179caab16caf24f2eef972accdc9bbea56a4dddcd0->leave($__internal_02922561ad876caacb1c1d179caab16caf24f2eef972accdc9bbea56a4dddcd0_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_c4544884e15f6e0c5d847a72bbe5fa5762b221c0e6ef87acbe4fab1ae8d4fb36 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4544884e15f6e0c5d847a72bbe5fa5762b221c0e6ef87acbe4fab1ae8d4fb36->enter($__internal_c4544884e15f6e0c5d847a72bbe5fa5762b221c0e6ef87acbe4fab1ae8d4fb36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_c39f4339cbd642298b48f62e0344da95d298027f5e2fd7fa82a291df25c6b675 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c39f4339cbd642298b48f62e0344da95d298027f5e2fd7fa82a291df25c6b675->enter($__internal_c39f4339cbd642298b48f62e0344da95d298027f5e2fd7fa82a291df25c6b675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_externe";
        
        $__internal_c39f4339cbd642298b48f62e0344da95d298027f5e2fd7fa82a291df25c6b675->leave($__internal_c39f4339cbd642298b48f62e0344da95d298027f5e2fd7fa82a291df25c6b675_prof);

        
        $__internal_c4544884e15f6e0c5d847a72bbe5fa5762b221c0e6ef87acbe4fab1ae8d4fb36->leave($__internal_c4544884e15f6e0c5d847a72bbe5fa5762b221c0e6ef87acbe4fab1ae8d4fb36_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_5e8107e6d807d20797c7ca30bbf800fab1985d2a1b496d306e0712c40f088e5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e8107e6d807d20797c7ca30bbf800fab1985d2a1b496d306e0712c40f088e5d->enter($__internal_5e8107e6d807d20797c7ca30bbf800fab1985d2a1b496d306e0712c40f088e5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_1f7eddc2a65d8482c3ed6ee405ed871385fcdcc4884501e62a327871a54476cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f7eddc2a65d8482c3ed6ee405ed871385fcdcc4884501e62a327871a54476cc->enter($__internal_1f7eddc2a65d8482c3ed6ee405ed871385fcdcc4884501e62a327871a54476cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 25
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 26
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 68
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 69
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediPlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 71
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheEntree", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimanchePlat", array()), "html", null, true);
        echo "</div>
                        <div class=\"text\">";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
                        <div class=\" text\">";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheDessert", array()), "html", null, true);
        echo "</div>
                    </div>
                </div>

            </div>




";
        
        $__internal_1f7eddc2a65d8482c3ed6ee405ed871385fcdcc4884501e62a327871a54476cc->leave($__internal_1f7eddc2a65d8482c3ed6ee405ed871385fcdcc4884501e62a327871a54476cc_prof);

        
        $__internal_5e8107e6d807d20797c7ca30bbf800fab1985d2a1b496d306e0712c40f088e5d->leave($__internal_5e8107e6d807d20797c7ca30bbf800fab1985d2a1b496d306e0712c40f088e5d_prof);

    }

    // line 96
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_c7f45aa8ebd49b64916e0fd5636a97e8b8f18ef9828a2dcc91fa3905e59649e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7f45aa8ebd49b64916e0fd5636a97e8b8f18ef9828a2dcc91fa3905e59649e5->enter($__internal_c7f45aa8ebd49b64916e0fd5636a97e8b8f18ef9828a2dcc91fa3905e59649e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_ee4d67921db8411baf63cf0b9ed5bf6d811ffba68263dcfbabf7705c9462752f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ee4d67921db8411baf63cf0b9ed5bf6d811ffba68263dcfbabf7705c9462752f->enter($__internal_ee4d67921db8411baf63cf0b9ed5bf6d811ffba68263dcfbabf7705c9462752f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 97
        echo "    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"";
        // line 101
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"";
        // line 106
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"";
        // line 111
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            Résident
        </a>
    </div>



";
        
        $__internal_ee4d67921db8411baf63cf0b9ed5bf6d811ffba68263dcfbabf7705c9462752f->leave($__internal_ee4d67921db8411baf63cf0b9ed5bf6d811ffba68263dcfbabf7705c9462752f_prof);

        
        $__internal_c7f45aa8ebd49b64916e0fd5636a97e8b8f18ef9828a2dcc91fa3905e59649e5->leave($__internal_c7f45aa8ebd49b64916e0fd5636a97e8b8f18ef9828a2dcc91fa3905e59649e5_prof);

    }

    public function getTemplateName()
    {
        return "menu/externe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 111,  273 => 106,  265 => 101,  259 => 97,  250 => 96,  230 => 84,  226 => 83,  222 => 82,  218 => 81,  205 => 71,  201 => 70,  197 => 69,  193 => 68,  183 => 61,  179 => 60,  175 => 59,  171 => 58,  161 => 51,  157 => 50,  153 => 49,  149 => 48,  136 => 38,  132 => 37,  128 => 36,  124 => 35,  114 => 28,  110 => 27,  106 => 26,  102 => 25,  92 => 18,  88 => 17,  84 => 16,  80 => 15,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'menu_externe' %}

{% block main %}
        <h1> Menu Externe</h1>
        <p class=\"subtitle\"> Menu du midi uniquement</p>


            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Lundi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.lundiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.lundiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.lundiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.lundiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mardi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mardiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mardiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mardiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mardiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Mercredi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.mercrediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.mercrediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.mercrediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Jeudi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.jeudiEntree }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiPlat }}</div>
                        <div class=\"text\">{{ menuMidi.jeudiAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.jeudiDessert }}</div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuMidi.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuMidi.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuMidi.vendrediDessert }}</div>
                        </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"titre\">Samedi </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.samediEntree }}</div>
                        <div class=\" text\">{{ menuMidi.samediPlat }}</div>
                        <div class=\"text\">{{ menuMidi.samediAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.samediDessert }}</div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-md-offset-4 col-xs-12\">
                    <div class=\"titre\">Dimanche </div>
                    <div class=\"menu\">
                        <div class=\" text\">{{ menuMidi.dimancheEntree }}</div>
                        <div class=\" text\">{{ menuMidi.dimanchePlat }}</div>
                        <div class=\"text\">{{ menuMidi.dimancheAccompagnement }}</div>
                        <div class=\" text\">{{ menuMidi.dimancheDessert }}</div>
                    </div>
                </div>

            </div>




{% endblock %}


{% block sidebar %}
    <p class=\"titre\">
        Voir le menu :
    </p>
    <div>
        <a href=\"{{ path('externe') }}\" >
            Externe
        </a>
    </div>
    <div>
        <a  href=\"{{ path('scolaire') }}\">
            Scolaire
        </a>
    </div>
    <div>
        <a href=\"{{ path('resident') }}\" >
            Résident
        </a>
    </div>



{% endblock %}
", "menu/externe.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\menu\\externe.html.twig");
    }
}
