<?php

/* @SonataBlock/Block/block_no_page_available.html.twig */
class __TwigTemplate_6d218cb40ce2e2b59b0fb54b982b43a8484816e0059a5f58ba5fcd82f0a8a887 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf56c0fc9a426ac654ae774fd472acf0bbe5052cd8c0ce82434a1990e59502a1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf56c0fc9a426ac654ae774fd472acf0bbe5052cd8c0ce82434a1990e59502a1->enter($__internal_cf56c0fc9a426ac654ae774fd472acf0bbe5052cd8c0ce82434a1990e59502a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_no_page_available.html.twig"));

        $__internal_7e0cef4c35c16684e0f8b3e17029b43a326897e7b599e6244daa9de66f03bda9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e0cef4c35c16684e0f8b3e17029b43a326897e7b599e6244daa9de66f03bda9->enter($__internal_7e0cef4c35c16684e0f8b3e17029b43a326897e7b599e6244daa9de66f03bda9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_no_page_available.html.twig"));

        
        $__internal_cf56c0fc9a426ac654ae774fd472acf0bbe5052cd8c0ce82434a1990e59502a1->leave($__internal_cf56c0fc9a426ac654ae774fd472acf0bbe5052cd8c0ce82434a1990e59502a1_prof);

        
        $__internal_7e0cef4c35c16684e0f8b3e17029b43a326897e7b599e6244daa9de66f03bda9->leave($__internal_7e0cef4c35c16684e0f8b3e17029b43a326897e7b599e6244daa9de66f03bda9_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_no_page_available.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
", "@SonataBlock/Block/block_no_page_available.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_no_page_available.html.twig");
    }
}
