<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_76f95d76816d9f7b0528ea772ce286ecbb91435da27673c05df71bc1cbe0feae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_96960b6d792b50a75e0bed42f349ab968f923869e96a070bd54f98be4ba5edf5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96960b6d792b50a75e0bed42f349ab968f923869e96a070bd54f98be4ba5edf5->enter($__internal_96960b6d792b50a75e0bed42f349ab968f923869e96a070bd54f98be4ba5edf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $__internal_42c7105c4c0a22f31df37b8dd302aa459ce11381d1e96f5adbad1d5ed1a35e73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_42c7105c4c0a22f31df37b8dd302aa459ce11381d1e96f5adbad1d5ed1a35e73->enter($__internal_42c7105c4c0a22f31df37b8dd302aa459ce11381d1e96f5adbad1d5ed1a35e73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_96960b6d792b50a75e0bed42f349ab968f923869e96a070bd54f98be4ba5edf5->leave($__internal_96960b6d792b50a75e0bed42f349ab968f923869e96a070bd54f98be4ba5edf5_prof);

        
        $__internal_42c7105c4c0a22f31df37b8dd302aa459ce11381d1e96f5adbad1d5ed1a35e73->leave($__internal_42c7105c4c0a22f31df37b8dd302aa459ce11381d1e96f5adbad1d5ed1a35e73_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_5f78406bbb5f2e11fce8a2090e65ed6fa05feafd4cb60ea13d86be58a6a1e8b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f78406bbb5f2e11fce8a2090e65ed6fa05feafd4cb60ea13d86be58a6a1e8b1->enter($__internal_5f78406bbb5f2e11fce8a2090e65ed6fa05feafd4cb60ea13d86be58a6a1e8b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_d1ad714474bdbff3495ebc88c995d4d9ed941d139f0bcd1d7fce6abd89f2e7a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1ad714474bdbff3495ebc88c995d4d9ed941d139f0bcd1d7fce6abd89f2e7a7->enter($__internal_d1ad714474bdbff3495ebc88c995d4d9ed941d139f0bcd1d7fce6abd89f2e7a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 4)->display($context);
        
        $__internal_d1ad714474bdbff3495ebc88c995d4d9ed941d139f0bcd1d7fce6abd89f2e7a7->leave($__internal_d1ad714474bdbff3495ebc88c995d4d9ed941d139f0bcd1d7fce6abd89f2e7a7_prof);

        
        $__internal_5f78406bbb5f2e11fce8a2090e65ed6fa05feafd4cb60ea13d86be58a6a1e8b1->leave($__internal_5f78406bbb5f2e11fce8a2090e65ed6fa05feafd4cb60ea13d86be58a6a1e8b1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Profile:show.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/show.html.twig");
    }
}
