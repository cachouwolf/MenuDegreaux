<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_ffd47511a6b858674cb3a758a0cba805b39d864aa73e1a09d610f002da519a2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d85df1afc03904457ab41e42d1f4b0ab8d741a8ba2de5077761e24521ce414a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d85df1afc03904457ab41e42d1f4b0ab8d741a8ba2de5077761e24521ce414a8->enter($__internal_d85df1afc03904457ab41e42d1f4b0ab8d741a8ba2de5077761e24521ce414a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_d4a3d7813fc5dcf28f9991db0256c48af122db99d4f3ba8b02390b9a0351005b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4a3d7813fc5dcf28f9991db0256c48af122db99d4f3ba8b02390b9a0351005b->enter($__internal_d4a3d7813fc5dcf28f9991db0256c48af122db99d4f3ba8b02390b9a0351005b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_d85df1afc03904457ab41e42d1f4b0ab8d741a8ba2de5077761e24521ce414a8->leave($__internal_d85df1afc03904457ab41e42d1f4b0ab8d741a8ba2de5077761e24521ce414a8_prof);

        
        $__internal_d4a3d7813fc5dcf28f9991db0256c48af122db99d4f3ba8b02390b9a0351005b->leave($__internal_d4a3d7813fc5dcf28f9991db0256c48af122db99d4f3ba8b02390b9a0351005b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\checkbox_widget.html.php");
    }
}
