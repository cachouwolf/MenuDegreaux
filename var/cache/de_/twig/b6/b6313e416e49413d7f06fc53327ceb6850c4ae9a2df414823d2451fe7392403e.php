<?php

/* :form:fields.html.twig */
class __TwigTemplate_6f0213bc17a8a68718e2aaa15cb7062e1372ce226050bf3bdb71435d81b91d1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_62f47864af046015cc17f926ccbfca2e865c42b276473754c6b61d4a95138701 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_62f47864af046015cc17f926ccbfca2e865c42b276473754c6b61d4a95138701->enter($__internal_62f47864af046015cc17f926ccbfca2e865c42b276473754c6b61d4a95138701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:fields.html.twig"));

        $__internal_3845c02a3f009aa8fdb84431e5479a6e53d0d3872725c0111cf97c90a4727505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3845c02a3f009aa8fdb84431e5479a6e53d0d3872725c0111cf97c90a4727505->enter($__internal_3845c02a3f009aa8fdb84431e5479a6e53d0d3872725c0111cf97c90a4727505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:fields.html.twig"));

        // line 1
        echo "



";
        // line 5
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_62f47864af046015cc17f926ccbfca2e865c42b276473754c6b61d4a95138701->leave($__internal_62f47864af046015cc17f926ccbfca2e865c42b276473754c6b61d4a95138701_prof);

        
        $__internal_3845c02a3f009aa8fdb84431e5479a6e53d0d3872725c0111cf97c90a4727505->leave($__internal_3845c02a3f009aa8fdb84431e5479a6e53d0d3872725c0111cf97c90a4727505_prof);

    }

    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_57a31fd59ce490d6daa9d1164fa2e0ed6ed64e1603f723fcbf942c7509673f70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_57a31fd59ce490d6daa9d1164fa2e0ed6ed64e1603f723fcbf942c7509673f70->enter($__internal_57a31fd59ce490d6daa9d1164fa2e0ed6ed64e1603f723fcbf942c7509673f70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_8fa6f89face971239fb44fc2060d519d947795fe092d32623a9f8cbddb628fda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8fa6f89face971239fb44fc2060d519d947795fe092d32623a9f8cbddb628fda->enter($__internal_8fa6f89face971239fb44fc2060d519d947795fe092d32623a9f8cbddb628fda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 6
        echo "    <div class=\"input-group\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter((isset($context["tags"]) ? $context["tags"] : $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_8fa6f89face971239fb44fc2060d519d947795fe092d32623a9f8cbddb628fda->leave($__internal_8fa6f89face971239fb44fc2060d519d947795fe092d32623a9f8cbddb628fda_prof);

        
        $__internal_57a31fd59ce490d6daa9d1164fa2e0ed6ed64e1603f723fcbf942c7509673f70->leave($__internal_57a31fd59ce490d6daa9d1164fa2e0ed6ed64e1603f723fcbf942c7509673f70_prof);

    }

    public function getTemplateName()
    {
        return ":form:fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  50 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("



{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", ":form:fields.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/form/fields.html.twig");
    }
}
