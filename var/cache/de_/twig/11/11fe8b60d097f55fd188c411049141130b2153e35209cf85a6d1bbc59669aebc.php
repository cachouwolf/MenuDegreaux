<?php

/* @SonataBlock/Block/block_core_rss.html.twig */
class __TwigTemplate_ce0ef6d9f1f15b605bce4e86c6262e000e13d9fd38667b2a9fb1ab7ea8d4d716 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_core_rss.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39aacbe82c8177a9eb1274cffed730908a41d6a857c4fd8971b10a8980c45912 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39aacbe82c8177a9eb1274cffed730908a41d6a857c4fd8971b10a8980c45912->enter($__internal_39aacbe82c8177a9eb1274cffed730908a41d6a857c4fd8971b10a8980c45912_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_rss.html.twig"));

        $__internal_53df94747618e7fe217336d450b38baf3daa0269819697b91990532d73cd547a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53df94747618e7fe217336d450b38baf3daa0269819697b91990532d73cd547a->enter($__internal_53df94747618e7fe217336d450b38baf3daa0269819697b91990532d73cd547a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_rss.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_39aacbe82c8177a9eb1274cffed730908a41d6a857c4fd8971b10a8980c45912->leave($__internal_39aacbe82c8177a9eb1274cffed730908a41d6a857c4fd8971b10a8980c45912_prof);

        
        $__internal_53df94747618e7fe217336d450b38baf3daa0269819697b91990532d73cd547a->leave($__internal_53df94747618e7fe217336d450b38baf3daa0269819697b91990532d73cd547a_prof);

    }

    // line 13
    public function block_block($context, array $blocks = array())
    {
        $__internal_df1070096c68ec106115370fde9de0fd4d5fd2f7117bcf64a92e48fe92d87918 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df1070096c68ec106115370fde9de0fd4d5fd2f7117bcf64a92e48fe92d87918->enter($__internal_df1070096c68ec106115370fde9de0fd4d5fd2f7117bcf64a92e48fe92d87918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_8b91ac2898853270c4b4e7a1582107887109b58dd2abad377444b03b0b8e61f6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b91ac2898853270c4b4e7a1582107887109b58dd2abad377444b03b0b8e61f6->enter($__internal_8b91ac2898853270c4b4e7a1582107887109b58dd2abad377444b03b0b8e61f6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 14
        echo "    <h3 class=\"sonata-feed-title\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "title", array()), "html", null, true);
        echo "</h3>

    <div class=\"sonata-feeds-container\">
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["feeds"]) ? $context["feeds"] : $this->getContext($context, "feeds")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["feed"]) {
            // line 18
            echo "            <div>
                <strong><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "link", array()), "html", null, true);
            echo "\" rel=\"nofollow\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "title", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "title", array()), "html", null, true);
            echo "</a></strong>
                <div>";
            // line 20
            echo $this->getAttribute($context["feed"], "description", array());
            echo "</div>
            </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 23
            echo "                No feeds available.
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feed'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    </div>
";
        
        $__internal_8b91ac2898853270c4b4e7a1582107887109b58dd2abad377444b03b0b8e61f6->leave($__internal_8b91ac2898853270c4b4e7a1582107887109b58dd2abad377444b03b0b8e61f6_prof);

        
        $__internal_df1070096c68ec106115370fde9de0fd4d5fd2f7117bcf64a92e48fe92d87918->leave($__internal_df1070096c68ec106115370fde9de0fd4d5fd2f7117bcf64a92e48fe92d87918_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_core_rss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 25,  79 => 23,  71 => 20,  63 => 19,  60 => 18,  55 => 17,  48 => 14,  39 => 13,  18 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends sonata_block.templates.block_base %}

{% block block %}
    <h3 class=\"sonata-feed-title\">{{ settings.title }}</h3>

    <div class=\"sonata-feeds-container\">
        {% for feed in feeds %}
            <div>
                <strong><a href=\"{{ feed.link}}\" rel=\"nofollow\" title=\"{{ feed.title }}\">{{ feed.title }}</a></strong>
                <div>{{ feed.description|raw }}</div>
            </div>
        {% else %}
                No feeds available.
        {% endfor %}
    </div>
{% endblock %}
", "@SonataBlock/Block/block_core_rss.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_core_rss.html.twig");
    }
}
