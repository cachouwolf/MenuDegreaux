<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_fb762163dd4d091dd5dbe56f69239ec2bbe7f7867623ade9a46f9bdb6f9570d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ac2fc1b4b34f933818d8bb195fcb64cf10490c0a043a3026a6b01655ce32bad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ac2fc1b4b34f933818d8bb195fcb64cf10490c0a043a3026a6b01655ce32bad->enter($__internal_0ac2fc1b4b34f933818d8bb195fcb64cf10490c0a043a3026a6b01655ce32bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        $__internal_f9a5cc22fc1d382cd7d2a1eacf78179c01872538a3411192728492f49bafaf5f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9a5cc22fc1d382cd7d2a1eacf78179c01872538a3411192728492f49bafaf5f->enter($__internal_f9a5cc22fc1d382cd7d2a1eacf78179c01872538a3411192728492f49bafaf5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.txt.twig"));

        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code"));
        echo " ";
        echo (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text"));
        echo "\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
";
        
        $__internal_0ac2fc1b4b34f933818d8bb195fcb64cf10490c0a043a3026a6b01655ce32bad->leave($__internal_0ac2fc1b4b34f933818d8bb195fcb64cf10490c0a043a3026a6b01655ce32bad_prof);

        
        $__internal_f9a5cc22fc1d382cd7d2a1eacf78179c01872538a3411192728492f49bafaf5f->leave($__internal_f9a5cc22fc1d382cd7d2a1eacf78179c01872538a3411192728492f49bafaf5f_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("Oops! An Error Occurred
=======================

The server returned a \"{{ status_code }} {{ status_text }}\".

Something is broken. Please let us know what you were doing when this error occurred.
We will fix it as soon as possible. Sorry for any inconvenience caused.
", "TwigBundle:Exception:error.txt.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.txt.twig");
    }
}
