<?php

/* SonataBlockBundle:Block:block_exception_debug.html.twig */
class __TwigTemplate_d1eb41eee6f09400442f9e43d33504de28f44ad6fff08983fb2667f891957fe2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_exception_debug.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3637598beb544377f4badeac900c742f023d17671f3147df98e25d0cdf7d6713 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3637598beb544377f4badeac900c742f023d17671f3147df98e25d0cdf7d6713->enter($__internal_3637598beb544377f4badeac900c742f023d17671f3147df98e25d0cdf7d6713_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception_debug.html.twig"));

        $__internal_e0f098fd9af1f4546d4f8421816a284be0cf91f9cc4be97407169158ebf9202a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e0f098fd9af1f4546d4f8421816a284be0cf91f9cc4be97407169158ebf9202a->enter($__internal_e0f098fd9af1f4546d4f8421816a284be0cf91f9cc4be97407169158ebf9202a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_exception_debug.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3637598beb544377f4badeac900c742f023d17671f3147df98e25d0cdf7d6713->leave($__internal_3637598beb544377f4badeac900c742f023d17671f3147df98e25d0cdf7d6713_prof);

        
        $__internal_e0f098fd9af1f4546d4f8421816a284be0cf91f9cc4be97407169158ebf9202a->leave($__internal_e0f098fd9af1f4546d4f8421816a284be0cf91f9cc4be97407169158ebf9202a_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_502b2cb9734c0443adec032e9a00391bad27bad4416df0ffab3131d608100e0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_502b2cb9734c0443adec032e9a00391bad27bad4416df0ffab3131d608100e0c->enter($__internal_502b2cb9734c0443adec032e9a00391bad27bad4416df0ffab3131d608100e0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_2d2139069e41e8beef94ed7f42cf43609b4219ca39d3ebebf800bab0f45b47ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d2139069e41e8beef94ed7f42cf43609b4219ca39d3ebebf800bab0f45b47ea->enter($__internal_2d2139069e41e8beef94ed7f42cf43609b4219ca39d3ebebf800bab0f45b47ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\" ";
        if ((isset($context["forceStyle"]) ? $context["forceStyle"] : $this->getContext($context, "forceStyle"))) {
            echo "style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"";
        }
        echo ">

        ";
        // line 18
        echo "        ";
        if ((isset($context["forceStyle"]) ? $context["forceStyle"] : $this->getContext($context, "forceStyle"))) {
            // line 19
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception_layout.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        ";
        }
        // line 22
        echo "        ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "SonataBlockBundle:Block:block_exception_debug.html.twig", 22)->display($context);
        // line 23
        echo "    </div>
";
        
        $__internal_2d2139069e41e8beef94ed7f42cf43609b4219ca39d3ebebf800bab0f45b47ea->leave($__internal_2d2139069e41e8beef94ed7f42cf43609b4219ca39d3ebebf800bab0f45b47ea_prof);

        
        $__internal_502b2cb9734c0443adec032e9a00391bad27bad4416df0ffab3131d608100e0c->leave($__internal_502b2cb9734c0443adec032e9a00391bad27bad4416df0ffab3131d608100e0c_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_exception_debug.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  69 => 22,  64 => 20,  59 => 19,  56 => 18,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\" {% if forceStyle %}style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"{% endif %}>

        {# this is dirty but the alternative would require a new block-optimized exception css #}
        {% if forceStyle %}
            <link href=\"{{ asset('bundles/framework/css/exception_layout.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"{{ asset('bundles/framework/css/exception.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        {% endif %}
        {% include 'TwigBundle:Exception:exception.html.twig' %}
    </div>
{% endblock %}
", "SonataBlockBundle:Block:block_exception_debug.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_exception_debug.html.twig");
    }
}
