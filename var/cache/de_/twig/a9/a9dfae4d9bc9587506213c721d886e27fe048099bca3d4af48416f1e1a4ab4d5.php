<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_0a7179eaa08ff66dd37eb2cb41235224224b31c9be5388cef276a3f165dc853f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60e586660306d8f70e00b7a0df1acf827bf1fd2da7b6e6acbc17a60c158e1434 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60e586660306d8f70e00b7a0df1acf827bf1fd2da7b6e6acbc17a60c158e1434->enter($__internal_60e586660306d8f70e00b7a0df1acf827bf1fd2da7b6e6acbc17a60c158e1434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_a10ec34681c4e5b8a6cc9fc2759e3e11ca8515823fdaaac0a037528502648c16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a10ec34681c4e5b8a6cc9fc2759e3e11ca8515823fdaaac0a037528502648c16->enter($__internal_a10ec34681c4e5b8a6cc9fc2759e3e11ca8515823fdaaac0a037528502648c16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_60e586660306d8f70e00b7a0df1acf827bf1fd2da7b6e6acbc17a60c158e1434->leave($__internal_60e586660306d8f70e00b7a0df1acf827bf1fd2da7b6e6acbc17a60c158e1434_prof);

        
        $__internal_a10ec34681c4e5b8a6cc9fc2759e3e11ca8515823fdaaac0a037528502648c16->leave($__internal_a10ec34681c4e5b8a6cc9fc2759e3e11ca8515823fdaaac0a037528502648c16_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\collection_widget.html.php");
    }
}
