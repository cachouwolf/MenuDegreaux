<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_5ad162bc6d07991bdbe466bb6d2b0ab275181ebe657958702e40350e9ca9b06c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9dfce3fadb71fdb4eabc4cedd1ff934bba612ac31a8fd2479623445c72e1070 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9dfce3fadb71fdb4eabc4cedd1ff934bba612ac31a8fd2479623445c72e1070->enter($__internal_c9dfce3fadb71fdb4eabc4cedd1ff934bba612ac31a8fd2479623445c72e1070_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $__internal_3829d938a053145ee28e4056911aab8aaa169f4265462d9ce1c4b559aabaaee2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3829d938a053145ee28e4056911aab8aaa169f4265462d9ce1c4b559aabaaee2->enter($__internal_3829d938a053145ee28e4056911aab8aaa169f4265462d9ce1c4b559aabaaee2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c9dfce3fadb71fdb4eabc4cedd1ff934bba612ac31a8fd2479623445c72e1070->leave($__internal_c9dfce3fadb71fdb4eabc4cedd1ff934bba612ac31a8fd2479623445c72e1070_prof);

        
        $__internal_3829d938a053145ee28e4056911aab8aaa169f4265462d9ce1c4b559aabaaee2->leave($__internal_3829d938a053145ee28e4056911aab8aaa169f4265462d9ce1c4b559aabaaee2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c9c6084c100bf1beb935a35cb57ba5bc039c1e7c689d86bb51ebdca2dae00ad1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9c6084c100bf1beb935a35cb57ba5bc039c1e7c689d86bb51ebdca2dae00ad1->enter($__internal_c9c6084c100bf1beb935a35cb57ba5bc039c1e7c689d86bb51ebdca2dae00ad1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_df5cd266a7d447ec18e6b65874a93f46f3c4ddb3fc2465aac3b7713bde0edd65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df5cd266a7d447ec18e6b65874a93f46f3c4ddb3fc2465aac3b7713bde0edd65->enter($__internal_df5cd266a7d447ec18e6b65874a93f46f3c4ddb3fc2465aac3b7713bde0edd65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_df5cd266a7d447ec18e6b65874a93f46f3c4ddb3fc2465aac3b7713bde0edd65->leave($__internal_df5cd266a7d447ec18e6b65874a93f46f3c4ddb3fc2465aac3b7713bde0edd65_prof);

        
        $__internal_c9c6084c100bf1beb935a35cb57ba5bc039c1e7c689d86bb51ebdca2dae00ad1->leave($__internal_c9c6084c100bf1beb935a35cb57ba5bc039c1e7c689d86bb51ebdca2dae00ad1_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/ChangePassword/change_password_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:ChangePassword:change_password.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
