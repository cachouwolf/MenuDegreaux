<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_bbece9301334c97e486868f7d7bf1920554dd44c08ce7c5ec8f39454ae1642b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70d0450932b13f44c5cea3f8fec7f1653426de49c0c9f1d8da45b3fe25d474fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70d0450932b13f44c5cea3f8fec7f1653426de49c0c9f1d8da45b3fe25d474fb->enter($__internal_70d0450932b13f44c5cea3f8fec7f1653426de49c0c9f1d8da45b3fe25d474fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_c9dcaa7586593073b7f752ac4658ea90272639b0321987681c4a2793dd488d27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9dcaa7586593073b7f752ac4658ea90272639b0321987681c4a2793dd488d27->enter($__internal_c9dcaa7586593073b7f752ac4658ea90272639b0321987681c4a2793dd488d27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_70d0450932b13f44c5cea3f8fec7f1653426de49c0c9f1d8da45b3fe25d474fb->leave($__internal_70d0450932b13f44c5cea3f8fec7f1653426de49c0c9f1d8da45b3fe25d474fb_prof);

        
        $__internal_c9dcaa7586593073b7f752ac4658ea90272639b0321987681c4a2793dd488d27->leave($__internal_c9dcaa7586593073b7f752ac4658ea90272639b0321987681c4a2793dd488d27_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_2aae228a4db25babb336c80ed9729189ae423ce975f63f3896a0b51371f9aac3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2aae228a4db25babb336c80ed9729189ae423ce975f63f3896a0b51371f9aac3->enter($__internal_2aae228a4db25babb336c80ed9729189ae423ce975f63f3896a0b51371f9aac3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_443ac0a3c832b32bfd34148559574aad0a6792a8795d4602ea4efad02cf48b99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_443ac0a3c832b32bfd34148559574aad0a6792a8795d4602ea4efad02cf48b99->enter($__internal_443ac0a3c832b32bfd34148559574aad0a6792a8795d4602ea4efad02cf48b99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_443ac0a3c832b32bfd34148559574aad0a6792a8795d4602ea4efad02cf48b99->leave($__internal_443ac0a3c832b32bfd34148559574aad0a6792a8795d4602ea4efad02cf48b99_prof);

        
        $__internal_2aae228a4db25babb336c80ed9729189ae423ce975f63f3896a0b51371f9aac3->leave($__internal_2aae228a4db25babb336c80ed9729189ae423ce975f63f3896a0b51371f9aac3_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_8242c59db75d9e6c976977cad97d324e19b9efa892341d3d5a3f088f5084fcb6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8242c59db75d9e6c976977cad97d324e19b9efa892341d3d5a3f088f5084fcb6->enter($__internal_8242c59db75d9e6c976977cad97d324e19b9efa892341d3d5a3f088f5084fcb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_c628a8f7a637569e9344c4c15677581d6bb1fd68d33eee22250f9e364f0e1fee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c628a8f7a637569e9344c4c15677581d6bb1fd68d33eee22250f9e364f0e1fee->enter($__internal_c628a8f7a637569e9344c4c15677581d6bb1fd68d33eee22250f9e364f0e1fee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_c628a8f7a637569e9344c4c15677581d6bb1fd68d33eee22250f9e364f0e1fee->leave($__internal_c628a8f7a637569e9344c4c15677581d6bb1fd68d33eee22250f9e364f0e1fee_prof);

        
        $__internal_8242c59db75d9e6c976977cad97d324e19b9efa892341d3d5a3f088f5084fcb6->leave($__internal_8242c59db75d9e6c976977cad97d324e19b9efa892341d3d5a3f088f5084fcb6_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_6660607df4de976fb4ac12966a03d1831192d402af2fefea91c646057335bd41 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6660607df4de976fb4ac12966a03d1831192d402af2fefea91c646057335bd41->enter($__internal_6660607df4de976fb4ac12966a03d1831192d402af2fefea91c646057335bd41_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_deff8ec530e557967f2ee7895af88c74de976bb45ed7609352ccd449df6defc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_deff8ec530e557967f2ee7895af88c74de976bb45ed7609352ccd449df6defc4->enter($__internal_deff8ec530e557967f2ee7895af88c74de976bb45ed7609352ccd449df6defc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_deff8ec530e557967f2ee7895af88c74de976bb45ed7609352ccd449df6defc4->leave($__internal_deff8ec530e557967f2ee7895af88c74de976bb45ed7609352ccd449df6defc4_prof);

        
        $__internal_6660607df4de976fb4ac12966a03d1831192d402af2fefea91c646057335bd41->leave($__internal_6660607df4de976fb4ac12966a03d1831192d402af2fefea91c646057335bd41_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
