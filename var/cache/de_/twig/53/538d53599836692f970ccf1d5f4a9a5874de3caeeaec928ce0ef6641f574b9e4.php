<?php

/* SonataBlockBundle:Block:block_core_menu.html.twig */
class __TwigTemplate_3020efd108838855eeca8943cd3db9e195d2f6c57bc00b7c4404df713baa2e4e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_menu.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c71a520dac3b3d341d62b6f803757dad29f7cb7e2f3a18cc8f3efd32c4bab3a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c71a520dac3b3d341d62b6f803757dad29f7cb7e2f3a18cc8f3efd32c4bab3a2->enter($__internal_c71a520dac3b3d341d62b6f803757dad29f7cb7e2f3a18cc8f3efd32c4bab3a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_menu.html.twig"));

        $__internal_58b5bf2cb055dd1ad3418658e0336245f5bb2bcf12756d270fe7c74313859636 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58b5bf2cb055dd1ad3418658e0336245f5bb2bcf12756d270fe7c74313859636->enter($__internal_58b5bf2cb055dd1ad3418658e0336245f5bb2bcf12756d270fe7c74313859636_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_menu.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c71a520dac3b3d341d62b6f803757dad29f7cb7e2f3a18cc8f3efd32c4bab3a2->leave($__internal_c71a520dac3b3d341d62b6f803757dad29f7cb7e2f3a18cc8f3efd32c4bab3a2_prof);

        
        $__internal_58b5bf2cb055dd1ad3418658e0336245f5bb2bcf12756d270fe7c74313859636->leave($__internal_58b5bf2cb055dd1ad3418658e0336245f5bb2bcf12756d270fe7c74313859636_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_a3a8d865192bb59be7b61da09b398d95721e6a50b84ba62e589ec3c390112a15 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3a8d865192bb59be7b61da09b398d95721e6a50b84ba62e589ec3c390112a15->enter($__internal_a3a8d865192bb59be7b61da09b398d95721e6a50b84ba62e589ec3c390112a15_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_46307df470076ad96c3851f864916228ad92f884a6acb539ebab9beee70dbdb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46307df470076ad96c3851f864916228ad92f884a6acb539ebab9beee70dbdb6->enter($__internal_46307df470076ad96c3851f864916228ad92f884a6acb539ebab9beee70dbdb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render((isset($context["menu"]) ? $context["menu"] : $this->getContext($context, "menu")), (isset($context["menu_options"]) ? $context["menu_options"] : $this->getContext($context, "menu_options")));
        echo "
";
        
        $__internal_46307df470076ad96c3851f864916228ad92f884a6acb539ebab9beee70dbdb6->leave($__internal_46307df470076ad96c3851f864916228ad92f884a6acb539ebab9beee70dbdb6_prof);

        
        $__internal_a3a8d865192bb59be7b61da09b398d95721e6a50b84ba62e589ec3c390112a15->leave($__internal_a3a8d865192bb59be7b61da09b398d95721e6a50b84ba62e589ec3c390112a15_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ knp_menu_render(menu, menu_options) }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_menu.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_core_menu.html.twig");
    }
}
