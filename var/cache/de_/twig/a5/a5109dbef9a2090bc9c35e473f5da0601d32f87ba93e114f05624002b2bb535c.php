<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_07bf3899502d2bec57a4ee204243df92fe337e391f9b3c936b353c7b99d1688e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7027b9a0f08f3157125edacf6016dbb3096f5cdc794e50eb05012022af779119 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7027b9a0f08f3157125edacf6016dbb3096f5cdc794e50eb05012022af779119->enter($__internal_7027b9a0f08f3157125edacf6016dbb3096f5cdc794e50eb05012022af779119_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $__internal_5dc3373afe06d9da35061b898f74fae5c447ec62905bc54de03910ff3e69fd16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dc3373afe06d9da35061b898f74fae5c447ec62905bc54de03910ff3e69fd16->enter($__internal_5dc3373afe06d9da35061b898f74fae5c447ec62905bc54de03910ff3e69fd16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7027b9a0f08f3157125edacf6016dbb3096f5cdc794e50eb05012022af779119->leave($__internal_7027b9a0f08f3157125edacf6016dbb3096f5cdc794e50eb05012022af779119_prof);

        
        $__internal_5dc3373afe06d9da35061b898f74fae5c447ec62905bc54de03910ff3e69fd16->leave($__internal_5dc3373afe06d9da35061b898f74fae5c447ec62905bc54de03910ff3e69fd16_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_fce9fec3d155e6320da354997846c052cb2c04c3c7d4c88fe319a224cac7e29c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fce9fec3d155e6320da354997846c052cb2c04c3c7d4c88fe319a224cac7e29c->enter($__internal_fce9fec3d155e6320da354997846c052cb2c04c3c7d4c88fe319a224cac7e29c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_6e749ec65c878d4a40fa76e0d3b05985b34c60d1a05a0e4e5b4daf27a563aa04 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e749ec65c878d4a40fa76e0d3b05985b34c60d1a05a0e4e5b4daf27a563aa04->enter($__internal_6e749ec65c878d4a40fa76e0d3b05985b34c60d1a05a0e4e5b4daf27a563aa04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_6e749ec65c878d4a40fa76e0d3b05985b34c60d1a05a0e4e5b4daf27a563aa04->leave($__internal_6e749ec65c878d4a40fa76e0d3b05985b34c60d1a05a0e4e5b4daf27a563aa04_prof);

        
        $__internal_fce9fec3d155e6320da354997846c052cb2c04c3c7d4c88fe319a224cac7e29c->leave($__internal_fce9fec3d155e6320da354997846c052cb2c04c3c7d4c88fe319a224cac7e29c_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/list_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:list.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/list.html.twig");
    }
}
