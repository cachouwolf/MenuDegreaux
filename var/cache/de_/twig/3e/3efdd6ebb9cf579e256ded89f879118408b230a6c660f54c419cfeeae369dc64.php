<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_a33cb803934834c15693f72380de79c2d031f443e2eaf586e064b1feb01de6ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e16e730334bd5aba65e0e148b27bbafc0aa78670d389e89a0c57ec49f0ea316c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e16e730334bd5aba65e0e148b27bbafc0aa78670d389e89a0c57ec49f0ea316c->enter($__internal_e16e730334bd5aba65e0e148b27bbafc0aa78670d389e89a0c57ec49f0ea316c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_173e015a41ca84a708fa87dd0e3a410c095776222411a60917dee15e8de3a873 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_173e015a41ca84a708fa87dd0e3a410c095776222411a60917dee15e8de3a873->enter($__internal_173e015a41ca84a708fa87dd0e3a410c095776222411a60917dee15e8de3a873_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_e16e730334bd5aba65e0e148b27bbafc0aa78670d389e89a0c57ec49f0ea316c->leave($__internal_e16e730334bd5aba65e0e148b27bbafc0aa78670d389e89a0c57ec49f0ea316c_prof);

        
        $__internal_173e015a41ca84a708fa87dd0e3a410c095776222411a60917dee15e8de3a873->leave($__internal_173e015a41ca84a708fa87dd0e3a410c095776222411a60917dee15e8de3a873_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_container_attributes.html.php");
    }
}
