<?php

/* base.html.twig */
class __TwigTemplate_0f21059a07985fdd2226432bf73a916d68637c40346a5edd42a7522ed1832432 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e52dd6cb3c7bbd32925b8af2f9da42b4652e56bdd773873ada2c0e68197ca3e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e52dd6cb3c7bbd32925b8af2f9da42b4652e56bdd773873ada2c0e68197ca3e5->enter($__internal_e52dd6cb3c7bbd32925b8af2f9da42b4652e56bdd773873ada2c0e68197ca3e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_82512fde9269814ac3e50880f21df4082c7b4482948362e5d3346388172ac927 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82512fde9269814ac3e50880f21df4082c7b4482948362e5d3346388172ac927->enter($__internal_82512fde9269814ac3e50880f21df4082c7b4482948362e5d3346388172ac927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 143
        echo "
        <div class=\"container body-container\">
            ";
        // line 145
        $this->displayBlock('body', $context, $blocks);
        // line 159
        echo "        </div>

        ";
        // line 161
        $this->displayBlock('footer', $context, $blocks);
        // line 172
        echo "
        ";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "

    </body>
</html>
";
        
        $__internal_e52dd6cb3c7bbd32925b8af2f9da42b4652e56bdd773873ada2c0e68197ca3e5->leave($__internal_e52dd6cb3c7bbd32925b8af2f9da42b4652e56bdd773873ada2c0e68197ca3e5_prof);

        
        $__internal_82512fde9269814ac3e50880f21df4082c7b4482948362e5d3346388172ac927->leave($__internal_82512fde9269814ac3e50880f21df4082c7b4482948362e5d3346388172ac927_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_09e065652e0448ddfb0de87fc2ecbabca35ace00e8a82fe9dbdec13d2a9f4456 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09e065652e0448ddfb0de87fc2ecbabca35ace00e8a82fe9dbdec13d2a9f4456->enter($__internal_09e065652e0448ddfb0de87fc2ecbabca35ace00e8a82fe9dbdec13d2a9f4456_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_5a93d690ef29dbbf5b32e5da2196ce3393a56f045ff770cc8d403749d4e75642 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a93d690ef29dbbf5b32e5da2196ce3393a56f045ff770cc8d403749d4e75642->enter($__internal_5a93d690ef29dbbf5b32e5da2196ce3393a56f045ff770cc8d403749d4e75642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Degreaux";
        
        $__internal_5a93d690ef29dbbf5b32e5da2196ce3393a56f045ff770cc8d403749d4e75642->leave($__internal_5a93d690ef29dbbf5b32e5da2196ce3393a56f045ff770cc8d403749d4e75642_prof);

        
        $__internal_09e065652e0448ddfb0de87fc2ecbabca35ace00e8a82fe9dbdec13d2a9f4456->leave($__internal_09e065652e0448ddfb0de87fc2ecbabca35ace00e8a82fe9dbdec13d2a9f4456_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_290d2ae4083775f3066702a55c4219935f4f77679b76af9535f3f3b394106e50 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_290d2ae4083775f3066702a55c4219935f4f77679b76af9535f3f3b394106e50->enter($__internal_290d2ae4083775f3066702a55c4219935f4f77679b76af9535f3f3b394106e50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_6ddf04a74936f2264252eaaa2cfbce47ae5939c6b2fca403a7aef035a83e34ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6ddf04a74936f2264252eaaa2cfbce47ae5939c6b2fca403a7aef035a83e34ff->enter($__internal_6ddf04a74936f2264252eaaa2cfbce47ae5939c6b2fca403a7aef035a83e34ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "
        ";
        
        $__internal_6ddf04a74936f2264252eaaa2cfbce47ae5939c6b2fca403a7aef035a83e34ff->leave($__internal_6ddf04a74936f2264252eaaa2cfbce47ae5939c6b2fca403a7aef035a83e34ff_prof);

        
        $__internal_290d2ae4083775f3066702a55c4219935f4f77679b76af9535f3f3b394106e50->leave($__internal_290d2ae4083775f3066702a55c4219935f4f77679b76af9535f3f3b394106e50_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_bb21fda23ff0a2408a20320f2937cee9619425dff3e3530ba493077045c4ab9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb21fda23ff0a2408a20320f2937cee9619425dff3e3530ba493077045c4ab9d->enter($__internal_bb21fda23ff0a2408a20320f2937cee9619425dff3e3530ba493077045c4ab9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_e3b8ce061fe5bdcd6ab5ba01d9007ade65eb2d9e097d83581f13825b919f1a01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3b8ce061fe5bdcd6ab5ba01d9007ade65eb2d9e097d83581f13825b919f1a01->enter($__internal_e3b8ce061fe5bdcd6ab5ba01d9007ade65eb2d9e097d83581f13825b919f1a01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_e3b8ce061fe5bdcd6ab5ba01d9007ade65eb2d9e097d83581f13825b919f1a01->leave($__internal_e3b8ce061fe5bdcd6ab5ba01d9007ade65eb2d9e097d83581f13825b919f1a01_prof);

        
        $__internal_bb21fda23ff0a2408a20320f2937cee9619425dff3e3530ba493077045c4ab9d->leave($__internal_bb21fda23ff0a2408a20320f2937cee9619425dff3e3530ba493077045c4ab9d_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_bbdaf7fb5d20d06f551f2fdc4ba4274b27b67abde0f53ec05d64c9c160e6f377 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bbdaf7fb5d20d06f551f2fdc4ba4274b27b67abde0f53ec05d64c9c160e6f377->enter($__internal_bbdaf7fb5d20d06f551f2fdc4ba4274b27b67abde0f53ec05d64c9c160e6f377_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_1de23548576eb7df3a3f61619c9e9bea1ffd66ffd88c149e4552f2b669f09de9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1de23548576eb7df3a3f61619c9e9bea1ffd66ffd88c149e4552f2b669f09de9->enter($__internal_1de23548576eb7df3a3f61619c9e9bea1ffd66ffd88c149e4552f2b669f09de9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 132
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_1de23548576eb7df3a3f61619c9e9bea1ffd66ffd88c149e4552f2b669f09de9->leave($__internal_1de23548576eb7df3a3f61619c9e9bea1ffd66ffd88c149e4552f2b669f09de9_prof);

        
        $__internal_bbdaf7fb5d20d06f551f2fdc4ba4274b27b67abde0f53ec05d64c9c160e6f377->leave($__internal_bbdaf7fb5d20d06f551f2fdc4ba4274b27b67abde0f53ec05d64c9c160e6f377_prof);

    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_6cc89dbc30384c58ab72516f775dec6d1410ec10ab29ab44994927107865acda = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6cc89dbc30384c58ab72516f775dec6d1410ec10ab29ab44994927107865acda->enter($__internal_6cc89dbc30384c58ab72516f775dec6d1410ec10ab29ab44994927107865acda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        $__internal_7bb783bc5261e7a1edb109b4a220f40f909580eaff83c3fa55d4d6b7390b1b33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7bb783bc5261e7a1edb109b4a220f40f909580eaff83c3fa55d4d6b7390b1b33->enter($__internal_7bb783bc5261e7a1edb109b4a220f40f909580eaff83c3fa55d4d6b7390b1b33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 98
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 99
            echo "                                        <li>
                                            <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("{_locale}adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 106
        echo "
                                    ";
        // line 107
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 108
            echo "                                        <li>
                                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 115
            echo "                                        <li>
                                            <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 122
        echo "
                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 124
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 125
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 126
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "
                                ";
        
        $__internal_7bb783bc5261e7a1edb109b4a220f40f909580eaff83c3fa55d4d6b7390b1b33->leave($__internal_7bb783bc5261e7a1edb109b4a220f40f909580eaff83c3fa55d4d6b7390b1b33_prof);

        
        $__internal_6cc89dbc30384c58ab72516f775dec6d1410ec10ab29ab44994927107865acda->leave($__internal_6cc89dbc30384c58ab72516f775dec6d1410ec10ab29ab44994927107865acda_prof);

    }

    // line 145
    public function block_body($context, array $blocks = array())
    {
        $__internal_182425cfe4421e08a61857f3838152805517867ff8c69ca979fda004162332c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_182425cfe4421e08a61857f3838152805517867ff8c69ca979fda004162332c6->enter($__internal_182425cfe4421e08a61857f3838152805517867ff8c69ca979fda004162332c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_04189686dbe76b4f9b082f8c92ef12edde69fe2cb5dbde46ee6045f97298e7b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04189686dbe76b4f9b082f8c92ef12edde69fe2cb5dbde46ee6045f97298e7b4->enter($__internal_04189686dbe76b4f9b082f8c92ef12edde69fe2cb5dbde46ee6045f97298e7b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 146
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        ";
        // line 149
        $this->displayBlock('main', $context, $blocks);
        // line 151
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        ";
        // line 154
        $this->displayBlock('sidebar', $context, $blocks);
        // line 156
        echo "                    </div>
                </div>
            ";
        
        $__internal_04189686dbe76b4f9b082f8c92ef12edde69fe2cb5dbde46ee6045f97298e7b4->leave($__internal_04189686dbe76b4f9b082f8c92ef12edde69fe2cb5dbde46ee6045f97298e7b4_prof);

        
        $__internal_182425cfe4421e08a61857f3838152805517867ff8c69ca979fda004162332c6->leave($__internal_182425cfe4421e08a61857f3838152805517867ff8c69ca979fda004162332c6_prof);

    }

    // line 149
    public function block_main($context, array $blocks = array())
    {
        $__internal_60fa1c49a2e050c930a97a521a4b81e8694f46a3cb7310a5fb8e787d78d37663 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60fa1c49a2e050c930a97a521a4b81e8694f46a3cb7310a5fb8e787d78d37663->enter($__internal_60fa1c49a2e050c930a97a521a4b81e8694f46a3cb7310a5fb8e787d78d37663_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_625929c6bb3a6184d907dd2109eb8c548ab236dd0edf791e8ff28d010510fc40 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_625929c6bb3a6184d907dd2109eb8c548ab236dd0edf791e8ff28d010510fc40->enter($__internal_625929c6bb3a6184d907dd2109eb8c548ab236dd0edf791e8ff28d010510fc40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 150
        echo "                        ";
        
        $__internal_625929c6bb3a6184d907dd2109eb8c548ab236dd0edf791e8ff28d010510fc40->leave($__internal_625929c6bb3a6184d907dd2109eb8c548ab236dd0edf791e8ff28d010510fc40_prof);

        
        $__internal_60fa1c49a2e050c930a97a521a4b81e8694f46a3cb7310a5fb8e787d78d37663->leave($__internal_60fa1c49a2e050c930a97a521a4b81e8694f46a3cb7310a5fb8e787d78d37663_prof);

    }

    // line 154
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_dff4888caa8f5618e854b1523792799cb1129481c9685b33500c34f8de50a6d7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dff4888caa8f5618e854b1523792799cb1129481c9685b33500c34f8de50a6d7->enter($__internal_dff4888caa8f5618e854b1523792799cb1129481c9685b33500c34f8de50a6d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_d0bebeec16f94b832cf285328617424d484c4494e4fb6a0dcdf77b502c676890 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d0bebeec16f94b832cf285328617424d484c4494e4fb6a0dcdf77b502c676890->enter($__internal_d0bebeec16f94b832cf285328617424d484c4494e4fb6a0dcdf77b502c676890_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 155
        echo "                        ";
        
        $__internal_d0bebeec16f94b832cf285328617424d484c4494e4fb6a0dcdf77b502c676890->leave($__internal_d0bebeec16f94b832cf285328617424d484c4494e4fb6a0dcdf77b502c676890_prof);

        
        $__internal_dff4888caa8f5618e854b1523792799cb1129481c9685b33500c34f8de50a6d7->leave($__internal_dff4888caa8f5618e854b1523792799cb1129481c9685b33500c34f8de50a6d7_prof);

    }

    // line 161
    public function block_footer($context, array $blocks = array())
    {
        $__internal_b12ca93eb959222bf2e5bebccaac383202921ba93ec9f793341a3c5c0192ea10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b12ca93eb959222bf2e5bebccaac383202921ba93ec9f793341a3c5c0192ea10->enter($__internal_b12ca93eb959222bf2e5bebccaac383202921ba93ec9f793341a3c5c0192ea10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_4f5989662cd35ee61030a42f8875bd9255553607618e17c887642dbfe1d1e6eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f5989662cd35ee61030a42f8875bd9255553607618e17c887642dbfe1d1e6eb->enter($__internal_4f5989662cd35ee61030a42f8875bd9255553607618e17c887642dbfe1d1e6eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 162
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_4f5989662cd35ee61030a42f8875bd9255553607618e17c887642dbfe1d1e6eb->leave($__internal_4f5989662cd35ee61030a42f8875bd9255553607618e17c887642dbfe1d1e6eb_prof);

        
        $__internal_b12ca93eb959222bf2e5bebccaac383202921ba93ec9f793341a3c5c0192ea10->leave($__internal_b12ca93eb959222bf2e5bebccaac383202921ba93ec9f793341a3c5c0192ea10_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_25cb05847955e98f7839c914c9b5288a2c4acca48d1a5ea8694d999fa8ed3124 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25cb05847955e98f7839c914c9b5288a2c4acca48d1a5ea8694d999fa8ed3124->enter($__internal_25cb05847955e98f7839c914c9b5288a2c4acca48d1a5ea8694d999fa8ed3124_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_6bae2db4727fd3c09288a79b213122dae305b9acdf173ad605f7887d5a469801 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bae2db4727fd3c09288a79b213122dae305b9acdf173ad605f7887d5a469801->enter($__internal_6bae2db4727fd3c09288a79b213122dae305b9acdf173ad605f7887d5a469801_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_6bae2db4727fd3c09288a79b213122dae305b9acdf173ad605f7887d5a469801->leave($__internal_6bae2db4727fd3c09288a79b213122dae305b9acdf173ad605f7887d5a469801_prof);

        
        $__internal_25cb05847955e98f7839c914c9b5288a2c4acca48d1a5ea8694d999fa8ed3124->leave($__internal_25cb05847955e98f7839c914c9b5288a2c4acca48d1a5ea8694d999fa8ed3124_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  558 => 180,  554 => 179,  550 => 178,  546 => 177,  542 => 176,  538 => 175,  533 => 174,  524 => 173,  508 => 166,  502 => 162,  493 => 161,  483 => 155,  474 => 154,  464 => 150,  455 => 149,  443 => 156,  441 => 154,  436 => 151,  434 => 149,  427 => 146,  418 => 145,  407 => 130,  401 => 129,  392 => 126,  387 => 125,  382 => 124,  378 => 123,  375 => 122,  368 => 118,  363 => 116,  360 => 115,  353 => 111,  348 => 109,  345 => 108,  343 => 107,  340 => 106,  331 => 100,  328 => 99,  326 => 98,  302 => 77,  298 => 75,  292 => 71,  284 => 66,  281 => 65,  279 => 64,  272 => 60,  264 => 55,  256 => 50,  252 => 48,  250 => 47,  247 => 46,  238 => 45,  218 => 132,  216 => 45,  199 => 31,  193 => 27,  184 => 26,  167 => 24,  156 => 20,  147 => 19,  129 => 6,  115 => 182,  113 => 173,  110 => 172,  108 => 161,  104 => 159,  102 => 145,  98 => 143,  96 => 26,  91 => 24,  87 => 22,  85 => 19,  81 => 18,  76 => 16,  72 => 15,  68 => 14,  64 => 13,  60 => 12,  56 => 11,  52 => 10,  45 => 6,  38 => 2,  35 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Menu Degreaux{% endblock %}</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.png') }}\" >
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/css_sass/css/style.css') }}\">
        {% block stylesheets %}

        {% endblock %}
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                {% block header_navigation_links %}

                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/profile') }}\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/resseting') }}\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    {% if is_granted(\"ROLE_ADMIN\") %}
                                                        <li>
                                                            <a href=\"{{ asset('/{_locale}/register') }}\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                  </ul>
                                                </li>

                                    {% endif %}

                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li>
                                            <a href=\"{{ asset('{_locale}adminSonata') }}\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                        {% else %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_login') }}\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                {{ 'layout.login'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% for type, messages in app.session.flashBag.all %}
                                        {% for message in messages %}
                                            <div class=\"{{ type }}\">
                                                {{ message|trans({}, 'FOSUserBundle') }}
                                            </div>
                                        {% endfor %}
                                    {% endfor %}

                                {% endblock %}





                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                {{ include('default/_flash_messages.html.twig') }}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        {% block main %}
                        {% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        {% block sidebar %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; {{ 'now'|date('Y') }} - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}


    </body>
</html>
", "base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\base.html.twig");
    }
}
