<?php

/* :menu:homepage.html.twig */
class __TwigTemplate_f13c6430015ed6b0d8001f5807f71817aeece96a83734fa15e2081b0b2cec3cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":menu:homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78a5677ca5d4133793b83f85aaf5f5bf2f92dd6dd18c76ca55102089186f208e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78a5677ca5d4133793b83f85aaf5f5bf2f92dd6dd18c76ca55102089186f208e->enter($__internal_78a5677ca5d4133793b83f85aaf5f5bf2f92dd6dd18c76ca55102089186f208e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:homepage.html.twig"));

        $__internal_854c29ce3f7354442f77d95b9d105655649f00f67987361e5a491306bbaf0312 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_854c29ce3f7354442f77d95b9d105655649f00f67987361e5a491306bbaf0312->enter($__internal_854c29ce3f7354442f77d95b9d105655649f00f67987361e5a491306bbaf0312_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_78a5677ca5d4133793b83f85aaf5f5bf2f92dd6dd18c76ca55102089186f208e->leave($__internal_78a5677ca5d4133793b83f85aaf5f5bf2f92dd6dd18c76ca55102089186f208e_prof);

        
        $__internal_854c29ce3f7354442f77d95b9d105655649f00f67987361e5a491306bbaf0312->leave($__internal_854c29ce3f7354442f77d95b9d105655649f00f67987361e5a491306bbaf0312_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_4419919bd4c894fbcb5b74bf7e13b7d5171ec7dab2f1e2f15b44efd363e1ff6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4419919bd4c894fbcb5b74bf7e13b7d5171ec7dab2f1e2f15b44efd363e1ff6e->enter($__internal_4419919bd4c894fbcb5b74bf7e13b7d5171ec7dab2f1e2f15b44efd363e1ff6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_5e1a11da35447662baf316128f307634c2e6d9fad1a91d70e35103cbb5a41568 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e1a11da35447662baf316128f307634c2e6d9fad1a91d70e35103cbb5a41568->enter($__internal_5e1a11da35447662baf316128f307634c2e6d9fad1a91d70e35103cbb5a41568_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_5e1a11da35447662baf316128f307634c2e6d9fad1a91d70e35103cbb5a41568->leave($__internal_5e1a11da35447662baf316128f307634c2e6d9fad1a91d70e35103cbb5a41568_prof);

        
        $__internal_4419919bd4c894fbcb5b74bf7e13b7d5171ec7dab2f1e2f15b44efd363e1ff6e->leave($__internal_4419919bd4c894fbcb5b74bf7e13b7d5171ec7dab2f1e2f15b44efd363e1ff6e_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_61fd5002c9ed6f97e145b6f19b5f9adb486ff8af7bde8c1eea58ef22399e9a6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61fd5002c9ed6f97e145b6f19b5f9adb486ff8af7bde8c1eea58ef22399e9a6b->enter($__internal_61fd5002c9ed6f97e145b6f19b5f9adb486ff8af7bde8c1eea58ef22399e9a6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_248c32cdb9865c9fa60e6e51a57838d2c2a39488f31299d6a7e6ada36c689da6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_248c32cdb9865c9fa60e6e51a57838d2c2a39488f31299d6a7e6ada36c689da6->enter($__internal_248c32cdb9865c9fa60e6e51a57838d2c2a39488f31299d6a7e6ada36c689da6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
";
        
        $__internal_248c32cdb9865c9fa60e6e51a57838d2c2a39488f31299d6a7e6ada36c689da6->leave($__internal_248c32cdb9865c9fa60e6e51a57838d2c2a39488f31299d6a7e6ada36c689da6_prof);

        
        $__internal_61fd5002c9ed6f97e145b6f19b5f9adb486ff8af7bde8c1eea58ef22399e9a6b->leave($__internal_61fd5002c9ed6f97e145b6f19b5f9adb486ff8af7bde8c1eea58ef22399e9a6b_prof);

    }

    public function getTemplateName()
    {
        return ":menu:homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  85 => 18,  75 => 11,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}

{% block main %}
    <h1> Bienvenue sur le menu Degreaux</h1>
    <!-- choix 3 btn -->
    <div class=\"row categories\">
      <p> Vous êtes : </p>
      <div class=\"col-md-4 col-xs-12\">
          <a href=\"{{ path('externe') }}\" >
            <button id=\"externebtn\" type=\"button\" class=\"btn btn-default\" >
                Externe
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a  href=\"{{ path('scolaire') }}\">
            <button id=\"scolairebtn\"  type=\"button\" class=\"btn btn-default \">
                Scolaire
            </button>
        </a>
      </div>
      <div class=\"col-md-4 col-xs-12\">
        <a href=\"{{ path('resident') }}\" >
            <button id=\"residentbtn\"   type=\"button\" class=\"btn btn-default \">
                Résident
            </button>
        </a>
      </div>
    </div>
{% endblock %}
", ":menu:homepage.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/menu/homepage.html.twig");
    }
}
