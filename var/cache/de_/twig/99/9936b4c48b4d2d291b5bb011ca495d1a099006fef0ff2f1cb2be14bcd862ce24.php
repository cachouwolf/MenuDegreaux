<?php

/* FOSUserBundle:Security:login_content.html.twig */
class __TwigTemplate_0d420f426c690090b03435b462818e208c5c8d2b55a398f8a4cc2437df4cf499 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fbab3ba991a90c4346868334e7d158460dd2c469fddb8dbd259c8a0fd6d1919 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fbab3ba991a90c4346868334e7d158460dd2c469fddb8dbd259c8a0fd6d1919->enter($__internal_5fbab3ba991a90c4346868334e7d158460dd2c469fddb8dbd259c8a0fd6d1919_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        $__internal_cacb904e8a49be906710defc8c91d4b13fe5cf1079d70ae78da3be07c51587dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cacb904e8a49be906710defc8c91d4b13fe5cf1079d70ae78da3be07c51587dc->enter($__internal_cacb904e8a49be906710defc8c91d4b13fe5cf1079d70ae78da3be07c51587dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login_content.html.twig"));

        // line 2
        echo "<h1>Se connecter</h1>

";
        // line 4
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 5
            echo "<p style=\"color:red; font-weight:bold; font-size:15px;\">
        ";
            // line 6
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
</p>
";
        }
        // line 9
        echo "<form action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
    ";
        // line 10
        if ((isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token"))) {
            // line 11
            echo "        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
    ";
        }
        // line 13
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">
            <label for=\"username\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" required=\"required\" />
        </div>
        <div class=\"col-sm-12\">
            <label for=\"password\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />
        </div>
        <div class=\"col-sm-12\">
            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
            <label for=\"remember_me\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
        </div>
        ";
        // line 26
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 27
            echo "            <div class=\"erreur\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
        ";
        }
        // line 29
        echo "



        <div class=\"col-sm-12\">
            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\" class=\"btn btn-success\" />
        </div>

    </div>
</form>
";
        
        $__internal_5fbab3ba991a90c4346868334e7d158460dd2c469fddb8dbd259c8a0fd6d1919->leave($__internal_5fbab3ba991a90c4346868334e7d158460dd2c469fddb8dbd259c8a0fd6d1919_prof);

        
        $__internal_cacb904e8a49be906710defc8c91d4b13fe5cf1079d70ae78da3be07c51587dc->leave($__internal_cacb904e8a49be906710defc8c91d4b13fe5cf1079d70ae78da3be07c51587dc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 34,  88 => 29,  82 => 27,  80 => 26,  75 => 24,  67 => 19,  61 => 16,  57 => 15,  53 => 13,  47 => 11,  45 => 10,  40 => 9,  34 => 6,  31 => 5,  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
<h1>Se connecter</h1>

{% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
<p style=\"color:red; font-weight:bold; font-size:15px;\">
        {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
</p>
{% endif %}
<form action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
    {% if csrf_token %}
        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
    {% endif %}
    <div class=\"row\">
        <div class=\"col-sm-12\">
            <label for=\"username\">{{ 'security.login.username'|trans }}</label>
            <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" required=\"required\" />
        </div>
        <div class=\"col-sm-12\">
            <label for=\"password\">{{ 'security.login.password'|trans }}</label>
            <input type=\"password\" id=\"password\" name=\"_password\" required=\"required\" />
        </div>
        <div class=\"col-sm-12\">
            <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
            <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>
        </div>
        {% if error %}
            <div class=\"erreur\">{{ error.messageKey|trans(error.messageData, 'security') }}</div>
        {% endif %}




        <div class=\"col-sm-12\">
            <input type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\" class=\"btn btn-success\" />
        </div>

    </div>
</form>
", "FOSUserBundle:Security:login_content.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources/FOSUserBundle/views/Security/login_content.html.twig");
    }
}
