<?php

/* @SonataBlock/Profiler/icon.svg */
class __TwigTemplate_86a2a5bc110bdcdaf567a248509d58941350d34359fa8c73e66fa29543f6b88e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37164ff2ab076296b572eedf4ff82221cc3f9f8ff88f09653f0e75e83994bf2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_37164ff2ab076296b572eedf4ff82221cc3f9f8ff88f09653f0e75e83994bf2c->enter($__internal_37164ff2ab076296b572eedf4ff82221cc3f9f8ff88f09653f0e75e83994bf2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        $__internal_924f8c0093fe4997fcf0e25718931ded8282701cfa38686298aaff7a2a2b7d64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_924f8c0093fe4997fcf0e25718931ded8282701cfa38686298aaff7a2a2b7d64->enter($__internal_924f8c0093fe4997fcf0e25718931ded8282701cfa38686298aaff7a2a2b7d64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Profiler/icon.svg"));

        // line 1
        echo "<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
";
        
        $__internal_37164ff2ab076296b572eedf4ff82221cc3f9f8ff88f09653f0e75e83994bf2c->leave($__internal_37164ff2ab076296b572eedf4ff82221cc3f9f8ff88f09653f0e75e83994bf2c_prof);

        
        $__internal_924f8c0093fe4997fcf0e25718931ded8282701cfa38686298aaff7a2a2b7d64->leave($__internal_924f8c0093fe4997fcf0e25718931ded8282701cfa38686298aaff7a2a2b7d64_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Profiler/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg height=\"24\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\">
    <path fill=\"#AAAAAA\" d=\"M832 1024v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm896 768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90zm0-768v384q0 52-38 90t-90 38h-512q-52 0-90-38t-38-90v-384q0-52 38-90t90-38h512q52 0 90 38t38 90z\"/>
</svg>
", "@SonataBlock/Profiler/icon.svg", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Profiler\\icon.svg");
    }
}
