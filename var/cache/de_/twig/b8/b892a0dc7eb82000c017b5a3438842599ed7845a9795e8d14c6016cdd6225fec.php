<?php

/* TwigBundle:Exception:error500.html.twig */
class __TwigTemplate_2e99e3f4fd39a1941942a3e82e79b8fd1d437ab12bad7daa2dbf93f6676abfc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("base.html.twig", "TwigBundle:Exception:error500.html.twig", 11);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8cad8bfca6b84fbd9e88f4daac6cb702c3e2d12dd4ce2c62e7aca38ab934bc63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8cad8bfca6b84fbd9e88f4daac6cb702c3e2d12dd4ce2c62e7aca38ab934bc63->enter($__internal_8cad8bfca6b84fbd9e88f4daac6cb702c3e2d12dd4ce2c62e7aca38ab934bc63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error500.html.twig"));

        $__internal_7482b00ef42092e1f0b275ddb22e6374407b98ec5450f8610208c1ad98e354c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7482b00ef42092e1f0b275ddb22e6374407b98ec5450f8610208c1ad98e354c6->enter($__internal_7482b00ef42092e1f0b275ddb22e6374407b98ec5450f8610208c1ad98e354c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error500.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8cad8bfca6b84fbd9e88f4daac6cb702c3e2d12dd4ce2c62e7aca38ab934bc63->leave($__internal_8cad8bfca6b84fbd9e88f4daac6cb702c3e2d12dd4ce2c62e7aca38ab934bc63_prof);

        
        $__internal_7482b00ef42092e1f0b275ddb22e6374407b98ec5450f8610208c1ad98e354c6->leave($__internal_7482b00ef42092e1f0b275ddb22e6374407b98ec5450f8610208c1ad98e354c6_prof);

    }

    // line 13
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_e4b8115f31e3b2dcdce3314d0919a135b291bc21fa23522b21f569317b3a300f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4b8115f31e3b2dcdce3314d0919a135b291bc21fa23522b21f569317b3a300f->enter($__internal_e4b8115f31e3b2dcdce3314d0919a135b291bc21fa23522b21f569317b3a300f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_e7f4a5962500cddac2db62264418dab2245df32f6d83e0286ebd0e96817c8df0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7f4a5962500cddac2db62264418dab2245df32f6d83e0286ebd0e96817c8df0->enter($__internal_e7f4a5962500cddac2db62264418dab2245df32f6d83e0286ebd0e96817c8df0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "error";
        
        $__internal_e7f4a5962500cddac2db62264418dab2245df32f6d83e0286ebd0e96817c8df0->leave($__internal_e7f4a5962500cddac2db62264418dab2245df32f6d83e0286ebd0e96817c8df0_prof);

        
        $__internal_e4b8115f31e3b2dcdce3314d0919a135b291bc21fa23522b21f569317b3a300f->leave($__internal_e4b8115f31e3b2dcdce3314d0919a135b291bc21fa23522b21f569317b3a300f_prof);

    }

    // line 15
    public function block_main($context, array $blocks = array())
    {
        $__internal_61a5dbb3b355ec1951518c2f3b09deb361eb2b1ad9aab8a3448d8fd1c7616d96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_61a5dbb3b355ec1951518c2f3b09deb361eb2b1ad9aab8a3448d8fd1c7616d96->enter($__internal_61a5dbb3b355ec1951518c2f3b09deb361eb2b1ad9aab8a3448d8fd1c7616d96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_82fef600fad8a786a3a5d53b041af14f7acfac2fd5a9636773a7bd0db0ba461b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_82fef600fad8a786a3a5d53b041af14f7acfac2fd5a9636773a7bd0db0ba461b->enter($__internal_82fef600fad8a786a3a5d53b041af14f7acfac2fd5a9636773a7bd0db0ba461b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 16
        echo "    <h1 class=\"text-danger\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.name", array("%status_code%" => 500)), "html", null, true);
        echo "</h1>

    <p class=\"lead\">
        ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_500.description"), "html", null, true);
        echo "
    </p>
    <p>
        ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_500.suggestion", array("%url%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index")));
        echo "
    </p>
";
        
        $__internal_82fef600fad8a786a3a5d53b041af14f7acfac2fd5a9636773a7bd0db0ba461b->leave($__internal_82fef600fad8a786a3a5d53b041af14f7acfac2fd5a9636773a7bd0db0ba461b_prof);

        
        $__internal_61a5dbb3b355ec1951518c2f3b09deb361eb2b1ad9aab8a3448d8fd1c7616d96->leave($__internal_61a5dbb3b355ec1951518c2f3b09deb361eb2b1ad9aab8a3448d8fd1c7616d96_prof);

    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_c0b4d077a225fc9d3ce42356902774de906f9281eae02f89087adda2f4f5ddd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c0b4d077a225fc9d3ce42356902774de906f9281eae02f89087adda2f4f5ddd6->enter($__internal_c0b4d077a225fc9d3ce42356902774de906f9281eae02f89087adda2f4f5ddd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_4c315c8f144818f76dfe3ff7f522a339185f7556326390c6644fa10c3d1a9cf3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c315c8f144818f76dfe3ff7f522a339185f7556326390c6644fa10c3d1a9cf3->enter($__internal_4c315c8f144818f76dfe3ff7f522a339185f7556326390c6644fa10c3d1a9cf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 27
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 29
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_4c315c8f144818f76dfe3ff7f522a339185f7556326390c6644fa10c3d1a9cf3->leave($__internal_4c315c8f144818f76dfe3ff7f522a339185f7556326390c6644fa10c3d1a9cf3_prof);

        
        $__internal_c0b4d077a225fc9d3ce42356902774de906f9281eae02f89087adda2f4f5ddd6->leave($__internal_c0b4d077a225fc9d3ce42356902774de906f9281eae02f89087adda2f4f5ddd6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error500.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 29,  104 => 27,  95 => 26,  82 => 22,  76 => 19,  69 => 16,  60 => 15,  42 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    This template is used to render errors of type HTTP 500 (Internal Server Error)

    This is the simplest way to customize error pages in Symfony applications.
    In case you need it, you can also hook into the internal exception handling
    made by Symfony. This allows you to perform advanced tasks and even recover
    your application from some errors.
    See http://symfony.com/doc/current/cookbook/controller/error_pages.html
#}

{% extends 'base.html.twig' %}

{% block body_id 'error' %}

{% block main %}
    <h1 class=\"text-danger\">{{ 'http_error.name'|trans({ '%status_code%': 500 }) }}</h1>

    <p class=\"lead\">
        {{ 'http_error_500.description'|trans }}
    </p>
    <p>
        {{ 'http_error_500.suggestion'|trans({ '%url%': path('blog_index') })|raw }}
    </p>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "TwigBundle:Exception:error500.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources/TwigBundle/views/Exception/error500.html.twig");
    }
}
