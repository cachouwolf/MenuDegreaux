<?php

/* SonataAdminBundle:CRUD:show_currency.html.twig */
class __TwigTemplate_1ab3da970f7baf5b4560f272312c63ed3d4b3c7cbf7c54a8fa011eab67a30746 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_currency.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1dc987064178ffe72d1ebd90f3b927bdce49a491b5f79c1b424f206845cfccf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1dc987064178ffe72d1ebd90f3b927bdce49a491b5f79c1b424f206845cfccf8->enter($__internal_1dc987064178ffe72d1ebd90f3b927bdce49a491b5f79c1b424f206845cfccf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_currency.html.twig"));

        $__internal_231bc2f793ef37ebe64969abf9884387a6a5aa19fb4d021a8dc3b407359cf76f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_231bc2f793ef37ebe64969abf9884387a6a5aa19fb4d021a8dc3b407359cf76f->enter($__internal_231bc2f793ef37ebe64969abf9884387a6a5aa19fb4d021a8dc3b407359cf76f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_currency.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1dc987064178ffe72d1ebd90f3b927bdce49a491b5f79c1b424f206845cfccf8->leave($__internal_1dc987064178ffe72d1ebd90f3b927bdce49a491b5f79c1b424f206845cfccf8_prof);

        
        $__internal_231bc2f793ef37ebe64969abf9884387a6a5aa19fb4d021a8dc3b407359cf76f->leave($__internal_231bc2f793ef37ebe64969abf9884387a6a5aa19fb4d021a8dc3b407359cf76f_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_92290afba01bdd4b3925e148917e79420541b8943b7aa587402e3606b8d8a898 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92290afba01bdd4b3925e148917e79420541b8943b7aa587402e3606b8d8a898->enter($__internal_92290afba01bdd4b3925e148917e79420541b8943b7aa587402e3606b8d8a898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_3ca4ed315aa7250e236b5ad146fbf1d3e09f4dcff03f1155174ebd8543fc2cce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ca4ed315aa7250e236b5ad146fbf1d3e09f4dcff03f1155174ebd8543fc2cce->enter($__internal_3ca4ed315aa7250e236b5ad146fbf1d3e09f4dcff03f1155174ebd8543fc2cce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "
    ";
        }
        
        $__internal_3ca4ed315aa7250e236b5ad146fbf1d3e09f4dcff03f1155174ebd8543fc2cce->leave($__internal_3ca4ed315aa7250e236b5ad146fbf1d3e09f4dcff03f1155174ebd8543fc2cce_prof);

        
        $__internal_92290afba01bdd4b3925e148917e79420541b8943b7aa587402e3606b8d8a898->leave($__internal_92290afba01bdd4b3925e148917e79420541b8943b7aa587402e3606b8d8a898_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_currency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% if value is not null %}
        {{ field_description.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_currency.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_currency.html.twig");
    }
}
