<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_f7109ccb47106ff14de5ffaaa3ab7dd47fd13bcb437bf770a39e8577d899364b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:register.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_23411293ca8a46f467bd40285fa2f3a68f30a4ecaf59de4cee55743db525d90f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23411293ca8a46f467bd40285fa2f3a68f30a4ecaf59de4cee55743db525d90f->enter($__internal_23411293ca8a46f467bd40285fa2f3a68f30a4ecaf59de4cee55743db525d90f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $__internal_95ec420a76d817b68c17c3a53b5d6f1ef2f1420c98649bb14111233891ddbf38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95ec420a76d817b68c17c3a53b5d6f1ef2f1420c98649bb14111233891ddbf38->enter($__internal_95ec420a76d817b68c17c3a53b5d6f1ef2f1420c98649bb14111233891ddbf38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_23411293ca8a46f467bd40285fa2f3a68f30a4ecaf59de4cee55743db525d90f->leave($__internal_23411293ca8a46f467bd40285fa2f3a68f30a4ecaf59de4cee55743db525d90f_prof);

        
        $__internal_95ec420a76d817b68c17c3a53b5d6f1ef2f1420c98649bb14111233891ddbf38->leave($__internal_95ec420a76d817b68c17c3a53b5d6f1ef2f1420c98649bb14111233891ddbf38_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9d891fb0a2c09001c6dc9300481fe34d3be63faaf782f6289625a3bd0050f5ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d891fb0a2c09001c6dc9300481fe34d3be63faaf782f6289625a3bd0050f5ee->enter($__internal_9d891fb0a2c09001c6dc9300481fe34d3be63faaf782f6289625a3bd0050f5ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_413157887bb625c6e9059b91cfa190f8200e6bae0556cbcfe0a94a426e633cbc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_413157887bb625c6e9059b91cfa190f8200e6bae0556cbcfe0a94a426e633cbc->enter($__internal_413157887bb625c6e9059b91cfa190f8200e6bae0556cbcfe0a94a426e633cbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 4)->display($context);
        
        $__internal_413157887bb625c6e9059b91cfa190f8200e6bae0556cbcfe0a94a426e633cbc->leave($__internal_413157887bb625c6e9059b91cfa190f8200e6bae0556cbcfe0a94a426e633cbc_prof);

        
        $__internal_9d891fb0a2c09001c6dc9300481fe34d3be63faaf782f6289625a3bd0050f5ee->leave($__internal_9d891fb0a2c09001c6dc9300481fe34d3be63faaf782f6289625a3bd0050f5ee_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Registration/register_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:register.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/register.html.twig");
    }
}
