<?php

/* SonataAdminBundle:CRUD:edit_file.html.twig */
class __TwigTemplate_485833b06eaf147cd4c89713097d17b23dafd5f7fd71e5b5dfc6d4b1b24b7040 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_file.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b5bc318a9e25693fe48a2c814b71e0d5becfb7497fe2db14266612d5bad265e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b5bc318a9e25693fe48a2c814b71e0d5becfb7497fe2db14266612d5bad265e->enter($__internal_0b5bc318a9e25693fe48a2c814b71e0d5becfb7497fe2db14266612d5bad265e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_file.html.twig"));

        $__internal_55b84569e56593e909e3eb975db46d208aa8a74686925162a9596ac9f6fb6296 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55b84569e56593e909e3eb975db46d208aa8a74686925162a9596ac9f6fb6296->enter($__internal_55b84569e56593e909e3eb975db46d208aa8a74686925162a9596ac9f6fb6296_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_file.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b5bc318a9e25693fe48a2c814b71e0d5becfb7497fe2db14266612d5bad265e->leave($__internal_0b5bc318a9e25693fe48a2c814b71e0d5becfb7497fe2db14266612d5bad265e_prof);

        
        $__internal_55b84569e56593e909e3eb975db46d208aa8a74686925162a9596ac9f6fb6296->leave($__internal_55b84569e56593e909e3eb975db46d208aa8a74686925162a9596ac9f6fb6296_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_dccbbbdc54416169b7d1256025e68fe9155c69e03f5051f867642c86ef71d210 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dccbbbdc54416169b7d1256025e68fe9155c69e03f5051f867642c86ef71d210->enter($__internal_dccbbbdc54416169b7d1256025e68fe9155c69e03f5051f867642c86ef71d210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_c8191f96410617566e2296c0e5ac8bc0e5766efa25d92a5fbf6c11d350b124b5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8191f96410617566e2296c0e5ac8bc0e5766efa25d92a5fbf6c11d350b124b5->enter($__internal_c8191f96410617566e2296c0e5ac8bc0e5766efa25d92a5fbf6c11d350b124b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_c8191f96410617566e2296c0e5ac8bc0e5766efa25d92a5fbf6c11d350b124b5->leave($__internal_c8191f96410617566e2296c0e5ac8bc0e5766efa25d92a5fbf6c11d350b124b5_prof);

        
        $__internal_dccbbbdc54416169b7d1256025e68fe9155c69e03f5051f867642c86ef71d210->leave($__internal_dccbbbdc54416169b7d1256025e68fe9155c69e03f5051f867642c86ef71d210_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_file.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_file.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_file.html.twig");
    }
}
