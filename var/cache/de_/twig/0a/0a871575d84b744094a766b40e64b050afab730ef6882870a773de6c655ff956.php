<?php

/* SonataAdminBundle:CRUD:list_boolean.html.twig */
class __TwigTemplate_0ba496c61afd06acfe264b1a23c4c9c0dcd9a12b92a24aa9e640eed9080619dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field_span_attributes' => array($this, 'block_field_span_attributes'),
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_boolean.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_35aff9e8cc41bc737c49f8b28434d63bdaed5a3c38bf3fa17be8d1e73ac60b82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35aff9e8cc41bc737c49f8b28434d63bdaed5a3c38bf3fa17be8d1e73ac60b82->enter($__internal_35aff9e8cc41bc737c49f8b28434d63bdaed5a3c38bf3fa17be8d1e73ac60b82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_boolean.html.twig"));

        $__internal_4d65988e54e4db68cc334c3492233d8307f5a6ca5820236a103ca0299450831e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4d65988e54e4db68cc334c3492233d8307f5a6ca5820236a103ca0299450831e->enter($__internal_4d65988e54e4db68cc334c3492233d8307f5a6ca5820236a103ca0299450831e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_boolean.html.twig"));

        // line 14
        $context["isEditable"] = (($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "editable", array(), "any", true, true) && $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "editable", array())) && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"));
        // line 15
        $context["xEditableType"] = $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->getXEditableType($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "type", array()));
        // line 17
        if (((isset($context["isEditable"]) ? $context["isEditable"] : $this->getContext($context, "isEditable")) && (isset($context["xEditableType"]) ? $context["xEditableType"] : $this->getContext($context, "xEditableType")))) {
        }
        // line 12
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_35aff9e8cc41bc737c49f8b28434d63bdaed5a3c38bf3fa17be8d1e73ac60b82->leave($__internal_35aff9e8cc41bc737c49f8b28434d63bdaed5a3c38bf3fa17be8d1e73ac60b82_prof);

        
        $__internal_4d65988e54e4db68cc334c3492233d8307f5a6ca5820236a103ca0299450831e->leave($__internal_4d65988e54e4db68cc334c3492233d8307f5a6ca5820236a103ca0299450831e_prof);

    }

    // line 18
    public function block_field_span_attributes($context, array $blocks = array())
    {
        $__internal_0a59d951c69106f2ba42f77d6dfecf772059fdef556c18153b19dfe22f73b199 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a59d951c69106f2ba42f77d6dfecf772059fdef556c18153b19dfe22f73b199->enter($__internal_0a59d951c69106f2ba42f77d6dfecf772059fdef556c18153b19dfe22f73b199_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_span_attributes"));

        $__internal_69309392472368ce1f52993e37cce2d6ef4a45b6c410d9e9962acc6d2ba2b7fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69309392472368ce1f52993e37cce2d6ef4a45b6c410d9e9962acc6d2ba2b7fd->enter($__internal_69309392472368ce1f52993e37cce2d6ef4a45b6c410d9e9962acc6d2ba2b7fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_span_attributes"));

        // line 19
        echo "        ";
        ob_start();
        // line 20
        echo "            ";
        $this->displayParentBlock("field_span_attributes", $context, $blocks);
        echo "
            data-source=\"[{value: 0, text: '";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_type_no", array(), "SonataAdminBundle");
        echo "'},{value: 1, text: '";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->trans("label_type_yes", array(), "SonataAdminBundle");
        echo "'}]\"
        ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 23
        echo "    ";
        
        $__internal_69309392472368ce1f52993e37cce2d6ef4a45b6c410d9e9962acc6d2ba2b7fd->leave($__internal_69309392472368ce1f52993e37cce2d6ef4a45b6c410d9e9962acc6d2ba2b7fd_prof);

        
        $__internal_0a59d951c69106f2ba42f77d6dfecf772059fdef556c18153b19dfe22f73b199->leave($__internal_0a59d951c69106f2ba42f77d6dfecf772059fdef556c18153b19dfe22f73b199_prof);

    }

    // line 26
    public function block_field($context, array $blocks = array())
    {
        $__internal_7e3bbab4c43f3596276b63166a34dcb9ae0e1e2c25e1e7216b987b9c8f580606 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7e3bbab4c43f3596276b63166a34dcb9ae0e1e2c25e1e7216b987b9c8f580606->enter($__internal_7e3bbab4c43f3596276b63166a34dcb9ae0e1e2c25e1e7216b987b9c8f580606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_6bbe11a81d9676d7e7fc6be908e56f98abf75582bcbaf304272fa9433ffb41ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bbe11a81d9676d7e7fc6be908e56f98abf75582bcbaf304272fa9433ffb41ad->enter($__internal_6bbe11a81d9676d7e7fc6be908e56f98abf75582bcbaf304272fa9433ffb41ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 27
        $this->loadTemplate("SonataAdminBundle:CRUD:display_boolean.html.twig", "SonataAdminBundle:CRUD:list_boolean.html.twig", 27)->display($context);
        
        $__internal_6bbe11a81d9676d7e7fc6be908e56f98abf75582bcbaf304272fa9433ffb41ad->leave($__internal_6bbe11a81d9676d7e7fc6be908e56f98abf75582bcbaf304272fa9433ffb41ad_prof);

        
        $__internal_7e3bbab4c43f3596276b63166a34dcb9ae0e1e2c25e1e7216b987b9c8f580606->leave($__internal_7e3bbab4c43f3596276b63166a34dcb9ae0e1e2c25e1e7216b987b9c8f580606_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_boolean.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 27,  82 => 26,  72 => 23,  65 => 21,  60 => 20,  57 => 19,  48 => 18,  38 => 12,  35 => 17,  33 => 15,  31 => 14,  19 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% set isEditable = field_description.options.editable is defined and field_description.options.editable and admin.hasAccess('edit', object) %}
{% set xEditableType = field_description.type|sonata_xeditable_type %}

{% if isEditable and xEditableType %}
    {% block field_span_attributes %}
        {% spaceless %}
            {{ parent() }}
            data-source=\"[{value: 0, text: '{%- trans from 'SonataAdminBundle' %}label_type_no{% endtrans -%}'},{value: 1, text: '{%- trans from 'SonataAdminBundle' %}label_type_yes{% endtrans -%}'}]\"
        {% endspaceless %}
    {% endblock %}
{% endif %}

{% block field %}
    {%- include 'SonataAdminBundle:CRUD:display_boolean.html.twig' -%}
{% endblock %}
", "SonataAdminBundle:CRUD:list_boolean.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_boolean.html.twig");
    }
}
