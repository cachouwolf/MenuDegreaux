<?php

/* @Framework/Form/choice_attributes.html.php */
class __TwigTemplate_f8c7d8729da5656fd6677c4fc95a9ffed27483ef1e9f997a9ff993c12ac73673 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68b2858e1d082870e198e012022cd846ebad5fc0d4444d700e68d6f2c4d68cb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68b2858e1d082870e198e012022cd846ebad5fc0d4444d700e68d6f2c4d68cb5->enter($__internal_68b2858e1d082870e198e012022cd846ebad5fc0d4444d700e68d6f2c4d68cb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        $__internal_ad63a2ac2c2e4624bb3fd97a30a606bb2190a6c9d0561452649e953a5e3978bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ad63a2ac2c2e4624bb3fd97a30a606bb2190a6c9d0561452649e953a5e3978bf->enter($__internal_ad63a2ac2c2e4624bb3fd97a30a606bb2190a6c9d0561452649e953a5e3978bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_attributes.html.php"));

        // line 1
        echo "<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
";
        
        $__internal_68b2858e1d082870e198e012022cd846ebad5fc0d4444d700e68d6f2c4d68cb5->leave($__internal_68b2858e1d082870e198e012022cd846ebad5fc0d4444d700e68d6f2c4d68cb5_prof);

        
        $__internal_ad63a2ac2c2e4624bb3fd97a30a606bb2190a6c9d0561452649e953a5e3978bf->leave($__internal_ad63a2ac2c2e4624bb3fd97a30a606bb2190a6c9d0561452649e953a5e3978bf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$disabled): ?>disabled=\"disabled\" <?php endif ?>
<?php foreach (\$choice_attr as \$k => \$v): ?>
<?php if (\$v === true): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$k)) ?>
<?php elseif (\$v !== false): ?>
<?php printf('%s=\"%s\" ', \$view->escape(\$k), \$view->escape(\$v)) ?>
<?php endif ?>
<?php endforeach ?>
", "@Framework/Form/choice_attributes.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_attributes.html.php");
    }
}
