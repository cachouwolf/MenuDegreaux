<?php

/* SonataBlockBundle:Block:block_container.html.twig */
class __TwigTemplate_a2727729355b909bce9ebd8ea2e6d19da4329c082a7f072ee9207055acec333f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block_class' => array($this, 'block_block_class'),
            'block_role' => array($this, 'block_block_role'),
            'block' => array($this, 'block_block'),
            'block_child_render' => array($this, 'block_block_child_render'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_container.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee44df07d235ec7ddb777c513ec55f0901647c0e6c7860c79d1c33b7946162e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee44df07d235ec7ddb777c513ec55f0901647c0e6c7860c79d1c33b7946162e8->enter($__internal_ee44df07d235ec7ddb777c513ec55f0901647c0e6c7860c79d1c33b7946162e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_container.html.twig"));

        $__internal_ed726c858b7840a9eeabe30ae18fded0163abd3a7404d82a827cdaef55517eb0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed726c858b7840a9eeabe30ae18fded0163abd3a7404d82a827cdaef55517eb0->enter($__internal_ed726c858b7840a9eeabe30ae18fded0163abd3a7404d82a827cdaef55517eb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_container.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee44df07d235ec7ddb777c513ec55f0901647c0e6c7860c79d1c33b7946162e8->leave($__internal_ee44df07d235ec7ddb777c513ec55f0901647c0e6c7860c79d1c33b7946162e8_prof);

        
        $__internal_ed726c858b7840a9eeabe30ae18fded0163abd3a7404d82a827cdaef55517eb0->leave($__internal_ed726c858b7840a9eeabe30ae18fded0163abd3a7404d82a827cdaef55517eb0_prof);

    }

    // line 15
    public function block_block_class($context, array $blocks = array())
    {
        $__internal_3c32acfe79fd8f7c7c4ecd59720a128c7305ef1ccccc82b8e171fdedbf97c896 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c32acfe79fd8f7c7c4ecd59720a128c7305ef1ccccc82b8e171fdedbf97c896->enter($__internal_3c32acfe79fd8f7c7c4ecd59720a128c7305ef1ccccc82b8e171fdedbf97c896_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        $__internal_88525cfb49c2b17926273a6ae38974d44437c628d4790022334abdddc08acb93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88525cfb49c2b17926273a6ae38974d44437c628d4790022334abdddc08acb93->enter($__internal_88525cfb49c2b17926273a6ae38974d44437c628d4790022334abdddc08acb93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        echo " cms-container";
        if ( !$this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "hasParent", array(), "method")) {
            echo " cms-container-root";
        }
        if ($this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "class", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "class", array()), "html", null, true);
        }
        
        $__internal_88525cfb49c2b17926273a6ae38974d44437c628d4790022334abdddc08acb93->leave($__internal_88525cfb49c2b17926273a6ae38974d44437c628d4790022334abdddc08acb93_prof);

        
        $__internal_3c32acfe79fd8f7c7c4ecd59720a128c7305ef1ccccc82b8e171fdedbf97c896->leave($__internal_3c32acfe79fd8f7c7c4ecd59720a128c7305ef1ccccc82b8e171fdedbf97c896_prof);

    }

    // line 18
    public function block_block_role($context, array $blocks = array())
    {
        $__internal_86c07855c5d7966297c835ef64e6e057712aa20ec3daba7df5beb5919bdaa28c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86c07855c5d7966297c835ef64e6e057712aa20ec3daba7df5beb5919bdaa28c->enter($__internal_86c07855c5d7966297c835ef64e6e057712aa20ec3daba7df5beb5919bdaa28c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        $__internal_d5fd2e34be9c3535c887b66d03f675d636beba9d8f694de44d553232a3c1dd0d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5fd2e34be9c3535c887b66d03f675d636beba9d8f694de44d553232a3c1dd0d->enter($__internal_d5fd2e34be9c3535c887b66d03f675d636beba9d8f694de44d553232a3c1dd0d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        echo "container";
        
        $__internal_d5fd2e34be9c3535c887b66d03f675d636beba9d8f694de44d553232a3c1dd0d->leave($__internal_d5fd2e34be9c3535c887b66d03f675d636beba9d8f694de44d553232a3c1dd0d_prof);

        
        $__internal_86c07855c5d7966297c835ef64e6e057712aa20ec3daba7df5beb5919bdaa28c->leave($__internal_86c07855c5d7966297c835ef64e6e057712aa20ec3daba7df5beb5919bdaa28c_prof);

    }

    // line 21
    public function block_block($context, array $blocks = array())
    {
        $__internal_2575a0c2a7676f5da701ed37f8b8f419903d7c91f98b9cc7f77faf627993a9f0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2575a0c2a7676f5da701ed37f8b8f419903d7c91f98b9cc7f77faf627993a9f0->enter($__internal_2575a0c2a7676f5da701ed37f8b8f419903d7c91f98b9cc7f77faf627993a9f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_526893721e0d37be18dabac330a1cd07dac20ce2f5a538825a14d30bc22b7894 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_526893721e0d37be18dabac330a1cd07dac20ce2f5a538825a14d30bc22b7894->enter($__internal_526893721e0d37be18dabac330a1cd07dac20ce2f5a538825a14d30bc22b7894_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 22
        echo "    ";
        if ((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator"))) {
            echo $this->getAttribute((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator")), "pre", array());
        }
        // line 23
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "children", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 24
            echo "        ";
            $this->displayBlock('block_child_render', $context, $blocks);
            // line 27
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "    ";
        if ((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator"))) {
            echo $this->getAttribute((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator")), "post", array());
        }
        
        $__internal_526893721e0d37be18dabac330a1cd07dac20ce2f5a538825a14d30bc22b7894->leave($__internal_526893721e0d37be18dabac330a1cd07dac20ce2f5a538825a14d30bc22b7894_prof);

        
        $__internal_2575a0c2a7676f5da701ed37f8b8f419903d7c91f98b9cc7f77faf627993a9f0->leave($__internal_2575a0c2a7676f5da701ed37f8b8f419903d7c91f98b9cc7f77faf627993a9f0_prof);

    }

    // line 24
    public function block_block_child_render($context, array $blocks = array())
    {
        $__internal_66ea87b8b1a428798226d0791202885db6ce7c25df3fb3251293552cd4789f5b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66ea87b8b1a428798226d0791202885db6ce7c25df3fb3251293552cd4789f5b->enter($__internal_66ea87b8b1a428798226d0791202885db6ce7c25df3fb3251293552cd4789f5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        $__internal_7b33c1edd84322df24161619a710284fd52a2f5d85a355a61765d7a8013dfb4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b33c1edd84322df24161619a710284fd52a2f5d85a355a61765d7a8013dfb4b->enter($__internal_7b33c1edd84322df24161619a710284fd52a2f5d85a355a61765d7a8013dfb4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        // line 25
        echo "            ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child"))));
        echo "
        ";
        
        $__internal_7b33c1edd84322df24161619a710284fd52a2f5d85a355a61765d7a8013dfb4b->leave($__internal_7b33c1edd84322df24161619a710284fd52a2f5d85a355a61765d7a8013dfb4b_prof);

        
        $__internal_66ea87b8b1a428798226d0791202885db6ce7c25df3fb3251293552cd4789f5b->leave($__internal_66ea87b8b1a428798226d0791202885db6ce7c25df3fb3251293552cd4789f5b_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_container.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 25,  147 => 24,  134 => 28,  120 => 27,  117 => 24,  99 => 23,  94 => 22,  85 => 21,  67 => 18,  42 => 15,  21 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{# block classes are prepended with a container class #}
{% block block_class %} cms-container{% if not block.hasParent() %} cms-container-root{%endif%}{% if settings.class %} {{ settings.class }}{% endif %}{% endblock %}

{# identify a block role used by the page editor #}
{% block block_role %}container{% endblock %}

{# render container block #}
{% block block %}
    {% if decorator %}{{ decorator.pre|raw }}{% endif %}
    {% for child in block.children %}
        {% block block_child_render %}
            {{ sonata_block_render(child) }}
        {% endblock %}
    {% endfor %}
    {% if decorator %}{{ decorator.post|raw }}{% endif %}
{% endblock %}
", "SonataBlockBundle:Block:block_container.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_container.html.twig");
    }
}
