<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_2768a68810e3d669c4b445289b4959bceb4d00386e75f4fe298efe55f5ea3754 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_47b85272771dc21ece1fbf91b8d29eeec8615e1145f267a6dc17e6a6b179c08f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47b85272771dc21ece1fbf91b8d29eeec8615e1145f267a6dc17e6a6b179c08f->enter($__internal_47b85272771dc21ece1fbf91b8d29eeec8615e1145f267a6dc17e6a6b179c08f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_06dc9a251e7a82535ca6e7a8665d397f01dbbb7658ec123cfdcbd93a94889bcb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06dc9a251e7a82535ca6e7a8665d397f01dbbb7658ec123cfdcbd93a94889bcb->enter($__internal_06dc9a251e7a82535ca6e7a8665d397f01dbbb7658ec123cfdcbd93a94889bcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('button_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('date_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('time_widget', $context, $blocks);
        // line 90
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 128
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 132
        echo "
";
        // line 133
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 152
        echo "
";
        // line 153
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 163
        echo "
";
        // line 164
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 174
        echo "
";
        // line 176
        echo "
";
        // line 177
        $this->displayBlock('form_label', $context, $blocks);
        // line 181
        echo "
";
        // line 182
        $this->displayBlock('choice_label', $context, $blocks);
        // line 187
        echo "
";
        // line 188
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 191
        echo "
";
        // line 192
        $this->displayBlock('radio_label', $context, $blocks);
        // line 195
        echo "
";
        // line 196
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 220
        echo "
";
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('form_row', $context, $blocks);
        // line 230
        echo "
";
        // line 231
        $this->displayBlock('button_row', $context, $blocks);
        // line 236
        echo "
";
        // line 237
        $this->displayBlock('choice_row', $context, $blocks);
        // line 241
        echo "
";
        // line 242
        $this->displayBlock('date_row', $context, $blocks);
        // line 246
        echo "
";
        // line 247
        $this->displayBlock('time_row', $context, $blocks);
        // line 251
        echo "
";
        // line 252
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 256
        echo "
";
        // line 257
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 263
        echo "
";
        // line 264
        $this->displayBlock('radio_row', $context, $blocks);
        // line 270
        echo "
";
        // line 272
        echo "
";
        // line 273
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_47b85272771dc21ece1fbf91b8d29eeec8615e1145f267a6dc17e6a6b179c08f->leave($__internal_47b85272771dc21ece1fbf91b8d29eeec8615e1145f267a6dc17e6a6b179c08f_prof);

        
        $__internal_06dc9a251e7a82535ca6e7a8665d397f01dbbb7658ec123cfdcbd93a94889bcb->leave($__internal_06dc9a251e7a82535ca6e7a8665d397f01dbbb7658ec123cfdcbd93a94889bcb_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_b3c1a5f71d0ff0c90c50b8c72a97cfac14be1e4121ff7b5942b228a75d8ff5fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3c1a5f71d0ff0c90c50b8c72a97cfac14be1e4121ff7b5942b228a75d8ff5fd->enter($__internal_b3c1a5f71d0ff0c90c50b8c72a97cfac14be1e4121ff7b5942b228a75d8ff5fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_1a607f884c0659869a8084b30e12b1d81828e86b276396e3488adf9203b231ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a607f884c0659869a8084b30e12b1d81828e86b276396e3488adf9203b231ef->enter($__internal_1a607f884c0659869a8084b30e12b1d81828e86b276396e3488adf9203b231ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter((isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_1a607f884c0659869a8084b30e12b1d81828e86b276396e3488adf9203b231ef->leave($__internal_1a607f884c0659869a8084b30e12b1d81828e86b276396e3488adf9203b231ef_prof);

        
        $__internal_b3c1a5f71d0ff0c90c50b8c72a97cfac14be1e4121ff7b5942b228a75d8ff5fd->leave($__internal_b3c1a5f71d0ff0c90c50b8c72a97cfac14be1e4121ff7b5942b228a75d8ff5fd_prof);

    }

    // line 12
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_f12c3d2c2404e8bcbe8f734c4d60a2121aed33b17ae4a336cbf5384dbef8f4a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f12c3d2c2404e8bcbe8f734c4d60a2121aed33b17ae4a336cbf5384dbef8f4a8->enter($__internal_f12c3d2c2404e8bcbe8f734c4d60a2121aed33b17ae4a336cbf5384dbef8f4a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_a84b06b57ade05ab70e7d9d2b4fad0409d39a768088df86a8f6dce66709ba7e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a84b06b57ade05ab70e7d9d2b4fad0409d39a768088df86a8f6dce66709ba7e1->enter($__internal_a84b06b57ade05ab70e7d9d2b4fad0409d39a768088df86a8f6dce66709ba7e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 13
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 14
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_a84b06b57ade05ab70e7d9d2b4fad0409d39a768088df86a8f6dce66709ba7e1->leave($__internal_a84b06b57ade05ab70e7d9d2b4fad0409d39a768088df86a8f6dce66709ba7e1_prof);

        
        $__internal_f12c3d2c2404e8bcbe8f734c4d60a2121aed33b17ae4a336cbf5384dbef8f4a8->leave($__internal_f12c3d2c2404e8bcbe8f734c4d60a2121aed33b17ae4a336cbf5384dbef8f4a8_prof);

    }

    // line 17
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_871e8409da5f906c8d48c2cfb332172d95e58c7f31e8c8d447f72b5152a6faf9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_871e8409da5f906c8d48c2cfb332172d95e58c7f31e8c8d447f72b5152a6faf9->enter($__internal_871e8409da5f906c8d48c2cfb332172d95e58c7f31e8c8d447f72b5152a6faf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_d70f6caeb8584dd75e6fbbb41b7718e8ec3387ad6f3490c2f33420e4a94ac04d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d70f6caeb8584dd75e6fbbb41b7718e8ec3387ad6f3490c2f33420e4a94ac04d->enter($__internal_d70f6caeb8584dd75e6fbbb41b7718e8ec3387ad6f3490c2f33420e4a94ac04d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 18
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 19
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_d70f6caeb8584dd75e6fbbb41b7718e8ec3387ad6f3490c2f33420e4a94ac04d->leave($__internal_d70f6caeb8584dd75e6fbbb41b7718e8ec3387ad6f3490c2f33420e4a94ac04d_prof);

        
        $__internal_871e8409da5f906c8d48c2cfb332172d95e58c7f31e8c8d447f72b5152a6faf9->leave($__internal_871e8409da5f906c8d48c2cfb332172d95e58c7f31e8c8d447f72b5152a6faf9_prof);

    }

    // line 22
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_16b5c6f9fd459d34ce9f4b64344d02119ce0717fe0e84e3c5b5f68c0d7885e43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16b5c6f9fd459d34ce9f4b64344d02119ce0717fe0e84e3c5b5f68c0d7885e43->enter($__internal_16b5c6f9fd459d34ce9f4b64344d02119ce0717fe0e84e3c5b5f68c0d7885e43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_a004e9213fce0a10f4a097fab088fee778fd2007a7627ec0956f83561d2eaf8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a004e9213fce0a10f4a097fab088fee778fd2007a7627ec0956f83561d2eaf8a->enter($__internal_a004e9213fce0a10f4a097fab088fee778fd2007a7627ec0956f83561d2eaf8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 23
        echo "<div class=\"input-group\">
        ";
        // line 24
        $context["append"] = (is_string($__internal_429205282a31bc63d3d09480c2fd119f300ec44337d373e9b4123d3ddf87d44d = (isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern"))) && is_string($__internal_5e7c530fdae6234d47aa22b444a94236803804f89759c7cc1e625c2ffcf443ac = "{{") && ('' === $__internal_5e7c530fdae6234d47aa22b444a94236803804f89759c7cc1e625c2ffcf443ac || 0 === strpos($__internal_429205282a31bc63d3d09480c2fd119f300ec44337d373e9b4123d3ddf87d44d, $__internal_5e7c530fdae6234d47aa22b444a94236803804f89759c7cc1e625c2ffcf443ac)));
        // line 25
        echo "        ";
        if ( !(isset($context["append"]) ? $context["append"] : $this->getContext($context, "append"))) {
            // line 26
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 28
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 29
        if ((isset($context["append"]) ? $context["append"] : $this->getContext($context, "append"))) {
            // line 30
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter((isset($context["money_pattern"]) ? $context["money_pattern"] : $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 32
        echo "    </div>";
        
        $__internal_a004e9213fce0a10f4a097fab088fee778fd2007a7627ec0956f83561d2eaf8a->leave($__internal_a004e9213fce0a10f4a097fab088fee778fd2007a7627ec0956f83561d2eaf8a_prof);

        
        $__internal_16b5c6f9fd459d34ce9f4b64344d02119ce0717fe0e84e3c5b5f68c0d7885e43->leave($__internal_16b5c6f9fd459d34ce9f4b64344d02119ce0717fe0e84e3c5b5f68c0d7885e43_prof);

    }

    // line 35
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_209aec36dbe3aaebcc7eb14db49ad1814645ff461dd155c6a781b157f37f5a70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_209aec36dbe3aaebcc7eb14db49ad1814645ff461dd155c6a781b157f37f5a70->enter($__internal_209aec36dbe3aaebcc7eb14db49ad1814645ff461dd155c6a781b157f37f5a70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_038dba69b3b9c7d91573229a1372626f98cbba822af1fae992567a85313cf828 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_038dba69b3b9c7d91573229a1372626f98cbba822af1fae992567a85313cf828->enter($__internal_038dba69b3b9c7d91573229a1372626f98cbba822af1fae992567a85313cf828_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 36
        echo "<div class=\"input-group\">";
        // line 37
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 38
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_038dba69b3b9c7d91573229a1372626f98cbba822af1fae992567a85313cf828->leave($__internal_038dba69b3b9c7d91573229a1372626f98cbba822af1fae992567a85313cf828_prof);

        
        $__internal_209aec36dbe3aaebcc7eb14db49ad1814645ff461dd155c6a781b157f37f5a70->leave($__internal_209aec36dbe3aaebcc7eb14db49ad1814645ff461dd155c6a781b157f37f5a70_prof);

    }

    // line 42
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_b63574d0ae343d6856284a187416b3486984d61400c691747b7df7a39ca365b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b63574d0ae343d6856284a187416b3486984d61400c691747b7df7a39ca365b0->enter($__internal_b63574d0ae343d6856284a187416b3486984d61400c691747b7df7a39ca365b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_a4caae319c40e85887673e840a9c38d1e7849b45b4f02ee046ca0e53d6280f6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4caae319c40e85887673e840a9c38d1e7849b45b4f02ee046ca0e53d6280f6c->enter($__internal_a4caae319c40e85887673e840a9c38d1e7849b45b4f02ee046ca0e53d6280f6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 43
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 44
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 46
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 47
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 48
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'errors');
            // line 49
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'errors');
            // line 50
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            // line 51
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            // line 52
            echo "</div>";
        }
        
        $__internal_a4caae319c40e85887673e840a9c38d1e7849b45b4f02ee046ca0e53d6280f6c->leave($__internal_a4caae319c40e85887673e840a9c38d1e7849b45b4f02ee046ca0e53d6280f6c_prof);

        
        $__internal_b63574d0ae343d6856284a187416b3486984d61400c691747b7df7a39ca365b0->leave($__internal_b63574d0ae343d6856284a187416b3486984d61400c691747b7df7a39ca365b0_prof);

    }

    // line 56
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_17d6a5b513324db11649d987872f41db880eee506cb5eca218c7731f5bab4daf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_17d6a5b513324db11649d987872f41db880eee506cb5eca218c7731f5bab4daf->enter($__internal_17d6a5b513324db11649d987872f41db880eee506cb5eca218c7731f5bab4daf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_4c5461c1213fab94f2dd388b07470e5ada8c9b95ce49398a3395dc3dc585cffc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c5461c1213fab94f2dd388b07470e5ada8c9b95ce49398a3395dc3dc585cffc->enter($__internal_4c5461c1213fab94f2dd388b07470e5ada8c9b95ce49398a3395dc3dc585cffc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 57
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 58
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 60
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 61
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 62
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 64
            echo twig_replace_filter((isset($context["date_pattern"]) ? $context["date_pattern"] : $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 65
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 66
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 67
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 69
            if (( !array_key_exists("datetime", $context) ||  !(isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime")))) {
                // line 70
                echo "</div>";
            }
        }
        
        $__internal_4c5461c1213fab94f2dd388b07470e5ada8c9b95ce49398a3395dc3dc585cffc->leave($__internal_4c5461c1213fab94f2dd388b07470e5ada8c9b95ce49398a3395dc3dc585cffc_prof);

        
        $__internal_17d6a5b513324db11649d987872f41db880eee506cb5eca218c7731f5bab4daf->leave($__internal_17d6a5b513324db11649d987872f41db880eee506cb5eca218c7731f5bab4daf_prof);

    }

    // line 75
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_43a3ecce3e49ab62351af059769996f5c716f9eddea233b694b1dceea69eb64f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43a3ecce3e49ab62351af059769996f5c716f9eddea233b694b1dceea69eb64f->enter($__internal_43a3ecce3e49ab62351af059769996f5c716f9eddea233b694b1dceea69eb64f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_9140a04d9dc7e5b38bf9a20291061f1c77e6886b7b1c2e058e9f23e169bb5b8a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9140a04d9dc7e5b38bf9a20291061f1c77e6886b7b1c2e058e9f23e169bb5b8a->enter($__internal_9140a04d9dc7e5b38bf9a20291061f1c77e6886b7b1c2e058e9f23e169bb5b8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 76
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 77
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 79
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 80
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 81
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 83
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hour", array()), 'widget');
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minute", array()), 'widget');
            }
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "second", array()), 'widget');
            }
            // line 84
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == (isset($context["datetime"]) ? $context["datetime"] : $this->getContext($context, "datetime"))))) {
                // line 85
                echo "</div>";
            }
        }
        
        $__internal_9140a04d9dc7e5b38bf9a20291061f1c77e6886b7b1c2e058e9f23e169bb5b8a->leave($__internal_9140a04d9dc7e5b38bf9a20291061f1c77e6886b7b1c2e058e9f23e169bb5b8a_prof);

        
        $__internal_43a3ecce3e49ab62351af059769996f5c716f9eddea233b694b1dceea69eb64f->leave($__internal_43a3ecce3e49ab62351af059769996f5c716f9eddea233b694b1dceea69eb64f_prof);

    }

    // line 90
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_776b09f6df0955599342daa6cd1427f5a3e022f675c12b926c67243128d4679a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_776b09f6df0955599342daa6cd1427f5a3e022f675c12b926c67243128d4679a->enter($__internal_776b09f6df0955599342daa6cd1427f5a3e022f675c12b926c67243128d4679a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_8974e6948dab566d169fde51c6be13d8dc8bf29b47c9f62f64d01caf3d4ffab6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8974e6948dab566d169fde51c6be13d8dc8bf29b47c9f62f64d01caf3d4ffab6->enter($__internal_8974e6948dab566d169fde51c6be13d8dc8bf29b47c9f62f64d01caf3d4ffab6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 91
        if (((isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 95
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 96
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
            // line 97
            echo "<div class=\"table-responsive\">
                <table class=\"table ";
            // line 98
            echo twig_escape_filter($this->env, ((array_key_exists("table_class", $context)) ? (_twig_default_filter((isset($context["table_class"]) ? $context["table_class"] : $this->getContext($context, "table_class")), "table-bordered table-condensed table-striped")) : ("table-bordered table-condensed table-striped")), "html", null, true);
            echo "\">
                    <thead>
                    <tr>";
            // line 101
            if ((isset($context["with_years"]) ? $context["with_years"] : $this->getContext($context, "with_years"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "years", array()), 'label');
                echo "</th>";
            }
            // line 102
            if ((isset($context["with_months"]) ? $context["with_months"] : $this->getContext($context, "with_months"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "months", array()), 'label');
                echo "</th>";
            }
            // line 103
            if ((isset($context["with_weeks"]) ? $context["with_weeks"] : $this->getContext($context, "with_weeks"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "weeks", array()), 'label');
                echo "</th>";
            }
            // line 104
            if ((isset($context["with_days"]) ? $context["with_days"] : $this->getContext($context, "with_days"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "days", array()), 'label');
                echo "</th>";
            }
            // line 105
            if ((isset($context["with_hours"]) ? $context["with_hours"] : $this->getContext($context, "with_hours"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hours", array()), 'label');
                echo "</th>";
            }
            // line 106
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minutes", array()), 'label');
                echo "</th>";
            }
            // line 107
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo "<th>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "seconds", array()), 'label');
                echo "</th>";
            }
            // line 108
            echo "</tr>
                    </thead>
                    <tbody>
                    <tr>";
            // line 112
            if ((isset($context["with_years"]) ? $context["with_years"] : $this->getContext($context, "with_years"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "years", array()), 'widget');
                echo "</td>";
            }
            // line 113
            if ((isset($context["with_months"]) ? $context["with_months"] : $this->getContext($context, "with_months"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "months", array()), 'widget');
                echo "</td>";
            }
            // line 114
            if ((isset($context["with_weeks"]) ? $context["with_weeks"] : $this->getContext($context, "with_weeks"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "weeks", array()), 'widget');
                echo "</td>";
            }
            // line 115
            if ((isset($context["with_days"]) ? $context["with_days"] : $this->getContext($context, "with_days"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "days", array()), 'widget');
                echo "</td>";
            }
            // line 116
            if ((isset($context["with_hours"]) ? $context["with_hours"] : $this->getContext($context, "with_hours"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "hours", array()), 'widget');
                echo "</td>";
            }
            // line 117
            if ((isset($context["with_minutes"]) ? $context["with_minutes"] : $this->getContext($context, "with_minutes"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "minutes", array()), 'widget');
                echo "</td>";
            }
            // line 118
            if ((isset($context["with_seconds"]) ? $context["with_seconds"] : $this->getContext($context, "with_seconds"))) {
                echo "<td>";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "seconds", array()), 'widget');
                echo "</td>";
            }
            // line 119
            echo "</tr>
                    </tbody>
                </table>
            </div>";
            // line 123
            if ((isset($context["with_invert"]) ? $context["with_invert"] : $this->getContext($context, "with_invert"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "invert", array()), 'widget');
            }
            // line 124
            echo "</div>";
        }
        
        $__internal_8974e6948dab566d169fde51c6be13d8dc8bf29b47c9f62f64d01caf3d4ffab6->leave($__internal_8974e6948dab566d169fde51c6be13d8dc8bf29b47c9f62f64d01caf3d4ffab6_prof);

        
        $__internal_776b09f6df0955599342daa6cd1427f5a3e022f675c12b926c67243128d4679a->leave($__internal_776b09f6df0955599342daa6cd1427f5a3e022f675c12b926c67243128d4679a_prof);

    }

    // line 128
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_1fc3d069dba333b2434f452d730358975a70a1bacdf449519aaed7a6925bc5bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fc3d069dba333b2434f452d730358975a70a1bacdf449519aaed7a6925bc5bc->enter($__internal_1fc3d069dba333b2434f452d730358975a70a1bacdf449519aaed7a6925bc5bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_8c110746130f2ef48205dda85d63a3ec7d51fd4c7d99522257488ff380a3c5a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c110746130f2ef48205dda85d63a3ec7d51fd4c7d99522257488ff380a3c5a8->enter($__internal_8c110746130f2ef48205dda85d63a3ec7d51fd4c7d99522257488ff380a3c5a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 129
        $context["attr"] = twig_array_merge((isset($context["attr"]) ? $context["attr"] : $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["attr"]) ? $context["attr"] : null), "class", array()), "")) : ("")) . " form-control"))));
        // line 130
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        
        $__internal_8c110746130f2ef48205dda85d63a3ec7d51fd4c7d99522257488ff380a3c5a8->leave($__internal_8c110746130f2ef48205dda85d63a3ec7d51fd4c7d99522257488ff380a3c5a8_prof);

        
        $__internal_1fc3d069dba333b2434f452d730358975a70a1bacdf449519aaed7a6925bc5bc->leave($__internal_1fc3d069dba333b2434f452d730358975a70a1bacdf449519aaed7a6925bc5bc_prof);

    }

    // line 133
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_a432083a7f0d45b5846e4b26b2a4f0a9def57d567ecd757ff92fd2e4e91bb0e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a432083a7f0d45b5846e4b26b2a4f0a9def57d567ecd757ff92fd2e4e91bb0e5->enter($__internal_a432083a7f0d45b5846e4b26b2a4f0a9def57d567ecd757ff92fd2e4e91bb0e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_8b8ae20159eae4fadd73746f6558616e3d35fb7e3b020610971486712f12968b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b8ae20159eae4fadd73746f6558616e3d35fb7e3b020610971486712f12968b->enter($__internal_8b8ae20159eae4fadd73746f6558616e3d35fb7e3b020610971486712f12968b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 134
        if (twig_in_filter("-inline", (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) {
            // line 135
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 136
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 137
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 138
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 142
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 143
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 144
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 145
(isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 146
(isset($context["choice_translation_domain"]) ? $context["choice_translation_domain"] : $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 149
            echo "</div>";
        }
        
        $__internal_8b8ae20159eae4fadd73746f6558616e3d35fb7e3b020610971486712f12968b->leave($__internal_8b8ae20159eae4fadd73746f6558616e3d35fb7e3b020610971486712f12968b_prof);

        
        $__internal_a432083a7f0d45b5846e4b26b2a4f0a9def57d567ecd757ff92fd2e4e91bb0e5->leave($__internal_a432083a7f0d45b5846e4b26b2a4f0a9def57d567ecd757ff92fd2e4e91bb0e5_prof);

    }

    // line 153
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_af897d1365f7c020a0e8104180ca8b7297d67c5b2375a96f6fbe59a18be2e376 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af897d1365f7c020a0e8104180ca8b7297d67c5b2375a96f6fbe59a18be2e376->enter($__internal_af897d1365f7c020a0e8104180ca8b7297d67c5b2375a96f6fbe59a18be2e376_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_f2921087d4e9ed05fc4d5c410e4acf8586ea698625c3b5d11186e1780e5320cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f2921087d4e9ed05fc4d5c410e4acf8586ea698625c3b5d11186e1780e5320cd->enter($__internal_f2921087d4e9ed05fc4d5c410e4acf8586ea698625c3b5d11186e1780e5320cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 154
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 155
        if (twig_in_filter("checkbox-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 156
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 158
            echo "<div class=\"checkbox\">";
            // line 159
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 160
            echo "</div>";
        }
        
        $__internal_f2921087d4e9ed05fc4d5c410e4acf8586ea698625c3b5d11186e1780e5320cd->leave($__internal_f2921087d4e9ed05fc4d5c410e4acf8586ea698625c3b5d11186e1780e5320cd_prof);

        
        $__internal_af897d1365f7c020a0e8104180ca8b7297d67c5b2375a96f6fbe59a18be2e376->leave($__internal_af897d1365f7c020a0e8104180ca8b7297d67c5b2375a96f6fbe59a18be2e376_prof);

    }

    // line 164
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_e712404c6edc0098ef6100f67dcf934d5018168f8891d5ae04555cff32446630 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e712404c6edc0098ef6100f67dcf934d5018168f8891d5ae04555cff32446630->enter($__internal_e712404c6edc0098ef6100f67dcf934d5018168f8891d5ae04555cff32446630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_3ccd8952135886a1a0eaaa142e78f89b3bc07da1f542261d03ea9fe879f9d8f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ccd8952135886a1a0eaaa142e78f89b3bc07da1f542261d03ea9fe879f9d8f1->enter($__internal_3ccd8952135886a1a0eaaa142e78f89b3bc07da1f542261d03ea9fe879f9d8f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 165
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter((isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")), (($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")))) : ((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : (""))));
        // line 166
        if (twig_in_filter("radio-inline", (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class")))) {
            // line 167
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 169
            echo "<div class=\"radio\">";
            // line 170
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 171
            echo "</div>";
        }
        
        $__internal_3ccd8952135886a1a0eaaa142e78f89b3bc07da1f542261d03ea9fe879f9d8f1->leave($__internal_3ccd8952135886a1a0eaaa142e78f89b3bc07da1f542261d03ea9fe879f9d8f1_prof);

        
        $__internal_e712404c6edc0098ef6100f67dcf934d5018168f8891d5ae04555cff32446630->leave($__internal_e712404c6edc0098ef6100f67dcf934d5018168f8891d5ae04555cff32446630_prof);

    }

    // line 177
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_13e826bf519c14ae3936d2585c2c6e59a203824e3564fafa8f47d4c05372edca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13e826bf519c14ae3936d2585c2c6e59a203824e3564fafa8f47d4c05372edca->enter($__internal_13e826bf519c14ae3936d2585c2c6e59a203824e3564fafa8f47d4c05372edca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_eaceed78a27264a01a259bd31a0a1def04db6122657ff06a018cba1d059c86fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eaceed78a27264a01a259bd31a0a1def04db6122657ff06a018cba1d059c86fd->enter($__internal_eaceed78a27264a01a259bd31a0a1def04db6122657ff06a018cba1d059c86fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 178
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " control-label"))));
        // line 179
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_eaceed78a27264a01a259bd31a0a1def04db6122657ff06a018cba1d059c86fd->leave($__internal_eaceed78a27264a01a259bd31a0a1def04db6122657ff06a018cba1d059c86fd_prof);

        
        $__internal_13e826bf519c14ae3936d2585c2c6e59a203824e3564fafa8f47d4c05372edca->leave($__internal_13e826bf519c14ae3936d2585c2c6e59a203824e3564fafa8f47d4c05372edca_prof);

    }

    // line 182
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_55567ac4ff5e2ddc7b5f8b2943421502aa0d91ed51824717149112763c34b3a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55567ac4ff5e2ddc7b5f8b2943421502aa0d91ed51824717149112763c34b3a6->enter($__internal_55567ac4ff5e2ddc7b5f8b2943421502aa0d91ed51824717149112763c34b3a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_38e7ff1e8223662f43ee1155599e851f5065c2eb043b2ca3d1c1b25f5aedc672 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38e7ff1e8223662f43ee1155599e851f5065c2eb043b2ca3d1c1b25f5aedc672->enter($__internal_38e7ff1e8223662f43ee1155599e851f5065c2eb043b2ca3d1c1b25f5aedc672_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 184
        $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 185
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_38e7ff1e8223662f43ee1155599e851f5065c2eb043b2ca3d1c1b25f5aedc672->leave($__internal_38e7ff1e8223662f43ee1155599e851f5065c2eb043b2ca3d1c1b25f5aedc672_prof);

        
        $__internal_55567ac4ff5e2ddc7b5f8b2943421502aa0d91ed51824717149112763c34b3a6->leave($__internal_55567ac4ff5e2ddc7b5f8b2943421502aa0d91ed51824717149112763c34b3a6_prof);

    }

    // line 188
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_4665ced18227fa64f9fcf7fe1850080e723ed68abeb2aa8045e5b30f6ad4ab34 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4665ced18227fa64f9fcf7fe1850080e723ed68abeb2aa8045e5b30f6ad4ab34->enter($__internal_4665ced18227fa64f9fcf7fe1850080e723ed68abeb2aa8045e5b30f6ad4ab34_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_57cf7c2451f77725952c5b2adae8351f7f620f86ca10bb2b790b379f547f9ab7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57cf7c2451f77725952c5b2adae8351f7f620f86ca10bb2b790b379f547f9ab7->enter($__internal_57cf7c2451f77725952c5b2adae8351f7f620f86ca10bb2b790b379f547f9ab7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 189
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_57cf7c2451f77725952c5b2adae8351f7f620f86ca10bb2b790b379f547f9ab7->leave($__internal_57cf7c2451f77725952c5b2adae8351f7f620f86ca10bb2b790b379f547f9ab7_prof);

        
        $__internal_4665ced18227fa64f9fcf7fe1850080e723ed68abeb2aa8045e5b30f6ad4ab34->leave($__internal_4665ced18227fa64f9fcf7fe1850080e723ed68abeb2aa8045e5b30f6ad4ab34_prof);

    }

    // line 192
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_765b8889299d22ffe4119fd5bbef42a7c201a7cfd264881667494a8186a78c68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_765b8889299d22ffe4119fd5bbef42a7c201a7cfd264881667494a8186a78c68->enter($__internal_765b8889299d22ffe4119fd5bbef42a7c201a7cfd264881667494a8186a78c68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_81b1451291d41506857de6b40126c6ef4cfd0f73ce6e95c422ae024eeeb78a30 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81b1451291d41506857de6b40126c6ef4cfd0f73ce6e95c422ae024eeeb78a30->enter($__internal_81b1451291d41506857de6b40126c6ef4cfd0f73ce6e95c422ae024eeeb78a30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 193
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_81b1451291d41506857de6b40126c6ef4cfd0f73ce6e95c422ae024eeeb78a30->leave($__internal_81b1451291d41506857de6b40126c6ef4cfd0f73ce6e95c422ae024eeeb78a30_prof);

        
        $__internal_765b8889299d22ffe4119fd5bbef42a7c201a7cfd264881667494a8186a78c68->leave($__internal_765b8889299d22ffe4119fd5bbef42a7c201a7cfd264881667494a8186a78c68_prof);

    }

    // line 196
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_13fd42e0eb748d848ba86d4bab8c2427b9ca1b21d1c9176ab60f6227e6317814 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13fd42e0eb748d848ba86d4bab8c2427b9ca1b21d1c9176ab60f6227e6317814->enter($__internal_13fd42e0eb748d848ba86d4bab8c2427b9ca1b21d1c9176ab60f6227e6317814_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_32ae3adfdad823013f519f6499370e3660b444d537d1e938bcaedafc418104f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32ae3adfdad823013f519f6499370e3660b444d537d1e938bcaedafc418104f9->enter($__internal_32ae3adfdad823013f519f6499370e3660b444d537d1e938bcaedafc418104f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 197
        echo "    ";
        // line 198
        echo "    ";
        if (array_key_exists("widget", $context)) {
            // line 199
            echo "        ";
            if ((isset($context["required"]) ? $context["required"] : $this->getContext($context, "required"))) {
                // line 200
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " required"))));
                // line 201
                echo "        ";
            }
            // line 202
            echo "        ";
            if (array_key_exists("parent_label_class", $context)) {
                // line 203
                echo "            ";
                $context["label_attr"] = twig_array_merge((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["label_attr"]) ? $context["label_attr"] : null), "class", array()), "")) : ("")) . " ") . (isset($context["parent_label_class"]) ? $context["parent_label_class"] : $this->getContext($context, "parent_label_class"))))));
                // line 204
                echo "        ";
            }
            // line 205
            echo "        ";
            if (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false) && twig_test_empty((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))))) {
                // line 206
                if ( !twig_test_empty((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")))) {
                    // line 207
                    $context["label"] = twig_replace_filter((isset($context["label_format"]) ? $context["label_format"] : $this->getContext($context, "label_format")), array("%name%" =>                     // line 208
(isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")), "%id%" =>                     // line 209
(isset($context["id"]) ? $context["id"] : $this->getContext($context, "id"))));
                } else {
                    // line 212
                    $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize((isset($context["name"]) ? $context["name"] : $this->getContext($context, "name")));
                }
            }
            // line 215
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["label_attr"]) ? $context["label_attr"] : $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 216
            echo (isset($context["widget"]) ? $context["widget"] : $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")) === false)) ? (((((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) ? ((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), array(), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 217
            echo "</label>
    ";
        }
        
        $__internal_32ae3adfdad823013f519f6499370e3660b444d537d1e938bcaedafc418104f9->leave($__internal_32ae3adfdad823013f519f6499370e3660b444d537d1e938bcaedafc418104f9_prof);

        
        $__internal_13fd42e0eb748d848ba86d4bab8c2427b9ca1b21d1c9176ab60f6227e6317814->leave($__internal_13fd42e0eb748d848ba86d4bab8c2427b9ca1b21d1c9176ab60f6227e6317814_prof);

    }

    // line 223
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_9bb696bdf6b586f6a5a6da81f85bfc735e945beb097eb91c11be2781ccfb5f46 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bb696bdf6b586f6a5a6da81f85bfc735e945beb097eb91c11be2781ccfb5f46->enter($__internal_9bb696bdf6b586f6a5a6da81f85bfc735e945beb097eb91c11be2781ccfb5f46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_84ed6858ee644e36377d590b7c9aa5d27942755545bd2f8fbb9d6703cb58af48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84ed6858ee644e36377d590b7c9aa5d27942755545bd2f8fbb9d6703cb58af48->enter($__internal_84ed6858ee644e36377d590b7c9aa5d27942755545bd2f8fbb9d6703cb58af48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 224
        echo "<div class=\"form-group";
        if ((( !(isset($context["compound"]) ? $context["compound"] : $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter((isset($context["force_error"]) ? $context["force_error"] : $this->getContext($context, "force_error")), false)) : (false))) &&  !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 225
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'label');
        // line 226
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 227
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 228
        echo "</div>";
        
        $__internal_84ed6858ee644e36377d590b7c9aa5d27942755545bd2f8fbb9d6703cb58af48->leave($__internal_84ed6858ee644e36377d590b7c9aa5d27942755545bd2f8fbb9d6703cb58af48_prof);

        
        $__internal_9bb696bdf6b586f6a5a6da81f85bfc735e945beb097eb91c11be2781ccfb5f46->leave($__internal_9bb696bdf6b586f6a5a6da81f85bfc735e945beb097eb91c11be2781ccfb5f46_prof);

    }

    // line 231
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_5b3fe87bb73f5b971eb6743b82f0d8b48c8b392951fd33289c4a1c4034defd54 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b3fe87bb73f5b971eb6743b82f0d8b48c8b392951fd33289c4a1c4034defd54->enter($__internal_5b3fe87bb73f5b971eb6743b82f0d8b48c8b392951fd33289c4a1c4034defd54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_3fff364742bddba8b631618c5bea9a02f6317d640bec73d927f2c0ac1064b770 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fff364742bddba8b631618c5bea9a02f6317d640bec73d927f2c0ac1064b770->enter($__internal_3fff364742bddba8b631618c5bea9a02f6317d640bec73d927f2c0ac1064b770_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 232
        echo "<div class=\"form-group\">";
        // line 233
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 234
        echo "</div>";
        
        $__internal_3fff364742bddba8b631618c5bea9a02f6317d640bec73d927f2c0ac1064b770->leave($__internal_3fff364742bddba8b631618c5bea9a02f6317d640bec73d927f2c0ac1064b770_prof);

        
        $__internal_5b3fe87bb73f5b971eb6743b82f0d8b48c8b392951fd33289c4a1c4034defd54->leave($__internal_5b3fe87bb73f5b971eb6743b82f0d8b48c8b392951fd33289c4a1c4034defd54_prof);

    }

    // line 237
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_385637080ca6c0f7553d6f5fde44136ee3b971e6dbfd69e599ddaa80914526e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_385637080ca6c0f7553d6f5fde44136ee3b971e6dbfd69e599ddaa80914526e0->enter($__internal_385637080ca6c0f7553d6f5fde44136ee3b971e6dbfd69e599ddaa80914526e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_9a1852cdf65b5cdd442f920fa34f80a8ce7d2ccf04bae58747cf5d868a3bb70d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a1852cdf65b5cdd442f920fa34f80a8ce7d2ccf04bae58747cf5d868a3bb70d->enter($__internal_9a1852cdf65b5cdd442f920fa34f80a8ce7d2ccf04bae58747cf5d868a3bb70d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 238
        $context["force_error"] = true;
        // line 239
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_9a1852cdf65b5cdd442f920fa34f80a8ce7d2ccf04bae58747cf5d868a3bb70d->leave($__internal_9a1852cdf65b5cdd442f920fa34f80a8ce7d2ccf04bae58747cf5d868a3bb70d_prof);

        
        $__internal_385637080ca6c0f7553d6f5fde44136ee3b971e6dbfd69e599ddaa80914526e0->leave($__internal_385637080ca6c0f7553d6f5fde44136ee3b971e6dbfd69e599ddaa80914526e0_prof);

    }

    // line 242
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_3e2418b90adf23349e243246e7429f50ba06d2a0755f116702ed0542c3c67d20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e2418b90adf23349e243246e7429f50ba06d2a0755f116702ed0542c3c67d20->enter($__internal_3e2418b90adf23349e243246e7429f50ba06d2a0755f116702ed0542c3c67d20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_936875fb8305b333c875500ada6e52d1ab50472c525b52fd9bd9d4afa07d554b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_936875fb8305b333c875500ada6e52d1ab50472c525b52fd9bd9d4afa07d554b->enter($__internal_936875fb8305b333c875500ada6e52d1ab50472c525b52fd9bd9d4afa07d554b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 243
        $context["force_error"] = true;
        // line 244
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_936875fb8305b333c875500ada6e52d1ab50472c525b52fd9bd9d4afa07d554b->leave($__internal_936875fb8305b333c875500ada6e52d1ab50472c525b52fd9bd9d4afa07d554b_prof);

        
        $__internal_3e2418b90adf23349e243246e7429f50ba06d2a0755f116702ed0542c3c67d20->leave($__internal_3e2418b90adf23349e243246e7429f50ba06d2a0755f116702ed0542c3c67d20_prof);

    }

    // line 247
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_2f81bdc72089cdc1d8a028d2b15ff295c2c457b7d5717349ffd5601f6a5acc5f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f81bdc72089cdc1d8a028d2b15ff295c2c457b7d5717349ffd5601f6a5acc5f->enter($__internal_2f81bdc72089cdc1d8a028d2b15ff295c2c457b7d5717349ffd5601f6a5acc5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_b55fb17c0535d4caf846f60740d849fb6be026c6b53b758139558624a6f1e348 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b55fb17c0535d4caf846f60740d849fb6be026c6b53b758139558624a6f1e348->enter($__internal_b55fb17c0535d4caf846f60740d849fb6be026c6b53b758139558624a6f1e348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 248
        $context["force_error"] = true;
        // line 249
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_b55fb17c0535d4caf846f60740d849fb6be026c6b53b758139558624a6f1e348->leave($__internal_b55fb17c0535d4caf846f60740d849fb6be026c6b53b758139558624a6f1e348_prof);

        
        $__internal_2f81bdc72089cdc1d8a028d2b15ff295c2c457b7d5717349ffd5601f6a5acc5f->leave($__internal_2f81bdc72089cdc1d8a028d2b15ff295c2c457b7d5717349ffd5601f6a5acc5f_prof);

    }

    // line 252
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_3371a54a0d5ccd09577ae364f6d2652b5576b82a833a1cfb8d0b78da7d28b147 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3371a54a0d5ccd09577ae364f6d2652b5576b82a833a1cfb8d0b78da7d28b147->enter($__internal_3371a54a0d5ccd09577ae364f6d2652b5576b82a833a1cfb8d0b78da7d28b147_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_aabfad5c0c56c5f95c835080234de9796ef8b2c61146c4ad24406eb4886d8f62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aabfad5c0c56c5f95c835080234de9796ef8b2c61146c4ad24406eb4886d8f62->enter($__internal_aabfad5c0c56c5f95c835080234de9796ef8b2c61146c4ad24406eb4886d8f62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 253
        $context["force_error"] = true;
        // line 254
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_aabfad5c0c56c5f95c835080234de9796ef8b2c61146c4ad24406eb4886d8f62->leave($__internal_aabfad5c0c56c5f95c835080234de9796ef8b2c61146c4ad24406eb4886d8f62_prof);

        
        $__internal_3371a54a0d5ccd09577ae364f6d2652b5576b82a833a1cfb8d0b78da7d28b147->leave($__internal_3371a54a0d5ccd09577ae364f6d2652b5576b82a833a1cfb8d0b78da7d28b147_prof);

    }

    // line 257
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_85f186e309dd16ff5aed36b17814ae1933200b33afe80f449650407b30dec3a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85f186e309dd16ff5aed36b17814ae1933200b33afe80f449650407b30dec3a6->enter($__internal_85f186e309dd16ff5aed36b17814ae1933200b33afe80f449650407b30dec3a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_16e0d668f1ddadf789de5355496a799ca218bb881c479245a4519fc9a6047813 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_16e0d668f1ddadf789de5355496a799ca218bb881c479245a4519fc9a6047813->enter($__internal_16e0d668f1ddadf789de5355496a799ca218bb881c479245a4519fc9a6047813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 258
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 259
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 260
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 261
        echo "</div>";
        
        $__internal_16e0d668f1ddadf789de5355496a799ca218bb881c479245a4519fc9a6047813->leave($__internal_16e0d668f1ddadf789de5355496a799ca218bb881c479245a4519fc9a6047813_prof);

        
        $__internal_85f186e309dd16ff5aed36b17814ae1933200b33afe80f449650407b30dec3a6->leave($__internal_85f186e309dd16ff5aed36b17814ae1933200b33afe80f449650407b30dec3a6_prof);

    }

    // line 264
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_47573d5d0c98f6fcf46a85b22f189f83ae4bd5204516a36fc6e8250da8225458 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_47573d5d0c98f6fcf46a85b22f189f83ae4bd5204516a36fc6e8250da8225458->enter($__internal_47573d5d0c98f6fcf46a85b22f189f83ae4bd5204516a36fc6e8250da8225458_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_b50c77aa1fba747ad46b0b5700c24b4296369c2637a4ebae0330968a270b2185 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b50c77aa1fba747ad46b0b5700c24b4296369c2637a4ebae0330968a270b2185->enter($__internal_b50c77aa1fba747ad46b0b5700c24b4296369c2637a4ebae0330968a270b2185_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 265
        echo "<div class=\"form-group";
        if ( !(isset($context["valid"]) ? $context["valid"] : $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 266
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        // line 267
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        // line 268
        echo "</div>";
        
        $__internal_b50c77aa1fba747ad46b0b5700c24b4296369c2637a4ebae0330968a270b2185->leave($__internal_b50c77aa1fba747ad46b0b5700c24b4296369c2637a4ebae0330968a270b2185_prof);

        
        $__internal_47573d5d0c98f6fcf46a85b22f189f83ae4bd5204516a36fc6e8250da8225458->leave($__internal_47573d5d0c98f6fcf46a85b22f189f83ae4bd5204516a36fc6e8250da8225458_prof);

    }

    // line 273
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_6038c7133319c08fc6eb320e72a5c3a9910e5c4899505c0fd878e0c0898c9a8d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6038c7133319c08fc6eb320e72a5c3a9910e5c4899505c0fd878e0c0898c9a8d->enter($__internal_6038c7133319c08fc6eb320e72a5c3a9910e5c4899505c0fd878e0c0898c9a8d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_17cbb041de830a7ba7229f6e762eb8581c5c8ce5e42217f0a9f5b46d9a6d97ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17cbb041de830a7ba7229f6e762eb8581c5c8ce5e42217f0a9f5b46d9a6d97ac->enter($__internal_17cbb041de830a7ba7229f6e762eb8581c5c8ce5e42217f0a9f5b46d9a6d97ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 274
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 275
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 276
            echo "    <ul class=\"list-unstyled\">";
            // line 277
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 278
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 280
            echo "</ul>
    ";
            // line 281
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_17cbb041de830a7ba7229f6e762eb8581c5c8ce5e42217f0a9f5b46d9a6d97ac->leave($__internal_17cbb041de830a7ba7229f6e762eb8581c5c8ce5e42217f0a9f5b46d9a6d97ac_prof);

        
        $__internal_6038c7133319c08fc6eb320e72a5c3a9910e5c4899505c0fd878e0c0898c9a8d->leave($__internal_6038c7133319c08fc6eb320e72a5c3a9910e5c4899505c0fd878e0c0898c9a8d_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1135 => 281,  1132 => 280,  1124 => 278,  1120 => 277,  1118 => 276,  1112 => 275,  1110 => 274,  1101 => 273,  1091 => 268,  1089 => 267,  1087 => 266,  1081 => 265,  1072 => 264,  1062 => 261,  1060 => 260,  1058 => 259,  1052 => 258,  1043 => 257,  1033 => 254,  1031 => 253,  1022 => 252,  1012 => 249,  1010 => 248,  1001 => 247,  991 => 244,  989 => 243,  980 => 242,  970 => 239,  968 => 238,  959 => 237,  949 => 234,  947 => 233,  945 => 232,  936 => 231,  926 => 228,  924 => 227,  922 => 226,  920 => 225,  914 => 224,  905 => 223,  893 => 217,  889 => 216,  874 => 215,  870 => 212,  867 => 209,  866 => 208,  865 => 207,  863 => 206,  860 => 205,  857 => 204,  854 => 203,  851 => 202,  848 => 201,  845 => 200,  842 => 199,  839 => 198,  837 => 197,  828 => 196,  818 => 193,  809 => 192,  799 => 189,  790 => 188,  780 => 185,  778 => 184,  769 => 182,  759 => 179,  757 => 178,  748 => 177,  737 => 171,  735 => 170,  733 => 169,  730 => 167,  728 => 166,  726 => 165,  717 => 164,  706 => 160,  704 => 159,  702 => 158,  699 => 156,  697 => 155,  695 => 154,  686 => 153,  675 => 149,  669 => 146,  668 => 145,  667 => 144,  663 => 143,  659 => 142,  652 => 138,  651 => 137,  650 => 136,  646 => 135,  644 => 134,  635 => 133,  625 => 130,  623 => 129,  614 => 128,  603 => 124,  599 => 123,  594 => 119,  588 => 118,  582 => 117,  576 => 116,  570 => 115,  564 => 114,  558 => 113,  552 => 112,  547 => 108,  541 => 107,  535 => 106,  529 => 105,  523 => 104,  517 => 103,  511 => 102,  505 => 101,  500 => 98,  497 => 97,  495 => 96,  491 => 95,  489 => 94,  486 => 92,  484 => 91,  475 => 90,  463 => 85,  460 => 84,  450 => 83,  445 => 81,  443 => 80,  441 => 79,  438 => 77,  436 => 76,  427 => 75,  415 => 70,  413 => 69,  411 => 67,  410 => 66,  409 => 65,  408 => 64,  403 => 62,  401 => 61,  399 => 60,  396 => 58,  394 => 57,  385 => 56,  374 => 52,  372 => 51,  370 => 50,  368 => 49,  366 => 48,  362 => 47,  360 => 46,  357 => 44,  355 => 43,  346 => 42,  335 => 38,  333 => 37,  331 => 36,  322 => 35,  312 => 32,  306 => 30,  304 => 29,  302 => 28,  296 => 26,  293 => 25,  291 => 24,  288 => 23,  279 => 22,  269 => 19,  267 => 18,  258 => 17,  248 => 14,  246 => 13,  237 => 12,  227 => 9,  224 => 7,  222 => 6,  213 => 5,  203 => 273,  200 => 272,  197 => 270,  195 => 264,  192 => 263,  190 => 257,  187 => 256,  185 => 252,  182 => 251,  180 => 247,  177 => 246,  175 => 242,  172 => 241,  170 => 237,  167 => 236,  165 => 231,  162 => 230,  160 => 223,  157 => 222,  154 => 220,  152 => 196,  149 => 195,  147 => 192,  144 => 191,  142 => 188,  139 => 187,  137 => 182,  134 => 181,  132 => 177,  129 => 176,  126 => 174,  124 => 164,  121 => 163,  119 => 153,  116 => 152,  114 => 133,  111 => 132,  109 => 128,  107 => 90,  105 => 75,  102 => 74,  100 => 56,  97 => 55,  95 => 42,  92 => 41,  90 => 35,  87 => 34,  85 => 22,  82 => 21,  80 => 17,  77 => 16,  75 => 12,  72 => 11,  70 => 5,  67 => 4,  64 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block money_widget -%}
    <div class=\"input-group\">
        {% set append = money_pattern starts with '{{' %}
        {% if not append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
        {{- block('form_widget_simple') -}}
        {% if append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"input-group\">
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form.date) -}}
            {{- form_errors(form.time) -}}
            {{- form_widget(form.date, { datetime: true } ) -}}
            {{- form_widget(form.time, { datetime: true } ) -}}
        </div>
    {%- endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or not datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif %}
            {{- date_pattern|replace({
                '{{ year }}': form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}': form_widget(form.day),
            })|raw -}}
        {% if datetime is not defined or not datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or false == datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif -%}
        {{- form_widget(form.hour) }}{% if with_minutes %}:{{ form_widget(form.minute) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second) }}{% endif %}
        {% if datetime is not defined or false == datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock time_widget %}

{%- block dateinterval_widget -%}
    {%- if widget == 'single_text' -%}
        {{- block('form_widget_simple') -}}
    {%- else -%}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form) -}}
            <div class=\"table-responsive\">
                <table class=\"table {{ table_class|default('table-bordered table-condensed table-striped') }}\">
                    <thead>
                    <tr>
                        {%- if with_years %}<th>{{ form_label(form.years) }}</th>{% endif -%}
                        {%- if with_months %}<th>{{ form_label(form.months) }}</th>{% endif -%}
                        {%- if with_weeks %}<th>{{ form_label(form.weeks) }}</th>{% endif -%}
                        {%- if with_days %}<th>{{ form_label(form.days) }}</th>{% endif -%}
                        {%- if with_hours %}<th>{{ form_label(form.hours) }}</th>{% endif -%}
                        {%- if with_minutes %}<th>{{ form_label(form.minutes) }}</th>{% endif -%}
                        {%- if with_seconds %}<th>{{ form_label(form.seconds) }}</th>{% endif -%}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        {%- if with_years %}<td>{{ form_widget(form.years) }}</td>{% endif -%}
                        {%- if with_months %}<td>{{ form_widget(form.months) }}</td>{% endif -%}
                        {%- if with_weeks %}<td>{{ form_widget(form.weeks) }}</td>{% endif -%}
                        {%- if with_days %}<td>{{ form_widget(form.days) }}</td>{% endif -%}
                        {%- if with_hours %}<td>{{ form_widget(form.hours) }}</td>{% endif -%}
                        {%- if with_minutes %}<td>{{ form_widget(form.minutes) }}</td>{% endif -%}
                        {%- if with_seconds %}<td>{{ form_widget(form.seconds) }}</td>{% endif -%}
                    </tr>
                    </tbody>
                </table>
            </div>
            {%- if with_invert %}{{ form_widget(form.invert) }}{% endif -%}
        </div>
    {%- endif -%}
{%- endblock dateinterval_widget -%}

{% block choice_widget_collapsed -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
            }) -}}
        {% endfor -%}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'radio-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label %}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {% if widget is defined %}
        {% if required %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
        {% endif %}
        {% if parent_label_class is defined %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) %}
        {% endif %}
        {% if label is not same as(false) and label is empty %}
            {%- if label_format is not empty -%}
                {% set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {% endif %}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {% endif %}
{% endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_layout.html.twig");
    }
}
