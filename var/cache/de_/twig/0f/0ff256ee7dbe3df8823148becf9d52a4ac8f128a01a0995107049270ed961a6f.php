<?php

/* SonataAdminBundle:CRUD:preview.html.twig */
class __TwigTemplate_699ee866482db0717b100b14c0acb480b7be0bbe96dacfa55ed69130fa4342f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:edit.html.twig", "SonataAdminBundle:CRUD:preview.html.twig", 12);
        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'side_menu' => array($this, 'block_side_menu'),
            'formactions' => array($this, 'block_formactions'),
            'preview' => array($this, 'block_preview'),
            'form' => array($this, 'block_form'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:edit.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e3de767219a49ab97b04c9d58a44a06697e4ef7826d109962749b1b83a0785b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e3de767219a49ab97b04c9d58a44a06697e4ef7826d109962749b1b83a0785b->enter($__internal_8e3de767219a49ab97b04c9d58a44a06697e4ef7826d109962749b1b83a0785b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $__internal_5ce25b2b82f4f561fa55a1bb07540a1d60945f17fac5e33af84f31fc70d2dffc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ce25b2b82f4f561fa55a1bb07540a1d60945f17fac5e33af84f31fc70d2dffc->enter($__internal_5ce25b2b82f4f561fa55a1bb07540a1d60945f17fac5e33af84f31fc70d2dffc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:preview.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8e3de767219a49ab97b04c9d58a44a06697e4ef7826d109962749b1b83a0785b->leave($__internal_8e3de767219a49ab97b04c9d58a44a06697e4ef7826d109962749b1b83a0785b_prof);

        
        $__internal_5ce25b2b82f4f561fa55a1bb07540a1d60945f17fac5e33af84f31fc70d2dffc->leave($__internal_5ce25b2b82f4f561fa55a1bb07540a1d60945f17fac5e33af84f31fc70d2dffc_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_6b9c07cbf94cf61c8cf0582b14162475e3073688789e3cde857b98daf3471821 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b9c07cbf94cf61c8cf0582b14162475e3073688789e3cde857b98daf3471821->enter($__internal_6b9c07cbf94cf61c8cf0582b14162475e3073688789e3cde857b98daf3471821_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_379a1d62e3de62c6748b2c0b3ad1a68a98f461475c32d535d66bb39185d9fc92 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_379a1d62e3de62c6748b2c0b3ad1a68a98f461475c32d535d66bb39185d9fc92->enter($__internal_379a1d62e3de62c6748b2c0b3ad1a68a98f461475c32d535d66bb39185d9fc92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        
        $__internal_379a1d62e3de62c6748b2c0b3ad1a68a98f461475c32d535d66bb39185d9fc92->leave($__internal_379a1d62e3de62c6748b2c0b3ad1a68a98f461475c32d535d66bb39185d9fc92_prof);

        
        $__internal_6b9c07cbf94cf61c8cf0582b14162475e3073688789e3cde857b98daf3471821->leave($__internal_6b9c07cbf94cf61c8cf0582b14162475e3073688789e3cde857b98daf3471821_prof);

    }

    // line 17
    public function block_side_menu($context, array $blocks = array())
    {
        $__internal_9ef173a6441c3bd9d7315fc4604a9ab895e4b65124a88db06e51253b28a6be12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ef173a6441c3bd9d7315fc4604a9ab895e4b65124a88db06e51253b28a6be12->enter($__internal_9ef173a6441c3bd9d7315fc4604a9ab895e4b65124a88db06e51253b28a6be12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_menu"));

        $__internal_bbad64e15192985326bbd1c48a0283ee47ac1b4fd37ed7428d1253c4cdd5deaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bbad64e15192985326bbd1c48a0283ee47ac1b4fd37ed7428d1253c4cdd5deaf->enter($__internal_bbad64e15192985326bbd1c48a0283ee47ac1b4fd37ed7428d1253c4cdd5deaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_menu"));

        
        $__internal_bbad64e15192985326bbd1c48a0283ee47ac1b4fd37ed7428d1253c4cdd5deaf->leave($__internal_bbad64e15192985326bbd1c48a0283ee47ac1b4fd37ed7428d1253c4cdd5deaf_prof);

        
        $__internal_9ef173a6441c3bd9d7315fc4604a9ab895e4b65124a88db06e51253b28a6be12->leave($__internal_9ef173a6441c3bd9d7315fc4604a9ab895e4b65124a88db06e51253b28a6be12_prof);

    }

    // line 20
    public function block_formactions($context, array $blocks = array())
    {
        $__internal_4c9350563d28ed6846abfa02b12542b01be88a90e91d0c4a194b2c68ea5e2206 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c9350563d28ed6846abfa02b12542b01be88a90e91d0c4a194b2c68ea5e2206->enter($__internal_4c9350563d28ed6846abfa02b12542b01be88a90e91d0c4a194b2c68ea5e2206_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "formactions"));

        $__internal_b8c162935b86eac92b79df5bb40f36264470002734840668538fc1afbf6a8363 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8c162935b86eac92b79df5bb40f36264470002734840668538fc1afbf6a8363->enter($__internal_b8c162935b86eac92b79df5bb40f36264470002734840668538fc1afbf6a8363_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "formactions"));

        // line 21
        echo "    <button class=\"btn btn-success\" type=\"submit\" name=\"btn_preview_approve\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_preview_approve", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btn_preview_decline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        ";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_preview_decline", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </button>
";
        
        $__internal_b8c162935b86eac92b79df5bb40f36264470002734840668538fc1afbf6a8363->leave($__internal_b8c162935b86eac92b79df5bb40f36264470002734840668538fc1afbf6a8363_prof);

        
        $__internal_4c9350563d28ed6846abfa02b12542b01be88a90e91d0c4a194b2c68ea5e2206->leave($__internal_4c9350563d28ed6846abfa02b12542b01be88a90e91d0c4a194b2c68ea5e2206_prof);

    }

    // line 31
    public function block_preview($context, array $blocks = array())
    {
        $__internal_a394a09402eacb3fbd430c173fef1d7ed1b00cc19a9fda8992e1c29ce7ddbd08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a394a09402eacb3fbd430c173fef1d7ed1b00cc19a9fda8992e1c29ce7ddbd08->enter($__internal_a394a09402eacb3fbd430c173fef1d7ed1b00cc19a9fda8992e1c29ce7ddbd08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        $__internal_c4dc091a9f50a0b0f89a8015ebee68a5ab27cca81621b1ab533df1cb1b7be60b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4dc091a9f50a0b0f89a8015ebee68a5ab27cca81621b1ab533df1cb1b7be60b->enter($__internal_c4dc091a9f50a0b0f89a8015ebee68a5ab27cca81621b1ab533df1cb1b7be60b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "preview"));

        // line 32
        echo "    <div class=\"sonata-ba-view\">
        ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showgroups", array()));
        foreach ($context['_seq'] as $context["name"] => $context["view_group"]) {
            // line 34
            echo "            <table class=\"table table-bordered\">
                ";
            // line 35
            if ($context["name"]) {
                // line 36
                echo "                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            ";
                // line 38
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["name"], array(), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationdomain", array())), "html", null, true);
                echo "
                        </td>
                    </tr>
                ";
            }
            // line 42
            echo "
                ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["view_group"], "fields", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["field_name"]) {
                // line 44
                echo "                    <tr class=\"sonata-ba-view-container\">
                        ";
                // line 45
                if ($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : null), "show", array(), "any", false, true), $context["field_name"], array(), "array", true, true)) {
                    // line 46
                    echo "                            ";
                    echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, $this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "show", array()), $context["field_name"], array(), "array"), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")));
                    echo "
                        ";
                }
                // line 48
                echo "                    </tr>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "            </table>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['view_group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "    </div>
";
        
        $__internal_c4dc091a9f50a0b0f89a8015ebee68a5ab27cca81621b1ab533df1cb1b7be60b->leave($__internal_c4dc091a9f50a0b0f89a8015ebee68a5ab27cca81621b1ab533df1cb1b7be60b_prof);

        
        $__internal_a394a09402eacb3fbd430c173fef1d7ed1b00cc19a9fda8992e1c29ce7ddbd08->leave($__internal_a394a09402eacb3fbd430c173fef1d7ed1b00cc19a9fda8992e1c29ce7ddbd08_prof);

    }

    // line 55
    public function block_form($context, array $blocks = array())
    {
        $__internal_5941af21ae03ce76842767958916d05150e2936281fa87ae7028ef2bc009a535 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5941af21ae03ce76842767958916d05150e2936281fa87ae7028ef2bc009a535->enter($__internal_5941af21ae03ce76842767958916d05150e2936281fa87ae7028ef2bc009a535_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        $__internal_5324de31aaef6e18e7fc47dd86c120cca310da5ddc4dbb4f9dfaa1908f734e6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5324de31aaef6e18e7fc47dd86c120cca310da5ddc4dbb4f9dfaa1908f734e6b->enter($__internal_5324de31aaef6e18e7fc47dd86c120cca310da5ddc4dbb4f9dfaa1908f734e6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 56
        echo "    <div class=\"sonata-preview-form-container\">
        ";
        // line 57
        $this->displayParentBlock("form", $context, $blocks);
        echo "
    </div>
";
        
        $__internal_5324de31aaef6e18e7fc47dd86c120cca310da5ddc4dbb4f9dfaa1908f734e6b->leave($__internal_5324de31aaef6e18e7fc47dd86c120cca310da5ddc4dbb4f9dfaa1908f734e6b_prof);

        
        $__internal_5941af21ae03ce76842767958916d05150e2936281fa87ae7028ef2bc009a535->leave($__internal_5941af21ae03ce76842767958916d05150e2936281fa87ae7028ef2bc009a535_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:preview.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 57,  195 => 56,  186 => 55,  175 => 52,  168 => 50,  161 => 48,  155 => 46,  153 => 45,  150 => 44,  146 => 43,  143 => 42,  136 => 38,  132 => 36,  130 => 35,  127 => 34,  123 => 33,  120 => 32,  111 => 31,  98 => 27,  91 => 23,  87 => 21,  78 => 20,  61 => 17,  44 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:edit.html.twig' %}

{% block actions %}
{% endblock %}

{% block side_menu %}
{% endblock %}

{% block formactions %}
    <button class=\"btn btn-success\" type=\"submit\" name=\"btn_preview_approve\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'btn_preview_approve'|trans({}, 'SonataAdminBundle') }}
    </button>
    <button class=\"btn btn-danger\" type=\"submit\" name=\"btn_preview_decline\">
        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>
        {{ 'btn_preview_decline'|trans({}, 'SonataAdminBundle') }}
    </button>
{% endblock %}

{% block preview %}
    <div class=\"sonata-ba-view\">
        {% for name, view_group in admin.showgroups %}
            <table class=\"table table-bordered\">
                {% if name %}
                    <tr class=\"sonata-ba-view-title\">
                        <td colspan=\"2\">
                            {{ name|trans({}, admin.translationdomain) }}
                        </td>
                    </tr>
                {% endif %}

                {% for field_name in view_group.fields %}
                    <tr class=\"sonata-ba-view-container\">
                        {% if admin.show[field_name] is defined %}
                            {{ admin.show[field_name]|render_view_element(object) }}
                        {% endif %}
                    </tr>
                {% endfor %}
            </table>
        {% endfor %}
    </div>
{% endblock %}

{% block form %}
    <div class=\"sonata-preview-form-container\">
        {{ parent() }}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:preview.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/preview.html.twig");
    }
}
