<?php

/* @SonataBlock/Block/block_container.html.twig */
class __TwigTemplate_52fca5b16762887df69eb1fc9138cfc55f60821a921a650e3f85b7a10073f5e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block_class' => array($this, 'block_block_class'),
            'block_role' => array($this, 'block_block_role'),
            'block' => array($this, 'block_block'),
            'block_child_render' => array($this, 'block_block_child_render'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_container.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2ce53ef86a016e5f4ab52a61087e1a54e78f937a911f255b3fc310a40fa3d98 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2ce53ef86a016e5f4ab52a61087e1a54e78f937a911f255b3fc310a40fa3d98->enter($__internal_a2ce53ef86a016e5f4ab52a61087e1a54e78f937a911f255b3fc310a40fa3d98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_container.html.twig"));

        $__internal_9610388ce80867533b0cbae976ca73fdf920b39f7d07e4b2de97539015b324c0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9610388ce80867533b0cbae976ca73fdf920b39f7d07e4b2de97539015b324c0->enter($__internal_9610388ce80867533b0cbae976ca73fdf920b39f7d07e4b2de97539015b324c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_container.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a2ce53ef86a016e5f4ab52a61087e1a54e78f937a911f255b3fc310a40fa3d98->leave($__internal_a2ce53ef86a016e5f4ab52a61087e1a54e78f937a911f255b3fc310a40fa3d98_prof);

        
        $__internal_9610388ce80867533b0cbae976ca73fdf920b39f7d07e4b2de97539015b324c0->leave($__internal_9610388ce80867533b0cbae976ca73fdf920b39f7d07e4b2de97539015b324c0_prof);

    }

    // line 15
    public function block_block_class($context, array $blocks = array())
    {
        $__internal_330fd26648b420dae8ad491611abeae09a34abc8e16c3a518dc8206758eb1b5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_330fd26648b420dae8ad491611abeae09a34abc8e16c3a518dc8206758eb1b5c->enter($__internal_330fd26648b420dae8ad491611abeae09a34abc8e16c3a518dc8206758eb1b5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        $__internal_a30cefa4e70c1267b065553a96e0614c70f5fb80b9cc8a1081e9eef09b2ffad3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a30cefa4e70c1267b065553a96e0614c70f5fb80b9cc8a1081e9eef09b2ffad3->enter($__internal_a30cefa4e70c1267b065553a96e0614c70f5fb80b9cc8a1081e9eef09b2ffad3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_class"));

        echo " cms-container";
        if ( !$this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "hasParent", array(), "method")) {
            echo " cms-container-root";
        }
        if ($this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "class", array())) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "class", array()), "html", null, true);
        }
        
        $__internal_a30cefa4e70c1267b065553a96e0614c70f5fb80b9cc8a1081e9eef09b2ffad3->leave($__internal_a30cefa4e70c1267b065553a96e0614c70f5fb80b9cc8a1081e9eef09b2ffad3_prof);

        
        $__internal_330fd26648b420dae8ad491611abeae09a34abc8e16c3a518dc8206758eb1b5c->leave($__internal_330fd26648b420dae8ad491611abeae09a34abc8e16c3a518dc8206758eb1b5c_prof);

    }

    // line 18
    public function block_block_role($context, array $blocks = array())
    {
        $__internal_89107f214706add1d7771e53164679b38c92f6f2596b9e11096a78139fdb701f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89107f214706add1d7771e53164679b38c92f6f2596b9e11096a78139fdb701f->enter($__internal_89107f214706add1d7771e53164679b38c92f6f2596b9e11096a78139fdb701f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        $__internal_33f6efc230490a69a84e21ea0ab6b4462c0972b08660c752ef52f852ee0613a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33f6efc230490a69a84e21ea0ab6b4462c0972b08660c752ef52f852ee0613a9->enter($__internal_33f6efc230490a69a84e21ea0ab6b4462c0972b08660c752ef52f852ee0613a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_role"));

        echo "container";
        
        $__internal_33f6efc230490a69a84e21ea0ab6b4462c0972b08660c752ef52f852ee0613a9->leave($__internal_33f6efc230490a69a84e21ea0ab6b4462c0972b08660c752ef52f852ee0613a9_prof);

        
        $__internal_89107f214706add1d7771e53164679b38c92f6f2596b9e11096a78139fdb701f->leave($__internal_89107f214706add1d7771e53164679b38c92f6f2596b9e11096a78139fdb701f_prof);

    }

    // line 21
    public function block_block($context, array $blocks = array())
    {
        $__internal_790ab4210d6e893c1fe53d58a7cb5bfa343b93d2840c1c884be26c9970d3f0fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_790ab4210d6e893c1fe53d58a7cb5bfa343b93d2840c1c884be26c9970d3f0fd->enter($__internal_790ab4210d6e893c1fe53d58a7cb5bfa343b93d2840c1c884be26c9970d3f0fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_ae1c62aaf19e4280431468cf5d40fd6b4eee0381885165dd9f4d9fe11ec8ef85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ae1c62aaf19e4280431468cf5d40fd6b4eee0381885165dd9f4d9fe11ec8ef85->enter($__internal_ae1c62aaf19e4280431468cf5d40fd6b4eee0381885165dd9f4d9fe11ec8ef85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 22
        echo "    ";
        if ((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator"))) {
            echo $this->getAttribute((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator")), "pre", array());
        }
        // line 23
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "children", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 24
            echo "        ";
            $this->displayBlock('block_child_render', $context, $blocks);
            // line 27
            echo "    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 28
        echo "    ";
        if ((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator"))) {
            echo $this->getAttribute((isset($context["decorator"]) ? $context["decorator"] : $this->getContext($context, "decorator")), "post", array());
        }
        
        $__internal_ae1c62aaf19e4280431468cf5d40fd6b4eee0381885165dd9f4d9fe11ec8ef85->leave($__internal_ae1c62aaf19e4280431468cf5d40fd6b4eee0381885165dd9f4d9fe11ec8ef85_prof);

        
        $__internal_790ab4210d6e893c1fe53d58a7cb5bfa343b93d2840c1c884be26c9970d3f0fd->leave($__internal_790ab4210d6e893c1fe53d58a7cb5bfa343b93d2840c1c884be26c9970d3f0fd_prof);

    }

    // line 24
    public function block_block_child_render($context, array $blocks = array())
    {
        $__internal_9f2cce7ef34465130b024cb867541792b6c907096a6dc4c9f0fcfe87f5da4c64 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9f2cce7ef34465130b024cb867541792b6c907096a6dc4c9f0fcfe87f5da4c64->enter($__internal_9f2cce7ef34465130b024cb867541792b6c907096a6dc4c9f0fcfe87f5da4c64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        $__internal_c6797eabe00dac5495102c603adb73d507785d2bb5b350a3d0c9374ec89dbae1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6797eabe00dac5495102c603adb73d507785d2bb5b350a3d0c9374ec89dbae1->enter($__internal_c6797eabe00dac5495102c603adb73d507785d2bb5b350a3d0c9374ec89dbae1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block_child_render"));

        // line 25
        echo "            ";
        echo call_user_func_array($this->env->getFunction('sonata_block_render')->getCallable(), array((isset($context["child"]) ? $context["child"] : $this->getContext($context, "child"))));
        echo "
        ";
        
        $__internal_c6797eabe00dac5495102c603adb73d507785d2bb5b350a3d0c9374ec89dbae1->leave($__internal_c6797eabe00dac5495102c603adb73d507785d2bb5b350a3d0c9374ec89dbae1_prof);

        
        $__internal_9f2cce7ef34465130b024cb867541792b6c907096a6dc4c9f0fcfe87f5da4c64->leave($__internal_9f2cce7ef34465130b024cb867541792b6c907096a6dc4c9f0fcfe87f5da4c64_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_container.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 25,  147 => 24,  134 => 28,  120 => 27,  117 => 24,  99 => 23,  94 => 22,  85 => 21,  67 => 18,  42 => 15,  21 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{# block classes are prepended with a container class #}
{% block block_class %} cms-container{% if not block.hasParent() %} cms-container-root{%endif%}{% if settings.class %} {{ settings.class }}{% endif %}{% endblock %}

{# identify a block role used by the page editor #}
{% block block_role %}container{% endblock %}

{# render container block #}
{% block block %}
    {% if decorator %}{{ decorator.pre|raw }}{% endif %}
    {% for child in block.children %}
        {% block block_child_render %}
            {{ sonata_block_render(child) }}
        {% endblock %}
    {% endfor %}
    {% if decorator %}{{ decorator.post|raw }}{% endif %}
{% endblock %}
", "@SonataBlock/Block/block_container.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_container.html.twig");
    }
}
