<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_533b3a8ea9b97e33f3b51c5c525b556f60a6159ab652d7fed043cdc2394ee4bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c8110bddf8ea36351a0ddea650724367c1927e8d443d6efc23525ff0569ed70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c8110bddf8ea36351a0ddea650724367c1927e8d443d6efc23525ff0569ed70->enter($__internal_7c8110bddf8ea36351a0ddea650724367c1927e8d443d6efc23525ff0569ed70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        $__internal_e3da6f3ae08a52b6dbf35212fde7bee6ce031c9586af077fd0f63f952fdb0a11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3da6f3ae08a52b6dbf35212fde7bee6ce031c9586af077fd0f63f952fdb0a11->enter($__internal_e3da6f3ae08a52b6dbf35212fde7bee6ce031c9586af077fd0f63f952fdb0a11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_7c8110bddf8ea36351a0ddea650724367c1927e8d443d6efc23525ff0569ed70->leave($__internal_7c8110bddf8ea36351a0ddea650724367c1927e8d443d6efc23525ff0569ed70_prof);

        
        $__internal_e3da6f3ae08a52b6dbf35212fde7bee6ce031c9586af077fd0f63f952fdb0a11->leave($__internal_e3da6f3ae08a52b6dbf35212fde7bee6ce031c9586af077fd0f63f952fdb0a11_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "@Twig/Exception/exception.atom.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.atom.twig");
    }
}
