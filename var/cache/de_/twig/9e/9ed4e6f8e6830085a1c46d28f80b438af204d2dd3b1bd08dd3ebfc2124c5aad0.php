<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_d5c33f1b6492b89f3f850958346a77046b7e4d4e1b7a70688bda1b6a0d725cf6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aeab41a188a47311059dca3b9745c01165489ebd027c28d8900d575849b6b46e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aeab41a188a47311059dca3b9745c01165489ebd027c28d8900d575849b6b46e->enter($__internal_aeab41a188a47311059dca3b9745c01165489ebd027c28d8900d575849b6b46e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_ab3742b5c25bce502283f90c322d06b0b1949b02f77bd8bcaec60989cd1ee69e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab3742b5c25bce502283f90c322d06b0b1949b02f77bd8bcaec60989cd1ee69e->enter($__internal_ab3742b5c25bce502283f90c322d06b0b1949b02f77bd8bcaec60989cd1ee69e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_aeab41a188a47311059dca3b9745c01165489ebd027c28d8900d575849b6b46e->leave($__internal_aeab41a188a47311059dca3b9745c01165489ebd027c28d8900d575849b6b46e_prof);

        
        $__internal_ab3742b5c25bce502283f90c322d06b0b1949b02f77bd8bcaec60989cd1ee69e->leave($__internal_ab3742b5c25bce502283f90c322d06b0b1949b02f77bd8bcaec60989cd1ee69e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}
